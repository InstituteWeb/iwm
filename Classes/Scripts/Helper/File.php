<?php
namespace InstituteWeb\Iwm\Scripts\Helper;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

/**
 * File Helper
 *
 * @package InstituteWeb\Iwm
 */
class File
{
    /** Shortcut for directory separator */
    const DS = DIRECTORY_SEPARATOR;

    /**
     * Returns true if PHP runs on Windows OS
     *
     * @return bool
     */
    public static function isWindowsOs()
    {
        return DIRECTORY_SEPARATOR === '\\';
    }

    /**
     * Does the same like realpath() does, but does not require that the referenced file
     * is existing.
     *
     * @param string $path
     * @return string
     */
    public static function normalizePath($path)
    {
        $parts = [];// Array to build a new path from the good parts
        $path = str_replace('\\', '/', $path);// Replace backslashes with forwardslashes
        $path = preg_replace('/\/+/', '/', $path);// Combine multiple slashes into a single slash
        $segments = explode('/', $path);// Collect path segments
        foreach ($segments as $segment) {
            if ($segment !== '.') {
                $test = array_pop($parts);
                if (is_null($test)) {
                    $parts[] = $segment;
                } else {
                    if ($segment === '..') {
                        if ($test === '..') {
                            $parts[] = $test;
                        }

                        if ($test === '..' || $test === '') {
                            $parts[] = $segment;
                        }
                    } else {
                        $parts[] = $test;
                        $parts[] = $segment;
                    }
                }
            }
        }
        return implode(DIRECTORY_SEPARATOR, $parts);
    }

    /**
     * Concatenates given path parts with DIRECTORY_SEPARATOR
     *
     * @param array $pathParts
     * @param bool $appendDirectorySeparator
     * @return string
     */
    public static function concatenatePathParts(array $pathParts, $appendDirectorySeparator = false)
    {
        if ($appendDirectorySeparator) {
            return implode(DIRECTORY_SEPARATOR, $pathParts) . DIRECTORY_SEPARATOR;
        }
        return implode(DIRECTORY_SEPARATOR, $pathParts);
    }

    /**
     * Get absolute path to root of TYPO3 installation.
     * With ending slash.
     *
     * @return string
     */
    public static function getTypo3RootPath()
    {
        return realpath(static::concatenatePathParts([__DIR__, '..', '..', '..', '..', '..', '..'])) . static::DS;
    }

    /**
     * Get absolute path of typo3conf directory in root of TYPO3 installation.
     * With ending slash.
     *
     * @return string
     */
    public static function getTypo3ConfPath()
    {
        return realpath(static::concatenatePathParts([static::getTypo3RootPath(), 'typo3conf'])) . static::DS;
    }

    /**
     * Get contens from LocalConfiguration.php
     *
     * @return array
     */
    public static function getLocalConfigurationValues()
    {
        return static::loadPhpConfigurationArray(static::getTypo3ConfPath() . 'LocalConfiguration.php');
    }

    /**
     * Get contents from AdditionalConfiguration.php
     *
     * @return array
     */
    public static function getDefaultConfigurationValues()
    {
        $pathToDefaultConfiguration = realpath(static::concatenatePathParts([
            static::getTypo3RootPath(), 'typo3', 'sysext', 'core', 'Configuration', 'DefaultConfiguration.php'
        ]));

        return static::loadPhpConfigurationArray($pathToDefaultConfiguration);
    }

    /**
     * Loads php file expecting an returning array
     *
     * @param string $path
     * @return array
     */
    public static function loadPhpConfigurationArray($path)
    {
        if (!file_exists($path)) {
            throw new \RuntimeException('File not found: ' . $path);
        }
        $values = @require_once($path);
        if (!is_array($values)) {
            return [];
        }
        return $values;
    }
}
