<?php
namespace InstituteWeb\Iwm\Scripts\Helper;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

/**
 * Class ApplicationContext Helper
 *
 * @package InstituteWeb\Iwm
 */
class ApplicationContext
{
    /**
     * Get current application context as string
     * e.g. "Development/DevServer"
     *
     * @param bool $stripRootContext When true the root context is removed from context
     * @return string
     */
    public static function getCurrentApplicationContext($stripRootContext = false)
    {
        $context = self::getCurrentApplicationContextStatus()['context'];
        if ($stripRootContext) {
            $contextParts = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode('/', $context);
            array_shift($contextParts);
            return implode('/', $contextParts);
        }
        return $context;
    }

    /**
     * Get relative path to expected config file, by set TYPO3_CONTEXT under given path
     *
     * @return string Path to php configuration file
     */
    public static function getConfigPathFromSetApplicationContext()
    {
        $contextWithoutRoot = static::getCurrentApplicationContext(true);
        if (empty($contextWithoutRoot)) {
            return str_pad('Config file: -', 64, ' ', STR_PAD_RIGHT);
        }
        return File::concatenatePathParts(['typo3conf', 'Environments', $contextWithoutRoot]) . '.php';
    }

    /**
     * Get current application context from .env file and return array containing the status.
     *
     * @return array Available keys: type, file, context
     * @throws \RuntimeException
     */
    public static function getCurrentApplicationContextStatus()
    {
        if (file_exists(File::getTypo3RootPath() . '.env')) {
            $dotenv = new \Dotenv\Dotenv(File::getTypo3RootPath());
            $dotenv->load();
            unset($dotenv);
        }

        // from actual environment variables defined by Dotenv/Dotenv
        if (getenv('TYPO3_CONTEXT')) {
            return [
                'type' => 'dotenv',
                'file' => null,
                'context' => getenv('TYPO3_CONTEXT')
            ];
        }
        // if no context is set
        return [
            'type' => null,
            'file' => null,
            'context' => ''
        ];
    }

    /**
     * Removes TYPO3_CONTEXT environment variables from .env file
     *
     * @param string $cwd Current working directory
     * @return array Touched files. Keyname is filename. Value is status of writing to file.
     */
    public static function removeApplicationContext($cwd)
    {
        $touchedFiles = [];
        $dotEnvWriter = new DotEnvWriter($cwd);
        if (!$dotEnvWriter->isNew() && $dotEnvWriter->has('TYPO3_CONTEXT')) {
            $dotEnvWriter->remove('TYPO3_CONTEXT')->save();
            $touchedFiles[$dotEnvWriter->getPath()] = true;
        }
        return $touchedFiles;
    }
}
