<?php
namespace InstituteWeb\Iwm\Scripts\Helper;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ConsoleOutput
 *
 * @package InstituteWeb\Iwm
 */
class ConsoleOutput extends \TYPO3\CMS\Extbase\Mvc\Cli\ConsoleOutput
{
    /**
     * Extends TYPO3's ConsoleOutput
     *
     * @param InputInterface|null $input
     * @param OutputInterface|null $output
     * @return ConsoleOutput
     */
    public function __construct(InputInterface $input = null, OutputInterface $output = null)
    {
        parent::__construct();
        if ($output) {
            $this->output = $output;
        }
    }

    /**
     * @return null|OutputInterface
     */
    public function getOutput()
    {
        return $this->output;
    }

    /**
     * Output menu and validates input
     *
     * @param string $title
     * @param array $options
     * @param bool $returnSelectedOptionKey When true not the entered number is returned, the value of the option is
     * @param bool $startWithZero When true the first menu item starts with 0) instead of 1)
     * @return int|string entered menu entry number or value of selected option
     */
    public function outputMenu($title, array $options = [], $returnSelectedOptionKey = false, $startWithZero = false)
    {
        $question = $title . PHP_EOL . PHP_EOL;
        $i = 0;
        foreach (array_values($options) as $option) {
            $index = $i;
            if (!$startWithZero) {
                $index = $index + 1;
            }
            $question .= ' ' . $index . ') ' . $option . PHP_EOL;
            $i++;
        }

        $from = $startWithZero ? 0 : 1;
        $to = $startWithZero ? count($options) - 1 : count($options);

        $question .= PHP_EOL . '[' . $from . '-' . $to . ']? ';

        return $this->askAndValidate($question, function ($value) use ($from, $to, $options, $returnSelectedOptionKey, $startWithZero) {
            if ($value < $from || $value > $to) {
                throw new \UnexpectedValueException('Invalid value entered!');
            }
            if ($returnSelectedOptionKey) {
                $optionsWithoutKeys = array_values($options);
                $pointer = $value;
                if ($startWithZero) {
                    $pointer = $pointer - 1;
                }
                return $optionsWithoutKeys[$pointer];
            }
            return (int) $value;
        });
    }


    public function outputTable($rows, $headers = null, $prependIndex = false)
    {
        if ($prependIndex) {
            $i = 0;
            foreach ($rows as $index => $row) {
                array_unshift($rows[$index], ++$i);
            }
            array_unshift($headers, is_string($prependIndex) ? $prependIndex : 'Id');
        }
        parent::outputTable($rows, $headers);
    }
}
