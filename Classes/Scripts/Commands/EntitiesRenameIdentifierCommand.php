<?php
namespace InstituteWeb\Iwm\Scripts\Commands;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Iwm\Environments\DataProvider\EntityManager;
use InstituteWeb\Iwm\Environments\DataProvider\Models\AbstractSystemEntryModel;
use InstituteWeb\Iwm\Environments\Environment;
use InstituteWeb\Iwm\Scripts\Helper;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use Tree\Node\Node;
use Tree\Visitor\PreOrderVisitor;
use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\Registry;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Compares entities in current context with database and inserts/updates them
 *
 * @package Iwm\Iwm
 */
class EntitiesRenameIdentifierCommand extends AbstractCommand
{
    /**
     * Configure this command
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('entities:rename-identifier')
            ->setDescription('Renames an identifier in code and database.')

            ->addArgument(
                'oldIdentifier',
                InputArgument::OPTIONAL,
                'Old name of identifier which should get renamed'
            )
            ->addArgument(
                'newIdentifier',
                InputArgument::OPTIONAL,
                'New name of identifier.'
            )

            ->addOption(
                'force',
                'f',
                InputOption::VALUE_NONE,
                'Forces renaming of entities. Be careful!'
            )
            ->addOption(
                'path',
                null,
                InputOption::VALUE_OPTIONAL,
                'Path to configuration files.',
                Helper\File::concatenatePathParts([getcwd(), 'typo3conf', 'Environments'])
            )
            ->addOption(
                'no-files',
                null,
                InputOption::VALUE_NONE
            )
        ;
    }

    /**
     * Executes this command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void|int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->initIO($input, $output);

        $entityMappings = $this->getEntityMappings();

        // get old identifier
        $oldIdentifier = $this->askForOldIdentifier($entityMappings);

        // get new identifier
        $newIdentifier = $this->askForNewIdentifier($entityMappings);


        // Update entity mappings in sys_registry
        $modifiedEntityMappings = $entityMappings;
        $modifiedEntityMappings[$newIdentifier] = $modifiedEntityMappings[$oldIdentifier];
        unset($modifiedEntityMappings[$oldIdentifier]);
        Environment::getInstance()->setEntityMappings($modifiedEntityMappings);


        // Update configuration files
        $this->updateIdentifierInConfigurationFiles($oldIdentifier, $newIdentifier);

        $this->output->outputLine(
            PHP_EOL . 'When you append the identifier mapping to TypoScript, you need to perform "iwm entities:update".'
        );

        $continue = $this->output->askConfirmation('Do you want to call it now? [Y/n]');
        if ($continue) {
            $command = $this->getApplication()->find('entities:update');
            $command->run(new ArrayInput(['--force' => true]), $output);
        }
    }

    /**
     * Get entity mappings.
     * Outputs error messages and returns empty array, on failure.
     *
     * @return array
     */
    protected function getEntityMappings()
    {
        // Get current context (from .env)
        $context = Helper\ApplicationContext::getCurrentApplicationContext();
        if (getenv('TYPO3_CONTEXT') !== $context) {
            putenv('TYPO3_CONTEXT=' . $context);
        }

        // Include configuration files
        EntityManager::getInstance()->reset();
        $additionalConfiguration = Helper\File::getTypo3ConfPath() . 'AdditionalConfiguration.php';
        require ($additionalConfiguration);

        if (!EntityManager::getInstance()->hasEntries()) {
            $this->output->outputLine('<error>No configured entries found!</error>');
            return [];
        }

        $entityMappings = Environment::getInstance()->getEntityMappings();
        if (empty($entityMappings)) {
            $this->output->outputLine('<error>No entity mappings found in sys_registry.</error>');
            return [];
        }
        return $entityMappings;
    }

    /**
     * Get value for old identifier from arguments or user input
     *
     * @param array $entityMappings
     * @return string Value of old identifier
     */
    protected function askForOldIdentifier(array $entityMappings)
    {
        $validateOldIdentifier = function ($value) use ($entityMappings) {
            if (!array_key_exists($value, $entityMappings)) {
                throw new \InvalidArgumentException(
                    'Given old identifier "' . $value . '" not found in entity mapping in sys_registry.'
                );
            }
            return $value;
        };

        $oldIdentifier = $this->input->getArgument('oldIdentifier');
        if ($oldIdentifier) {
            $validateOldIdentifier($oldIdentifier);
            return $oldIdentifier;
        }
        return $this->output->askAndValidate(
            'Please enter the old identifier name: ',
            $validateOldIdentifier,
            false,
            null,
            array_keys($entityMappings)
        );
    }

    /**
     * Get value for new identifier from arguments or user input
     *
     * @param array $entityMappings
     * @return string Value of new identifier
     */
    protected function askForNewIdentifier(array $entityMappings)
    {
        $validateNewIdentifier = function ($value) use ($entityMappings) {
            if (!$this->input->getOption('force') && array_key_exists($value, $entityMappings)) {
                throw new \InvalidArgumentException('Given new identifier "' . $value . '" is already existing!');
            }
            return $value;
        };

        $newIdentifier = $this->input->getArgument('newIdentifier');
        if ($newIdentifier) {
            $validateNewIdentifier($newIdentifier);
            return $newIdentifier;
        }
        return $this->output->askAndValidate('Please enter the new identifier name: ', $validateNewIdentifier);
    }

    /**
     * @param string $oldIdentifier
     * @param string $newIdentifier
     * @return void
     */
    protected function updateIdentifierInConfigurationFiles($oldIdentifier, $newIdentifier)
    {
        $touchedFiles = [];
        if (!$this->input->getOption('no-files')) {
            $configurationFiles = $this->getPhpConfigurationFiles($this->input->getOption('path'), false, true);
            foreach (array_keys($configurationFiles) as $filePath) {
                if (!is_writable($filePath)) {
                    throw new \RuntimeException('Can\'t modify file "' . $filePath . '". Insufficient write access.');
                }

                $fileContents = file_get_contents($filePath);
                $newFileContents = str_replace('"' . $oldIdentifier . '"', '"' . $newIdentifier . '"', $fileContents);
                $newFileContents = str_replace('\'' . $oldIdentifier . '\'', '\'' . $newIdentifier . '\'', $newFileContents);

                if ($fileContents !== $newFileContents) {
                    file_put_contents($filePath, $newFileContents);
                    $touchedFiles[] = $filePath;
                }
            }
        }

        $this->output->outputLine('Renamed the identifier "' . $oldIdentifier . '" to "' . $newIdentifier . '" in sys_registry');
        if (!empty($touchedFiles)) {
            $this->output->outputLine('and in these files:');
            $this->output->outputLine(implode(PHP_EOL, $touchedFiles));
            $this->output->outputLine(PHP_EOL . 'Notice: Don\'t forget to add the changed files to VCS!');
        }
    }
}
