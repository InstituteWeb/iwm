<?php
namespace InstituteWeb\Iwm\Scripts\Commands;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Iwm\Environments\DataProvider\EntityManager;
use InstituteWeb\Iwm\Environments\Environment;
use InstituteWeb\Iwm\Scripts\Helper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 *
 * @package Iwm\Iwm
 */
class InstallCommand extends AbstractCommand
{
    /**
     * @var \PDO
     */
    protected $pdo;

    /**
     * Configure this command
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('install')
            ->setDescription('Installs iw_master for TYPO3 and set up database.')

            ->addOption(
                'host',
                null,
                InputOption::VALUE_OPTIONAL,
                'Database host',
                '127.0.0.1'
            )
            ->addOption(
                'user',
                'u',
                InputOption::VALUE_OPTIONAL,
                'Database user',
                ''
            )
            ->addOption(
                'pass',
                'p',
                InputOption::VALUE_OPTIONAL,
                'Database credentials',
                ''
            )
            ->addOption(
                'db',
                null,
                InputOption::VALUE_OPTIONAL,
                'Database to choose',
                ''
            )
            ->addOption(
                'port',
                null,
                InputOption::VALUE_OPTIONAL,
                'Optional port to connect to',
                '3306'
            )
            ->addOption(
                'socket',
                null,
                InputOption::VALUE_OPTIONAL,
                'Optional socket',
                ''
            )

            ->addOption(
                'admin-user-name',
                null,
                InputOption::VALUE_OPTIONAL,
                'Username of admin user to create during installation',
                'admin'
            )
            ->addOption(
                'admin-password',
                null,
                InputOption::VALUE_OPTIONAL,
                'Password of admin user to create during installation',
                'supersecret'
            )

            ->addOption(
                'finish',
                null,
                InputOption::VALUE_NONE,
                'Calls all operations after typo3cms has been involved. Required for Windows OS.'
            )
        ;
    }

    /**
     * Executes this command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->initIO($input, $output);

        // When --finish option is given just the clean up tasks are performed
        if ($input->getOption('finish')) {
            if (!file_exists(Helper\File::getTypo3RootPath() . 'FIRST_INSTALL')) {
                throw new \Exception(
                    'No FIRST_INSTALL file found, which is expected to be existing during the installation process. ' .
                    'Call install command without --finish option.'
                );
            }
            $this->finishInstallation();
            return;
        }

        // Prepare installation and get DatabaseConnection instance
        $database = $this->prepareInstallation();

        // Runs PackageStates.php generation
        if (!Helper\File::isWindowsOs()) {
            $buildPackageStates = $this->output->askConfirmation(
                'We need a PackageStates.php file with activated packages. ' . PHP_EOL .
                'Create one, based on entries in composer.json? [Y/n] '
            );
            $this->output->outputLine();
            if ($buildPackageStates) {
                $this->runTypo3ConsoleGeneratePackageStates();
                $this->output->outputLine();
            }
        }

        // Runs typo3cms install
        $this->runTypo3ConsoleInstaller($input, $database);

        // Check for entities to get imported
        $this->handleEntitiesImport();

        // Clean up tasks
        $this->finishInstallation();
    }

    /**
     * Prepares the installation of iw_master and returns set up DatabaseConnection
     *
     * @return DatabaseConnection|void
     */
    protected function prepareInstallation()
    {
        $this->output->outputLine($this->getHeaderTitle());
        $this->output->outputLine($this->getWelcomeText());

        $continue = $this->output->askConfirmation(PHP_EOL . 'Are you ready? [Y/n] ');
        if (!$continue) {
            $this->output->outputLine('> Okay, take all time you need.');
            return;
        }

        // Handle Application Context (which is required)
        $context = $this->handleApplicationContext();
        if (empty($context)) {
            return;
        }
        $configPath = Helper\ApplicationContext::getConfigPathFromSetApplicationContext();

        $this->output->outputLine();
        $this->output->outputLine('Found application context: ' . $context);
        $this->output->outputLine('Expected config file: ' . $configPath . (file_exists($configPath) ? ' (file exists)': ' (file missing)'));
        $this->output->outputLine();

        // Copies default configuration example to given path (asks before copying)
        $this->createEnvironmentConfigurationFileIfNotExisting($configPath);

        // Get existing credentials and ask for updating them
        $GLOBALS['TYPO3_DB'] = $database = $this->handleDatabaseCredentials($configPath, $this->input);

        // Ask for admin username and password, to create during installation
        $this->handleAdminUser();

        // Check database tables and offer to drop them
        $this->handleDatabaseClearing($database);

        // Creates FIRST_INSTALL file
        file_put_contents(Helper\File::getTypo3RootPath() . 'FIRST_INSTALL', 'created by iwm install');

        return $database;
    }

    /**
     * Finishes the installation of iw_master and performs some clean up tasks.
     *
     * - Removes FIRST_INSTALL
     * - Cleans up LocalConfiguration.php
     * - Removes "iwm_auto_domain_record" file in typo3temp, if existing
     * - Check and fix folder structure
     *
     * @return void
     */
    protected function finishInstallation()
    {
        $this->output->outputLine();

        // Deletes FIRST_INSTALL file
        if (file_exists(Helper\File::getTypo3RootPath() . 'FIRST_INSTALL')) {
            unlink(Helper\File::getTypo3RootPath() . 'FIRST_INSTALL');
        }

        // Cleanup LocalConfiguration.php
        $this->output->output('Cleanup LocalConfiguration.php... ');
        $result = $this->cleanUpLocalConfiguration();
        $this->output->outputLine($result ? 'OK' : 'ERR');

        // Remove iwm_auto_domain_record hash from typo3temp
        if (file_exists(Helper\File::getTypo3RootPath() . 'typo3temp' . Helper\File::DS . 'iwm_auto_domain_record')) {
            unlink(Helper\File::getTypo3RootPath() . 'typo3temp' . Helper\File::DS . 'iwm_auto_domain_record');
        }

        // Create fileadmin/ folder
        $this->output->output(PHP_EOL . 'Check and fix folder structure... ');
        $fixFolderStatus = $this->fixFolderStructure();
        $this->output->outputLine($fixFolderStatus ? '<info>OK</info>' : '<error>ERR</error>');


        $this->output->outputLine();
        $this->output->outputLine('Great! Now we have a blank TYPO3 installation with one admin user.');
        $this->output->outputLine();

        // End
        $this->output->outputLine($this->getByeText());
    }

    /**
     * Calls fix folder structure task from install tool
     *
     * @return bool|\Exception
     */
    protected function fixFolderStructure()
    {
        try {
            define('PATH_site', str_replace('\\', '/', Helper\File::getTypo3RootPath()));
            define('TYPO3_OS', (!stristr(PHP_OS, 'darwin') && !stristr(PHP_OS, 'cygwin') && stristr(PHP_OS, 'win')) ? 'WIN' : '');
            $GLOBALS['TYPO3_CONF_VARS']['SYS']['fileCreateMask'] = Environment::getInstance()->typo3ConfVars['SYS']['fileCreateMask'] ?: '0664';
            $GLOBALS['TYPO3_CONF_VARS']['SYS']['folderCreateMask'] = Environment::getInstance()->typo3ConfVars['SYS']['folderCreateMask'] ?: '2775';

            /** @var $folderStructureFactory \TYPO3\CMS\Install\FolderStructure\DefaultFactory */
            $folderStructureFactory = new \TYPO3\CMS\Install\FolderStructure\DefaultFactory();
            /** @var $structureFacade \TYPO3\CMS\Install\FolderStructure\StructureFacade */
            $structureFacade = $folderStructureFactory->getStructure();

            $statusUtility = new \TYPO3\CMS\Install\Status\StatusUtility();
            $structureStatus = $structureFacade->getStatus();
            $errors = array_merge(
                $statusUtility->filterBySeverity($structureStatus, 'error'),
                $statusUtility->filterBySeverity($structureStatus, 'warning')
            );
            if (!empty($errors)) {
                $structureFacade->fix();
            }
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    /**
     * Establishes database connection
     *
     * @param string $host
     * @param string $user
     * @param string $pass
     * @param string $db
     * @return void
     */
    protected function establishDatabaseConnection($host, $user, $pass, $db)
    {
        $this->output->output('Connecting to database... ');
        try {
            $this->pdo = new \PDO(
                'mysql:dbname=' . $db . ';host=' . $host,
                $user,
                $pass,
                array(
                    \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
                )
            );
        } catch (\Exception $e) {
            $this->output->outputLine('failed!');
            $this->output->outputLine('Error: ' . $e->getMessage());
            die;
        }
        $this->output->outputLine('successful!');
        $this->output->outputLine();
    }



    /**
     * Set the application context.
     * Can't proceed without configured context.
     *
     * @return string
     * @throws \Exception
     */
    protected function handleApplicationContext()
    {
        $context = Helper\ApplicationContext::getCurrentApplicationContext();
        if (empty($context)) {
            $this->output->outputLine(PHP_EOL . 'Well that was quick. You have no application context set.');
            $this->output->outputLine('iw_master is only working when an application context has been set.' . PHP_EOL);

            if ($this->output->askConfirmation('Do you want to set the context? [Y/n] ')) {
                $this->output->outputLine(str_pad(' context:set ---', $this->output->getMaximumLineLength(), '-', STR_PAD_LEFT));
                $commandContextSet = $this->getApplication()->find('context:set');
                $commandContextSet->run(new \Symfony\Component\Console\Input\ArrayInput([]), $this->output->getOutput());
                $this->output->outputLine(str_pad(' / context:set ---', $this->output->getMaximumLineLength(), '-', STR_PAD_LEFT));
            } else {
                $this->output->outputLine('Can\'t proceed without set context.');
                return '';
            }
            sleep(1);
            $context = Helper\ApplicationContext::getCurrentApplicationContext();
        }
        return $context;
    }

    /**
     * Checks if given configPath is existing and ask for creating it, if it's not.
     * A proper configuration is required to continue installation process
     *
     * @param string $configPath
     * @throws \RuntimeException
     */
    protected function createEnvironmentConfigurationFileIfNotExisting($configPath)
    {
        if (!file_exists($configPath)) {
            $createFile = $this->output->askConfirmation('Create this file? [Y/n]');
            if (!$createFile) {
                throw new \RuntimeException(
                    'Can\'t proceed without an existing configuration file, matching the current application context.'
                );
            }
            $source = Helper\File::concatenatePathParts([
                getcwd(),
                'typo3conf',
                'ext',
                'iwm',
                'Resources',
                'Private',
                'ConfigurationTemplates',
                'Default.php'
            ]);
            $filesystem = new \Symfony\Component\Filesystem\Filesystem();
            $filesystem->copy($source, $configPath);
            $this->output->outputLine('Done.');
            $this->output->outputLine();
        }
    }

    /**
     * Asks for database credentials, checks database connection and stores the credentials
     * in given $configPath, on success.
     *
     * When the given configuration file already contains credentials, the method asks to reuse them.
     * Returns a valid DatabaseConnection on success.
     *
     * @param string $configPath
     * @param InputInterface $input
     * @return \TYPO3\CMS\Core\Database\DatabaseConnection
     * @TODO Refactor me!
     */
    protected function handleDatabaseCredentials($configPath, InputInterface $input)
    {
        $content = file_get_contents($configPath);
        preg_match(
            '/setDatabaseCredentials\(.*?\'(.*?)\',.*?\'(.*?)\',.*?\'(.*?)\',.*?\'(.*?)\'.*?\)/s',
            $content,
            $matches
        );

        $user = $matches[1];
        $pass = $matches[2];
        $database = $matches[3];
        $host = $matches[4];

        $continueWithExistingCredentials = false;
        if ($pass !== 'password' && ($user !== 'username' || $database !== 'db')) {
            $this->output->outputLine('Found database credentials in config file:');
            $this->output->outputTable(
                [[$host, $user, substr($pass, 0, 3) . '*******', $database]],
                ['Host', 'User', 'Pass', 'Database']
            );
            $continueWithExistingCredentials = $this->output->askConfirmation('Continue with these? [Y/n] ');
            $this->output->outputLine();
            if ($continueWithExistingCredentials) {
                $input->setOption('host', $host);
                $input->setOption('user', $user);
                $input->setOption('pass', $pass);
                $input->setOption('db', $database);
            }
        }

        if (!$continueWithExistingCredentials) {
            reenter:
            $this->output->outputLine('Enter the database credentials please:' . PHP_EOL);

            $input->setOption('host', $this->output->ask('Hostname [' . $input->getOption('host') . ']: ', $input->getOption('host')));
            $input->setOption('user', $this->output->ask('User [' . $input->getOption('user') . ']: ', $input->getOption('user')));
            $input->setOption('pass', $this->output->askHiddenResponse('Password: '));
            $input->setOption('db', $this->output->ask('Database [' . $input->getOption('db') . ']: ', $input->getOption('db')));
        }

        /** @var \TYPO3\CMS\Core\Database\DatabaseConnection $db */
        $db = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Database\DatabaseConnection::class);
        $db->setDatabaseHost($input->getOption('host'));
        $db->setDatabaseUsername($input->getOption('user'));
        $db->setDatabasePassword($input->getOption('pass'));
        $db->setDatabaseName($input->getOption('db'));
        if ($input->getOption('socket')) {
            $db->setDatabaseSocket($input->getOption('socket'));
        }
        if ($input->getOption('port')) {
            $db->setDatabasePort($input->getOption('port'));
        }

        $db->initialize();
        try {
            $db->connectDB();
        } catch (\Exception $e) {
            $this->output->outputLine($e->getMessage() . PHP_EOL);
            goto reenter;
        }

        if (!$continueWithExistingCredentials && $db->isConnected() && is_writable($configPath)) {
            // TODO: Refactor this!
            $params = [
                $input->getOption('user'),
                $input->getOption('pass'),
                $input->getOption('db'),
                $input->getOption('host'),
                $input->getOption('port'),
                $input->getOption('socket')
            ];

            $command = '$environment->setDatabaseCredentials(';
            foreach ($params as $param) {
                $command .= PHP_EOL . '        \'' . $param . '\',';
            }
            $command = rtrim($command, ',');
            $command .= PHP_EOL . '    );';

            $newContent = preg_replace(
                '/\$environment\-\>setDatabaseCredentials\(.*?\);/s',
                $command,
                $content
            );

            $save = $this->output->askConfirmation('Save database credentials to configuration file? [Y/n]');
            if ($save) {
                $status = file_put_contents($configPath, $newContent);
                if (!$status) {
                    throw new \RuntimeException('Can\'t store database credentials to "' . $configPath . '"!');
                }
                $this->output->outputLine('Saved.');
            }
        }
        return $db;
    }


    /**
     * Handles input of TYPO3 backend admin user credentials
     *
     * @return void
     */
    protected function handleAdminUser()
    {
        $this->output->outputLine('Please enter credentials for TYPO3 admin backend user:');
        $this->input->setOption(
            'admin-user-name',
            $this->output->ask('Admin username [' . $this->input->getOption('admin-user-name') . ']: ', $this->input->getOption('admin-user-name'))
        );
        $this->input->setOption(
            'admin-password',
            $this->output->ask('Admin password [' . $this->input->getOption('admin-password') . ']: ', $this->input->getOption('admin-password'))
        );
        $this->output->outputLine();
    }

    /**
     * Calls typo3_console to perform TYPO3 installation.
     *
     * On windows, the external tasks can't get triggered.
     * Instead a manual will get displayed what the next required steps are.
     *
     * @param InputInterface $input
     * @param DatabaseConnection $database
     * @return void
     */
    protected function runTypo3ConsoleInstaller(InputInterface $input, DatabaseConnection $database)
    {
        $createNewTable = 1;
        if ($input->getOption('db') && in_array($input->getOption('db'), $database->admin_get_dbs())) {
            $createNewTable = 0;
        }

        // Prepare options
        $options = [
            'database-user-name' => $input->getOption('user'),
            'database-user-password' => $input->getOption('pass'),
            'database-host-name' => $input->getOption('host'),
            'database-port' => $input->getOption('port'),
            'database-socket' => $input->getOption('socket'),
            'database-name' => $input->getOption('db'),
            'use-existing-database' => $createNewTable,
            'admin-user-name' => $input->getOption('admin-user-name'),
            'admin-password' => $input->getOption('admin-password'),

            'non-interactive' => '1',
        ];
        $optionLine = '';
        foreach ($options as $key => $value) {
            if ($value !== '') {
                $optionLine .= ' --' . $key . ' ' . $value;
            }
        }

        if (Helper\File::isWindowsOs()) {
            $this->output->outputLine(
                'Unfortunately Windows OS has some limitations, like the missing support of TTY mode.' . PHP_EOL .
                'So this script is not able to continue. These would be the next steps:' . PHP_EOL
            );

            $this->output->outputLine(
                '1. Call install script of typo3_console with TYPO3_PATH_WEB environment variable: ' . PHP_EOL .
                '   SET TYPO3_PATH_WEB=' . Helper\File::getTypo3RootPath() . '&&' . ' typo3cms.bat install:setup' . $optionLine
            );
            $this->output->outputLine();
            $this->output->outputLine(
                '2. Call typo3_console script to create PackageStates.php file:' . PHP_EOL .
                '   typo3cms.bat install:generatepackagestates'
            );
            $this->output->outputLine();
            $this->output->outputLine(
                '3. Call sync script for configured entities:' . PHP_EOL .
                '   iwm.bat update'
            );
            $this->output->outputLine();
            $this->output->outputLine(
                '4. Call finisher tasks in install script:' . PHP_EOL .
                '   iwm.bat install --finish'
            );
            $this->output->outputLine(PHP_EOL . 'Good luck and sorry for the inconveniences!');
            die;
        }

        $this->output->outputLine(
            str_pad(' typo3cms install:setup ---', $this->output->getMaximumLineLength(), '-', STR_PAD_LEFT)
        );

        // Create and run process
        $process = $this->executeProcess($this->getBinaryPath('typo3cms') . ' install:setup' . $optionLine);
        $this->output->output($process->getOutput());

        $this->output->outputLine(
            str_pad(' / typo3cms install:setup ---', $this->output->getMaximumLineLength(), '-', STR_PAD_LEFT)
        );
        $this->output->outputLine();
    }

    /**
     * Calls typo3_console to generate PackageStates.php based on given composer.json configuration
     *
     * @return void
     * @throws \Exception
     */
    protected function runTypo3ConsoleGeneratePackageStates()
    {
        $this->output->outputLine(str_pad(
            ' typo3cms install:generatepackagestates ---',
            $this->output->getMaximumLineLength(),
            '-',
            STR_PAD_LEFT
        ));

        // Create and run process
        $process = $this->executeProcess($this->getBinaryPath('typo3cms') . ' install:generatepackagestates');
        $this->output->output($process->getOutput());

        $this->output->outputLine(str_pad(
            ' / typo3cms install:generatepackagestates ---',
            $this->output->getMaximumLineLength(),
            '-',
            STR_PAD_LEFT
        ));
    }

    /**
     * Clears the database (by dropping all tables, asks first)
     *
     * @param DatabaseConnection $database
     * @return void
     */
    protected function handleDatabaseClearing(DatabaseConnection $database)
    {
        $amountTables = count($database->admin_get_tables());
        if ($amountTables > 0) {
            $cleanUp = $this->output->askConfirmation(
                'You have ' . $amountTables . ' tables in given database. It is recommended to start with an empty ' .
                'database.' . PHP_EOL . 'Drop all tables and data in all tables? (!!!) [y/N] ',
                false
            );
            if ($cleanUp) {
                $this->output->outputLine();
                $this->output->output('Dropping tables... ');
                foreach (array_keys($database->admin_get_tables()) as $tableName) {
                    $status = $database->admin_query('DROP TABLE `' . $tableName . '`;');
                    if (!$status) {
                        $this->output->outputLine('ERROR: Can\'t drop table ' . $tableName);
                    }
                }
                $this->output->outputLine('<info>OK</info>');
            } else {
                $this->output->outputLine('Proceeding without clearing database, on your own risk...');
            }
            $this->output->outputLine();
        }
    }

    /**
     * Handles the import of entities
     *
     * @return void
     * @throws \Exception
     */
    protected function handleEntitiesImport()
    {
        EntityManager::getInstance()->reset();
        $additionalConfiguration = Helper\File::getTypo3ConfPath() . 'AdditionalConfiguration.php';
        require($additionalConfiguration);

        if (EntityManager::getInstance()->hasEntries()) {
            $this->output->outputLine('Configured entities for the current environment found...');

            $processShowEntities = $this->executeProcess($this->getBinaryPath('iwm') . ' entities:update --dry-run');
            $this->output->output($processShowEntities->getOutput());

            $continue = $this->output->askConfirmation('Do you want to import them? [Y/n] ');
            if ($continue) {
                $this->output->outputLine();
                $this->output->outputLine('Importing...');

                $entitiesProcess = $this->executeProcess($this->getBinaryPath('iwm') . ' entities:update --force');
                $this->output->output($entitiesProcess->getOutput());

                $entitiesProcess = $this->executeProcess($this->getBinaryPath('iwm') . ' entities:update --force');
                $this->output->output($entitiesProcess->getOutput());

                $this->output->outputLine('<info>OK</info>');
            }
        }
    }

    /**
     * Removes settings from LocalConfiguration.php, which should get stored in environment settings,
     * instead of LocalConfiguration (like database credentials)
     *
     * @return bool True on success, otherwise false
     */
    protected function cleanUpLocalConfiguration()
    {
        $localConfiguration = Helper\File::getLocalConfigurationValues();
        unset($localConfiguration['DB']['database']);
        unset($localConfiguration['DB']['host']);
        unset($localConfiguration['DB']['password']);
        unset($localConfiguration['DB']['username']);
        unset($localConfiguration['SYS']['sitename']);

        if (!defined('LF')) {
            define('LF', PHP_EOL);
        }
        $result = GeneralUtility::writeFile(
            Helper\File::getTypo3ConfPath() . 'LocalConfiguration.php',
            '<?php' . PHP_EOL .
            'return ' .
            ArrayUtility::arrayExport(
                ArrayUtility::renumberKeysToAvoidLeapsIfKeysAreAllNumeric($localConfiguration)
            ) .
            ';' . PHP_EOL,
            true
        );
        return $result;
    }

    /**
     * Get welcome text
     *
     * @return string
     */
    protected function getWelcomeText()
    {
        return <<<TXT

Welcome,
        
this installer will ask you some questions in order to install TYPO3.
You need a database connection which TYPO3 can connect to.

For experts: You can also pass all data to options on command line and skip
the text input with -n for automated deployment. See help for all infos.
TXT;
    }

    /**
     * Get title
     *
     * @return string
     */
    protected function getHeaderTitle()
    {
        return <<<TXT
<comment>
+-----------------------------------------------------------------------------+
|                                                                             |
|               ~ <info>Welcome to iw_master installer for TYPO3 CMS</info> ~              |
|                                                                             |
+-----------------------------------------------------------------------------+
</comment>
TXT;
    }

    /**
     * Get bye text
     *
     * @return string
     */
    protected function getByeText()
    {
        return <<<TXT
        
The installation of TYPO3 was <info>successful</info>.
 
TXT;
    }
}
