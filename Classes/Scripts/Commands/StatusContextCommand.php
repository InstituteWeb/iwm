<?php
namespace InstituteWeb\Iwm\Scripts\Commands;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Iwm\Scripts\Helper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Status Context Command
 * Get status of TYPO3_CONTEXT environment variable.
 *
 * @package InstituteWeb\Iwm
 */
class StatusContextCommand extends AbstractCommand
{
    /**
     * Configure this command
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('status:context')
            ->setAliases(['context'])
            ->setDescription('Get status of TYPO3_CONTEXT environment variable.')
            ->addUsage('--context=current')
            ->addUsage('--context=Development/DevServer')

            ->addArgument(
                'path',
                InputArgument::OPTIONAL,
                'Unused in this Task'
            )

            ->addOption(
                'raw',
                'r',
                InputOption::VALUE_NONE,
                'When set just the raw application context string is returned, without further informations.'
            )
        ;
    }

    /**
     * Executes this command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->initIO($input, $output);

        $contextStatus = Helper\ApplicationContext::getCurrentApplicationContextStatus();
        $context = $contextStatus['context'];
        $type = $contextStatus['type'];

        if ($input->getOption('raw')) {
            $this->output->outputLine($context);
            return;
        }

        if (is_null($type)) {
            $this->output->outputLine('NO APPLICATION CONTEXT FOUND!');
            $this->output->outputLine('Please set TYPO3_CONTEXT environment variable.');
            return;
        }

        $this->output->outputLine('Current context: <comment>' . $context . '</comment>');
    }
}
