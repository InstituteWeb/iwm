<?php
namespace InstituteWeb\Iwm\Scripts\Commands;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Iwm\Scripts\Helper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 *
 * @package Iwm\Iwm
 */
class ContextSetCommand extends AbstractCommand
{
    /**
     * Configure this command
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('context:set')
            ->setAliases(['context-set'])
            ->setDescription('Sets TYPO3_CONTEXT to .env file.')
            ->setHelp('You can apply the TYPO3_CONTEXT environment variable in .env file with this tool.')
            ->addUsage('Development/DevServer')

            ->addArgument(
                'context',
                InputArgument::OPTIONAL,
                'The context which should get set (e.g. "Development/DevServer")'
            )
        ;
    }

    /**
     * Executes this command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->initIO($input, $output);

        $context = $input->getArgument('context');

        if (!$context) {
            $context = $this->handleEmptyContext($context, $input);
        }

        $dotEnvWriter = new Helper\DotEnvWriter(Helper\File::concatenatePathParts([getcwd(), '.env']));
        if ($dotEnvWriter->get('TYPO3_CONTEXT')) {
            if ($dotEnvWriter->get('TYPO3_CONTEXT') === $context) {
                $this->output->outputLine(
                    'Set TYPO3_CONTEXT is already "<info>' . $context . '</info>". Nothing to do here.'
                );
                return;
            }
            $continue = $this->output->askConfirmation(
                '.env file has already TYPO3_CONTEXT set, do you want to update the current value <info>' .
                $dotEnvWriter->get('TYPO3_CONTEXT') . '</info> with <info>' . $context . '</info>? [Y/n] '
            );
            if (!$continue) {
                return;
            }
        }
        $dotEnvWriter->set('TYPO3_CONTEXT', $context)->save();
        $this->output->outputLine(
            'Successfully set context to <info>' . $context . '</info> in <info>' . $dotEnvWriter->getPath() . '</info>'
        );
        putenv('TYPO3_CONTEXT=' . $context);
    }

    /**
     * When arguments are missing this method asks for them.
     *
     * @param string $context
     * @return string $context
     */
    protected function handleEmptyContext($context)
    {
        if (!$context) {
            $context = $this->output->askAndValidate(
                'Enter value for TYPO3_CONTEXT [Development/DevServer]: ',
                function ($value) {
                    if (strpos($value, 'Development') === false &&
                        strpos($value, 'Testing') === false &&
                        strpos($value, 'Production') === false
                    ) {
                        throw new \InvalidArgumentException(
                            'The context must always begin with "Development", "Testing" or "Production"'
                        );
                    }

                    $value = trim($value, '/');

                    if (strpos($value, '/') === false) {
                        throw new \InvalidArgumentException(
                            'The context must contain at least one sub context, to be able to point to ' .
                            'a configuration file for the environment.'
                        );
                    }

                    return $value;
                },
                false,
                'Development/DevServer'
            );
        }
        return $context;
    }
}
