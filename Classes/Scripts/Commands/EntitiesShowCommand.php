<?php
namespace InstituteWeb\Iwm\Scripts\Commands;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Iwm\Environments\DataProvider\EntityManager;
use InstituteWeb\Iwm\Environments\DataProvider\Models;
use InstituteWeb\Iwm\Environments\DataProvider\Modifiers\AbstractModifier;
use InstituteWeb\Iwm\Environments\Environment;
use InstituteWeb\Iwm\Scripts\Helper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Tree\Node\Node;

/**
 * Compares entities in current context with database and inserts/updates them
 *
 * @package Iwm\Iwm
 */
class EntitiesShowCommand extends AbstractCommand
{
    /**
     * Configure this command
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('entities:show')
            ->setAliases(['show'])
            ->setDescription('Get tree view of configured entities.')

            ->addArgument(
                'path',
                InputArgument::OPTIONAL,
                'Path to check.',
                Helper\File::concatenatePathParts([getcwd(), 'typo3conf', 'Environments'])
            )
        ;
    }

    /**
     * Executes this command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void|int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->initIO($input, $output);

        $this->getPhpConfigurationFiles($this->input->getArgument('path'));
        $rootNode = EntityManager::getInstance()->getAllAsNestedNodes();

        // Processing modifiers with high priority
        EntityManager::getInstance()->processModifiers(AbstractModifier::PRIORITY_HIGH);

        // Build and output table, based on nested items in rootNode
        $tableData = [];
        $this->buildTableDataWithNodes($rootNode, $tableData);

        $this->output->outputLine('Found entities');
        $this->output->outputLine('in <comment>' . realpath($this->input->getArgument('path')) . '</comment>');
        $this->output->outputTable(
            $tableData,
            [
                'Tree (Record Type)',
                'Label',
                'Identifier',
                'Level',
                'Sorting',
                'Configured in',
                'Mapped uid',
                'Existing',
                'In sync'
            ]
        );
    }

    /**
     * Recursive method to fill in rows in given, referenced $table array
     *
     * @param Node $node Node to start to build table from
     * @param array $table Referenced table array
     * @param bool $shortMode Does not show "sorting", "existing" and "in sync" options
     * @param int $depth Internal
     */
    protected function buildTableDataWithNodes(Node $node, array &$table, $shortMode = false, $depth = 0)
    {
        /** @var Node $childNode */
        foreach ($node->getChildren() as $childNode) {
            /** @var Models\AbstractSystemEntryModel $entity */
            $entity = $childNode->getValue();
            $indicator = count($childNode->getChildren()) > 0 ? '+' : ($childNode->getDepth() == 1 ? '-' : '-');

            $objectType = substr(get_class($entity), strlen('InstituteWeb\Iwm\Environments\DataProvider\Models') + 1);

            $entitySorting = null;
            if (method_exists($entity, 'getSorting')) {
                $entitySorting = $entity->getSorting() === INF ? '' : $entity->getSorting();
            }

            $table[] = array_merge(
                [
                    str_repeat('    ', $depth) . $indicator . ' ' . $objectType,
                    $entity->getLabel(),
                    strpos($entity->getIdentifier(), 'new_') === 0 ? '': $entity->getIdentifier()
                ],
                !$shortMode ? [
                    $childNode->getDepth(),
                    $entitySorting,
                    substr(EntityManager::getInstance()->getSourceByEntity($entity), strlen($this->input->getArgument('path')) + 1),
                ] : [],
                $this->getDatabaseSyncStatus($entity, $shortMode)
            );

            if (!$childNode->isLeaf()) {
                $this->buildTableDataWithNodes($childNode, $table, $shortMode, $depth + 1);
            }
        }
    }

    /**
     * Extracts from given root node the entities and compares them with actual database values
     *
     * @param Models\AbstractSystemEntryModel $entity
     * @param bool $shortMode
     * @return array
     * @throws \Exception
     * @internal param Node $entitiesRootNode
     */
    protected function getDatabaseSyncStatus(Models\AbstractSystemEntryModel $entity, $shortMode = false)
    {
        $mappedItems = Environment::getInstance()->getEntityMappings();

        $output = [];
        $isMapped = array_key_exists($entity->getIdentifier(), $mappedItems);
        $output['mappings'] = $isMapped ? $mappedItems[$entity->getIdentifier()] : '-';
        $output['existing'] = 'missing';
        $output['insync'] = 'No';

        if (!$shortMode && $isMapped) {
            $row = Environment::getInstance()->getDatabaseConnection()->exec_SELECTgetSingleRow(
                '*',
                $entity->getTablename(),
                'uid = ' . intval($mappedItems[$entity->getIdentifier()])
            );
            $diff = [];
            if (is_array($row)) {
                $output['existing'] = 'OK';
                $diff = $entity->getDiff($row, $mappedItems);
            }
            $output['insync'] = count(array_keys($diff)) === 0 ? 'Yes' : 'No, ' . count(array_keys($diff)) . ' diff(s)';
        }
        if ($shortMode) {
            unset($output['existing']);
            unset($output['insync']);
        }
        return $output;
    }
}
