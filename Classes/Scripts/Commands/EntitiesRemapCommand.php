<?php
namespace InstituteWeb\Iwm\Scripts\Commands;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Iwm\Environments\DataProvider\EntityManager;
use InstituteWeb\Iwm\Environments\DataProvider\Models;
use InstituteWeb\Iwm\Environments\DataProvider\Modifiers\AbstractModifier;
use InstituteWeb\Iwm\Environments\Environment;
use InstituteWeb\Iwm\Scripts\Helper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Compares entities in current context with database and inserts/updates them
 *
 * @package Iwm\Iwm
 */
class EntitiesRemapCommand extends EntitiesShowCommand
{
    /**
     * Configure this command
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('entities:remap')
            ->setAliases(['remap'])
            ->setDescription('Change uid mapping of entities.')
            ->addUsage('--repeat')

            ->addArgument(
                'path',
                InputArgument::OPTIONAL,
                'Path to check.',
                Helper\File::concatenatePathParts([getcwd(), 'typo3conf', 'Environments'])
            )
            ->addOption(
                'repeat',
                'r',
                \Symfony\Component\Console\Input\InputOption::VALUE_NONE,
                'Restart command after updating identifier.'
            )
        ;
    }

    /**
     * Executes this command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void|int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->initIO($input, $output);

        $this->getPhpConfigurationFiles($this->input->getArgument('path'));
        $rootNode = EntityManager::getInstance()->getAllAsNestedNodes();

        // Processing modifiers with high priority
        EntityManager::getInstance()->processModifiers(AbstractModifier::PRIORITY_HIGH);
        
        // Build and output table, based on nested items in rootNode
        $tableData = [];
        $this->buildTableDataWithNodes($rootNode, $tableData, true);

        $this->output->outputLine('Found entities');
        $this->output->outputLine('in <comment>' . realpath($this->input->getArgument('path')) . '</comment>');
        $this->output->outputTable(
            $tableData,
            [
                'Tree (Record Type)',
                'Label',
                'Identifier',
                'Mapped uid'
            ],
            true
        );
        $this->output->outputLine('Select "0" to exit.');

        $validateItemSelection = function ($value) use ($tableData) {
            if ($value < 0 || $value > count($tableData)) {
                throw new \UnexpectedValueException('No valid id entered!');
            }
            return $value;
        };
        $id = (int) $this->output->askAndValidate('Which item (id) would you like to re-map? ', $validateItemSelection);
        if ($id === 0) {
            $this->output->outputLine('Bye.');
            return;
        }
        $item = array_values($tableData)[$id - 1];
        $identifier = $item[2];

        $validateNewUid = function ($value) {
            if (!is_numeric($value)) {
                throw new \UnexpectedValueException('Please enter a valid uid.');
            }
            return $value;
        };
        $newUid = $this->output->askAndValidate('New uid for "' . $identifier . '"? ', $validateNewUid);

        $entityMappings = Environment::getInstance()->getEntityMappings();
        $entityMappings[$identifier] = $newUid;
        Environment::getInstance()->setEntityMappings($entityMappings);

        $this->output->outputLine('Done.');

        if ($input->getOption('repeat')) {
            $command = $this->getApplication()->find($this->getName());
            $command->run($input, $output);
        }
    }
}
