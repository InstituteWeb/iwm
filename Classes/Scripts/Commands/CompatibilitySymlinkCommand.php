<?php
namespace InstituteWeb\Iwm\Scripts\Commands;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Iwm\Scripts\Helper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

/**
 *
 * @package Iwm\Iwm
 */
class CompatibilitySymlinkCommand extends AbstractCommand
{
    /**
     * Configure this command
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('compatibility:symlink')
            ->setDescription('Replaces index.php symlink in root with actual index.php')

            ->addOption(
                'restore',
                'r',
                InputOption::VALUE_NONE,
                'When set the symlink is getting restored. On Windows requires admin privileges.'
            )
        ;
    }

    /**
     * Executes this command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     * @throws \RuntimeException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->initIO($input, $output);

        $indexRootPath = Helper\File::concatenatePathParts([getcwd(), 'index.php']);
        $indexSourcesPath = Helper\File::concatenatePathParts([getcwd(), 'typo3_src', 'index.php']);

        if (!is_writable(dirname($indexRootPath))) {
            throw new \RuntimeException('File not writable: ' . $indexRootPath);
        }

        if (!file_exists($indexSourcesPath)) {
            throw new \RuntimeException('File not existing: ' . $indexSourcesPath);
        }

        $continue = $this->output->askConfirmation(
            'File "' . $indexRootPath . '" is going to be replaced. Continue? [Y/n] ',
            true
        );
        if ($continue) {
            if (file_exists($indexRootPath)) {
                $unlinkStatus = unlink($indexRootPath);
                if (!$unlinkStatus) {
                    throw new \RuntimeException('Deleting failed! File: ' . $indexRootPath);
                }
            }

            if (!$input->getOption('restore')) {
                // Remove symlink
                $copyStatus = copy($indexSourcesPath, $indexRootPath);
                if (!$copyStatus) {
                    throw new \RuntimeException(
                        'Copying from "' . $indexSourcesPath . '" to "' . $indexRootPath . '" failed!'
                    );
                }
                $this->output->outputLine('index.php has been successfully replaced.');
            } else {
                // Restore symlink
                $file = new Filesystem();
                $file->symlink($indexSourcesPath, $indexRootPath, false);
                $this->output->outputLine('index.php has been successfully restored to symlink.');
            }

        }
    }
}
