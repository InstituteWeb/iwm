<?php
namespace InstituteWeb\Iwm\Scripts\Commands;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Iwm\Scripts\Helper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 *
 * @package Iwm\Iwm
 */
class ContextRemoveCommand extends AbstractCommand
{
    /**
     * Configure this command
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('context:remove')
            ->setDescription('Removes the TYPO3_CONTEXT from .env file.')
        ;
    }

    /**
     * Executes this command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->initIO($input, $output);

        $continue = $this->output->askConfirmation(
            'Do you want to remove all set TYPO3_CONTEXT variables in "' . getcwd() . '"? [Y/n] ',
            true
        );
        if (!$continue) {
            return;
        }

        // Remove context
        $touchedFiles = Helper\ApplicationContext::removeApplicationContext(getcwd());
        if (count($touchedFiles) === 0) {
            $this->output->outputLine('No TYPO3_CONTEXT found. Did not touch anything.');
            return;
        }

        // Output status
        if (in_array(false, $touchedFiles, true)) {
            $this->output->outputLine('Remove of TYPO3_CONTEXT failed:');
        } else {
            $this->output->outputLine('Remove of TYPO3_CONTEXT succeeded:');
        }
        foreach ($touchedFiles as $file => $status) {
            $this->output->outputLine(' ' . ($status ? '[<info>OK</info>] ' : '[<error>ERR</error>]') . ' ' . $file);
        }
    }
}
