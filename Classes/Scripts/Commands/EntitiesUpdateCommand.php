<?php
namespace InstituteWeb\Iwm\Scripts\Commands;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Iwm\Environments\DataProvider\EntityManager;
use InstituteWeb\Iwm\Environments\DataProvider\Models\AbstractSystemEntryModel;
use InstituteWeb\Iwm\Environments\DataProvider\Modifiers\AbstractModifier;
use InstituteWeb\Iwm\Environments\Environment;
use InstituteWeb\Iwm\Scripts\Helper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use Tree\Node\Node;
use Tree\Visitor\PreOrderVisitor;
use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\Registry;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Compares entities in current context with database and inserts/updates them
 *
 * @package Iwm\Iwm
 */
class EntitiesUpdateCommand extends AbstractCommand
{
    /**
     * Configure this command
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('entities:update')
            ->setAliases(['update'])
            ->setDescription('Put configured entities to database')

            ->addOption(
                'force',
                'f',
                InputOption::VALUE_NONE,
                'When set changes get applied to database.'
            )
            ->addOption(
                'dry-run',
                'd',
                InputOption::VALUE_NONE,
                'When set just the differences are output in a table.'
            )
            ->addOption(
                'all-details',
                'a',
                InputOption::VALUE_NONE,
                'When set also long text values get output in table output.'
            )
            ->addOption(
                'update-only',
                'u',
                InputOption::VALUE_NONE,
                'When set just update script is executed. Insert part is skipped.'
            )
        ;
    }

    /**
     * Executes this command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void|int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->initIO($input, $output);

        // Get current context (from .env)
        $context = Helper\ApplicationContext::getCurrentApplicationContext();
        if (getenv('TYPO3_CONTEXT') !== $context) {
            putenv('TYPO3_CONTEXT=' . $context);
        }

        // Include configuration files
        EntityManager::getInstance()->reset();
        $additionalConfiguration = Helper\File::getTypo3ConfPath() . 'AdditionalConfiguration.php';
        require ($additionalConfiguration);
        
        if (!EntityManager::getInstance()->hasEntries()) {
            $this->output->outputLine('No configured entries found!');
            return;
        }

        // Set up database connection
        $GLOBALS['TYPO3_DB'] = $database = Environment::getInstance()->getDatabaseConnection();

        // Fetch all configured entities, nested
        $configuredEntitiesRootNode = Environment::getInstance()->unmock(function () {
            $entityRootNode = EntityManager::getInstance()->getAllAsNestedNodes();
            return $entityRootNode;
        });

        $mappedItems = Environment::getInstance()->getEntityMappings();

        // Processing modifiers with high priority
        EntityManager::getInstance()->processModifiers(AbstractModifier::PRIORITY_HIGH);

        // Compares configured entities with database
        list($insert, $update) = $this->getInsertAndUpdateStatementsFromEntityRootNode(
            $configuredEntitiesRootNode,
            $mappedItems,
            $database
        );

        // Cancel if all is in sync
        if (empty($update) && empty($insert)) {
            $this->output->outputLine('All entities are in sync with given configuration.');
            return;
        }

        if (!$this->input->getOption('force')) {
            // Display table with records to insert/update for each table
            $tableData = $this->buildTableDataOfAffectedRecordsAmount($insert, $update);
            if (!empty($tableData)) {
                $this->output->outputLine('Entry amount per table:');
                $this->output->outputTable($tableData, ['Table', 'Insert entries', 'Update entries']);
            }

            // Display differences in existing entities
            if (!empty($update)) {
                $tableData = $this->buildTableDataForModifiedEntities($update);
                $this->output->outputLine();
                $this->output->outputLine('Found differences in existing entities:');
                $this->output->outputTable($tableData, ['Table', 'Where', 'Field',  'Current Value', 'New Value    ']);
            }
        }

        if ($this->input->getOption('dry-run')) {
            return;
        }

        // Ask to continue
        if (!$this->input->getOption('force')) {
            $continue = $this->output->askConfirmation('Do you want to apply entities to Database? [Y/n] ', true);
            if (!$continue) {
                return;
            }
        }

        $this->output->outputLine();
        $insertedEntities = false;
        if (!$this->input->getOption('update-only')) {
            if (!empty($insert)) {
                // Insert new entities to DB
                $this->output->output('Inserting entities... ');
                $mappedItems = $this->perfomInsertStatements($insert, $mappedItems, $database);
                $this->output->outputLine('<info>OK</info>');

                // Store inserted uids to mappedEntities in sys_registry
                $this->output->output('Store inserted uids to sys_registry... ');
                Environment::getInstance()->setEntityMappings($mappedItems);
                $insertedEntities = true;
                $this->output->outputLine('<info>OK</info>');
            } else {
                $this->output->outputLine('No entities to insert.');
            }
        }

        $mappedItems = Environment::getInstance()->getEntityMappings();

        // Compares again, with previously added entity uid mappings
        list($insert, $update) = $this->getInsertAndUpdateStatementsFromEntityRootNode(
            $configuredEntitiesRootNode,
            $mappedItems,
            $database
        );

        if (!$this->input->getOption('update-only') && $insertedEntities && !empty($update)) {
            $this->executeProcess($this->getBinaryPath('iwm') . ' update -u -f');
            return;
        }

        // Update existing entries with differences in DB
        if (!empty($update)) {
            $this->output->output('Updating entities... ');
            $this->performUpdateStatements($update, $database);
            $this->output->outputLine('<info>OK</info>');
        } else {
            $this->output->outputLine('No entities to update.');
        }

        $this->output->outputLine();
        $this->output->outputLine('Import of entities successful. Done.');
    }

    /**
     * Extracts from given root node the entities and compares them with actual database values
     *
     * @param Node $entitiesRootNode
     * @param array $mappedItems
     * @param DatabaseConnection $database
     * @return array
     * @throws \RuntimeException
     */
    protected function getInsertAndUpdateStatementsFromEntityRootNode(
        Node $entitiesRootNode,
        array $mappedItems,
        DatabaseConnection $database
    ) {
        $insert = [];
        $update = [];

        /** @var Node $node */
        foreach ($entitiesRootNode->accept(new PreOrderVisitor()) as $node) {
            $entity = $node->getValue();
            if ($entity instanceof AbstractSystemEntryModel) {
                if (is_null($entity->getTablename())) {
                    continue;
                }
                if (!in_array($entity->getTablename(), array_keys($database->admin_get_tables()))) {
                    throw new \RuntimeException('No table "' . $entity->getTablename() . '" existing in database!');
                }

                $availableFields = array_keys($database->admin_get_fields($entity->getTablename()));
                foreach ($entity->getFieldArray() as $key => $value) {
                    if (!in_array($key, $availableFields)) {
                        throw new \RuntimeException('Unknown field "' . $key . '" in table "' . $entity->getTablename() . '"!');
                    }
                }

                $fieldArray = $entity->getFieldArray($mappedItems);
                $existingRow = $database->exec_SELECTgetSingleRow('*', $entity->getTablename(), 'uid=' . $fieldArray['uid']);
                if ($existingRow) {
                    $diff = $entity->getDiff($existingRow, $mappedItems);
                    if (count(array_keys($diff))) {
                        $update[] = [
                            'table' => $entity->getTablename(),
                            'where' => 'uid = ' . $fieldArray['uid'],
                            'differences' => $diff,
                            'existingRow' => $existingRow
                        ];
                    }
                } else {
                    $insert[] = [
                        'table' => $entity->getTablename(),
                        'entity' => $entity,
                        'fields' => $fieldArray
                    ];
                }
            }
        }
        return [$insert, $update];
    }

    /**
     * Builds table data array with amount of affected records,
     * separated by table and splitted to "insert" and "update".
     *
     * @param array $insert
     * @param array $update
     * @return array Ready to use array to output table
     */
    protected function buildTableDataOfAffectedRecordsAmount(array $insert, array $update)
    {
        $tables = [];
        foreach ($insert as $singleInsert) {
            if (!isset($tables[$singleInsert['table']])) {
                $tables[$singleInsert['table']] = ['insert' => 1, 'update' => 0];
            } else {
                $tables[$singleInsert['table']]['insert'] = $tables[$singleInsert['table']]['insert'] + 1;
            }
        }
        foreach ($update as $singleUpdate) {
            if (!isset($tables[$singleUpdate['table']])) {
                $tables[$singleUpdate['table']] = ['insert' => 0, 'update' => 1];
            } else {
                $tables[$singleUpdate['table']]['update'] = $tables[$singleUpdate['table']]['update'] + 1;
            }
        }

        $rows = [];
        foreach ($tables as $table => $amounts) {
            $rows[] = [
                $table,
                $amounts['insert'],
                $amounts['update']
            ];
        }
        return $rows;
    }

    /**
     * Builds table data array with differences between configured entities and existing ones in database.
     *
     * @param array $update
     * @return array Ready to use array to output table
     */
    protected function buildTableDataForModifiedEntities(array $update)
    {
        $tables = [];
        foreach ($update as $singleUpdate) {
            $keys = [];
            $currentValues = [];
            $newValues = [];
            foreach ($singleUpdate['differences'] as $key => $newValue) {
                $keys[] = $key;
                $currentValues[] = $this->handleConfigurationValue($singleUpdate['existingRow'][$key]);
                $newValues[] = $this->handleConfigurationValue($newValue);
            }
            $tables[] = [
                $singleUpdate['table'],
                $singleUpdate['where'],
                implode(PHP_EOL, $keys),
                implode(PHP_EOL, $currentValues),
                implode(PHP_EOL, $newValues),
            ];
        }
        return $tables;
    }

    /**
     * Perform INSERT statements in database
     *
     * @param array $insert
     * @param array $mappedItems
     * @param DatabaseConnection $database
     * @return array Entity mapping
     * @throws \RuntimeException
     */
    protected function perfomInsertStatements(array $insert, array $mappedItems, DatabaseConnection $database)
    {
        foreach ($insert as $singleInsert) {
            /** @var AbstractSystemEntryModel $entity */
            $entity = $singleInsert['entity'];
            $status = $database->exec_INSERTquery(
                $singleInsert['table'],
                $entity->getFieldArray($mappedItems)
            );

            if (!$status) {
                throw new \RuntimeException('Error while inserting entities to database: ' . $database->sql_error());
            }
            if (!isset($mappedItems[$entity->getIdentifier()])) {
                $mappedItems[$entity->getIdentifier()] = $database->sql_insert_id();
            }
        }
        return $mappedItems;
    }

    /**
     * Perform UPDATE statements in database
     *
     * @param array $update
     * @param DatabaseConnection $database
     * @return void
     * @throws \RuntimeException
     */
    protected function performUpdateStatements(array $update, DatabaseConnection $database)
    {
        foreach ($update as $singleUpdate) {
            $status = $database->exec_UPDATEquery(
                $singleUpdate['table'],
                $singleUpdate['where'],
                $singleUpdate['differences']
            );
            if (!$status) {
                throw new \RuntimeException(
                    'Error while updating entity (' . $singleUpdate['where'] . '): ' . $database->sql_error()
                );
            }
        }
    }
}
