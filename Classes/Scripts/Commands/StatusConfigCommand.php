<?php
namespace InstituteWeb\Iwm\Scripts\Commands;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Iwm\Scripts\Helper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TYPO3\CMS\Core\Utility\ArrayUtility;

/**
 * Status Entities Command
 * Get status of configured entities
 *
 * @package InstituteWeb\Iwm
 */
class StatusConfigCommand extends AbstractCommand
{
    /**
     * Configure this command
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('status:config')
            ->setAliases(['config'])
            ->setDescription('Lists all php configuration files and their status.')

            ->addArgument(
                'path',
                InputArgument::OPTIONAL,
                'Path to check.',
                Helper\File::concatenatePathParts([getcwd(), 'typo3conf', 'Environments'])
            )
        ;
    }

    /**
     * Executes this command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->initIO($input, $output);

        $files = $this->getPhpConfigurationFiles($this->input->getArgument('path'), true, true);
        $this->buildAndOutputConfigurationFileStatus($files);
        $this->buildAndOutputSettingsTable($files);
    }

    /**
     * Returns one array of all made configurations in all given files.
     * We need this to compare the settings between the files.
     *
     * @param array $configurationFiles
     * @return array
     */
    protected function getAllConfigsFromGivenFiles($configurationFiles)
    {
        $usedSettings = [];
        foreach ($configurationFiles as $file) {
            $usedSettings = array_merge($usedSettings, array_keys(ArrayUtility::flatten($file['configuration'])));
        }
        return array_unique($usedSettings);
    }

    /**
     * Builds table with all given configuration files and their status
     *
     * @param array $files
     * @return void
     */
    protected function buildAndOutputConfigurationFileStatus(array $files)
    {
        $table = [];
        $i = 1;
        foreach ($files as $file) {
            $status = 'OK';
            if ($file['error']) {
                $status = trim('ERR: ' . preg_replace('/File:.*/', '', $file['error']));
            }
            $item = [
                substr(realpath($file['path']), strlen(realpath($this->input->getArgument('path'))) + 1),
                !empty($file['configuration']) ? 'Yes' : 'No',
                !empty($file['entities']) ? 'Yes (' . count($file['entities']) . ' items)' : 'No',
                $status
            ];
            array_unshift($item, $i++);
            $table[] = $item;
        }

        $this->output->outputLine('Found PHP configuration files');
        $this->output->outputLine('in <comment>' . realpath($this->input->getArgument('path')) . '</comment>');
        $columns = ['File', 'Has config', 'Has entities', 'Status'];
        array_unshift($columns, '#');

        $this->output->outputTable($table, $columns);
    }

    /**
     * Build table with settings of all given configuration files
     *
     * @param array $files PHP Configuration files
     * @return void
     */
    protected function buildAndOutputSettingsTable(array $files)
    {
        $table = [];
        $usedSettings = $this->getAllConfigsFromGivenFiles($files);
        foreach ($usedSettings as $usedSetting) {
            $row = [$usedSetting];
            foreach ($files as $file) {
                $flatConfigurations = ArrayUtility::flatten($file['configuration']);
                if (count($flatConfigurations) === 0) {
                    continue;
                }

                $value = $flatConfigurations[$usedSetting];
                if (empty($value)) {
                    $localConfigurationValues = Helper\File::getLocalConfigurationValues();
                    if (isset($localConfigurationValues[$usedSetting])) {
                        $value = '*' . $this->handleConfigurationValue($localConfigurationValues[$usedSetting]) . '*';
                    } else {
                        $defaultConfigurationValues = Helper\File::getDefaultConfigurationValues();
                        if (isset($defaultConfigurationValues[$usedSetting])) {
                            $value = '**' . $this->handleConfigurationValue($defaultConfigurationValues[$usedSetting]) . '**';
                        }
                    }
                }

                if ($usedSetting === 'DB.password') {
                    $value = '******';
                }
                $row[] = $value;
            }
            $table[] = $row;
        }

        $columns = ['Settings'];
        foreach ($files as $file) {
            $flatConfigurations = ArrayUtility::flatten($file['configuration']);
            if (count($flatConfigurations) === 0) {
                continue;
            }
            $columns[] = substr($file['path'], strlen($this->input->getArgument('path')) + 1);
        }

        $this->output->outputLine();
        $this->output->outputLine('Settings by configuration file:');
        $this->output->outputTable($table, $columns);
        $this->output->outputLine('(**) Setting from TYPO3 Default Configuration');
        $this->output->outputLine(' (*) Setting from LocalConfiguration.php');
    }
}
