<?php
namespace InstituteWeb\Iwm\Scripts\Commands;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Iwm\Environments\DataProvider\EntityManager;
use InstituteWeb\Iwm\Environments\Environment;
use InstituteWeb\Iwm\Scripts\Helper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Process\Process;
use TYPO3\CMS\Core\Utility\ArrayUtility;

/**
 * Abstract Command
 *
 * @package Iwm\Iwm
 */
abstract class AbstractCommand extends \Symfony\Component\Console\Command\Command
{
    /**
     * Shortcut for DIRECTORY_SEPARATOR
     */
    const DS = DIRECTORY_SEPARATOR;

    /**
     * @var \InstituteWeb\Iwm\Scripts\Helper\ConsoleOutput
     */
    protected $output;

    /**
     * @var InputInterface
     */
    protected $input;

    /**
     * @var array
     */
    protected static $phpConfigurationArrays = [];

    /**
     * EnvironmentCommand constructor.
     *
     * @param string|null $name
     */
    public function __construct($name = null)
    {
        parent::__construct($name);
    }

    /**
     * Initializes $this->input and $this->output
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function initIO(InputInterface $input, OutputInterface $output)
    {
        error_reporting(E_ALL ^ E_NOTICE);
        $this->input = $input;
        $this->output = new Helper\ConsoleOutput($input, $output);
    }

    /**
     * Checks given path for php configuration files and validates them.
     * Returns an array of entries.
     *
     * When $includeInaccurateItems is set also configuration files with errors will get returned.
     *
     * @param string $path Absolute path to folder which contains php configurations
     * @param bool $includeInaccurateItems When set the returned array also includes files with errors
     * @param bool $ignoreContext When set, the context is ignored and all files are returned
     * @return array of arrays with keys "path", "constants", "configuration" and "error"
     */
    protected function getPhpConfigurationFiles($path, $includeInaccurateItems = false, $ignoreContext = false)
    {
        $files = [];
        if (!$ignoreContext) {
            $files = $this->followPhpConfigurationFilesInAdditionalConfiguartion();
        }
        if (empty($files)) {
            $finder = new Finder();
            $files = $finder
                ->files()
                ->in($path)
                ->name('*.php')
                ->sort(function () {
                    return 0;
                });

            if ($files->count() > 100) {
                throw new \RuntimeException(
                    'Found ' . $files->count() . ' php files. This seems to be too much for configuration files only. ' .
                    'Sure that you are in the right directory?' . PHP_EOL . 'Path: ' . realpath($path)
                );
            }
        }

        $items = [];
        EntityManager::getInstance()->reset();
        /** @var SplFileInfo $file */
        foreach ($files as $file) {
            try {
                EntityManager::getInstance()->setCurrentSource($file);
                $environment = Environment::getInstance([], true);
                $environment->typo3ConfVars = [];
                $environment->callPhpConfigFile((string) $file);

                $items[(string) $file] = [
                    'path' => (string) $file,
                    'entities' => EntityManager::getInstance()->getAll(),
                    'configuration' => $environment->typo3ConfVars,
                    'error' => false
                ];
                EntityManager::getInstance()->setCurrentSource('');
            } catch (\RuntimeException $exception) {
                if ($includeInaccurateItems) {
                    $items[] = [
                        'path' => (string) $file,
                        'entities' => [],
                        'configuration' => [],
                        'error' => $exception->getMessage()
                    ];
                }
            }
        }
        $itemsInCurrentOrder = [];
        foreach ($files as $loadedFile) {
            $itemsInCurrentOrder[(string) $loadedFile] = $items[(string) $loadedFile];
        }
        return $itemsInCurrentOrder;
    }

    /**
     * @return array with list of files which has been included
     */
    protected function followPhpConfigurationFilesInAdditionalConfiguartion()
    {
        $GLOBALS = ['TYPO3_CONF_VARS' => []];
        Environment::getInstance([], true);
        $additionalConfiguration = Helper\File::getTypo3ConfPath() . 'AdditionalConfiguration.php';
        require ($additionalConfiguration);

        return Environment::getInstance()->getCalledPhpConfigFiles();
    }

    /**
     * Handles given values for cli output
     *
     * @param string $value
     * @return string
     */
    protected function handleConfigurationValue($value)
    {
        if (is_bool($value)) {
            return $value ? 'true' : 'false';
        }
        if (is_array($value)) {
            return 'Array[]';
        }

        if (!is_string($value) && !is_numeric($value) && empty($value)) {
            return '-';
        }

        if ($this->input->hasOption('all-details') && $this->input->getOption('all-details')) {
        }

        if (is_string($value) && strlen($value) > 40) {
            return '[TEXT]';
        }

        return $value;
    }

    /**
     * Runs given command with "php" (PHP_BINARY) prepended.
     *
     * @param string $processCall e.g. "iwm entities:update --dry-run"
     * @return Process
     * @throws \Exception
     */
    protected function executeProcess($processCall)
    {
        if (Helper\File::isWindowsOs()) {
            throw new \Exception('Can\'t execute processes on Windows OS, due missing TTY support.');
        }
        $process = new Process(PHP_BINARY . ' ' . $processCall);
        $process->setTimeout(null);
        $process->setTty(true);
        $process->run();
        return $process;
    }

    /**
     * Returns binary path, configured in composer.json
     *
     * @param string $binary Name of binary (without file extension!)
     * @return bool|string
     */
    protected function getBinaryPath($binary)
    {
        $composerJson = @json_decode(file_get_contents(getcwd() . self::DS . 'composer.json'), true);
        if ($composerJson) {
            if (isset($composerJson['config']['bin-dir'])) {
                $binDir = $composerJson['config']['bin-dir'];
            } else {
                $binDir = 'vendor' . self::DS . 'bin';
            }
            $binFile = Helper\File::isWindowsOs() ? $binary . '.bat' : $binary;

            $path = getcwd() . self::DS . $binDir . self::DS . $binFile;
            if (file_exists($path) && is_executable($path)) {
                return $path;
            }
        }
        return false;
    }
}
