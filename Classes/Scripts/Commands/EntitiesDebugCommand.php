<?php
namespace InstituteWeb\Iwm\Scripts\Commands;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Iwm\Environments\DataProvider\EntityManager;
use InstituteWeb\Iwm\Environments\DataProvider\Models;
use InstituteWeb\Iwm\Environments\DataProvider\Modifiers\AbstractModifier;
use InstituteWeb\Iwm\Environments\Environment;
use InstituteWeb\Iwm\Scripts\Helper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Compares entities in current context with database and inserts/updates them
 *
 * @package Iwm\Iwm
 */
class EntitiesDebugCommand extends EntitiesShowCommand
{
    /**
     * Configure this command
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('entities:debug')
            ->setAliases(['debug'])
            ->setDescription('Debug entities.')
            ->addUsage('--repeat')

            ->addArgument(
                'path',
                InputArgument::OPTIONAL,
                'Path to check.',
                Helper\File::concatenatePathParts([getcwd(), 'typo3conf', 'Environments'])
            )

            ->addOption(
                'repeat',
                'r',
                \Symfony\Component\Console\Input\InputOption::VALUE_NONE,
                'Restart command after updating identifier.'
            )
        ;
    }

    /**
     * Executes this command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void|int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->initIO($input, $output);

        $this->getPhpConfigurationFiles($this->input->getArgument('path'));
        $rootNode = EntityManager::getInstance()->getAllAsNestedNodes();

        // Processing modifiers with high priority
        EntityManager::getInstance()->processModifiers(AbstractModifier::PRIORITY_HIGH);

        // Build and output table, based on nested items in rootNode
        $tableData = [];
        $this->buildTableDataWithNodes($rootNode, $tableData, true);

        $this->output->outputLine('Found entities');
        $this->output->outputLine('in <comment>' . realpath($this->input->getArgument('path')) . '</comment>');
        $this->output->outputTable(
            $tableData,
            [
                'Tree (Record Type)',
                'Label',
                'Identifier',
                'Mapped uid'
            ],
            true
        );
        $this->output->outputLine('Select "0" to exit.');

        $validateItemSelection = function ($value) use ($tableData) {
            if ($value < 0 || $value > count($tableData)) {
                throw new \UnexpectedValueException('No valid id entered!');
            }
            return $value;
        };
        $id = (int) $this->output->askAndValidate('Which item (id) would you like to debug? ', $validateItemSelection);
        if ($id === 0) {
            $this->output->outputLine('Bye.');
            return;
        }

        $item = array_values($tableData)[$id - 1];
        $identifier = $item[2];


        // Get entity and debug field array
        $entity = EntityManager::getInstance()->get($identifier);
        $fieldArray = $entity->getFieldArray();

        $data = [];
        $fullDetailValues = [];
        foreach ($fieldArray as $key => $value) {
            if (strpos($value, PHP_EOL) !== false || strlen($value) > 55) {
                $fullDetailValues[$key] = $value;
                continue;
            }
            $data[] = [$key, $value];
        }

        $this->output->outputLine();
        $this->output->outputLine('Identifier: ' . $identifier);
        $this->output->outputTable($data, ['Attribute', 'Value']);

        foreach ($fullDetailValues as $key => $value) {
            $this->output->outputLine('--- Attribute "' . $key . '" -------');
            $this->output->outputLine(trim($value) . PHP_EOL);
        }

        if ($input->getOption('repeat')) {
            $command = $this->getApplication()->find($this->getName());
            $command->run($input, $output);
        }
    }
}
