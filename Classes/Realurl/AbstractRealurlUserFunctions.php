<?php
namespace InstituteWeb\Iwm\Realurl;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

/**
 * Class with userfunction to extend behaviour of realurl
 *
 * @package InstituteWeb\Iwm
 */
abstract class AbstractRealurlUserFunctions
{
    const REPLACEMENT_KEYS = 'keys';
    const REPLACEMENT_VALUES = 'values';

    /**
     * Replacements performing when creating links
     * from => to
     * eg. 'news/detail/tx_news/' => 'news/show/'
     *
     * @var array
     */
    protected $replacementsInUrl = array(
    );

    /**
     * Like replacementsInUrl, but will be performed just in encode process
     * eg. '/en.html' => '/en'
     *
     * @var array
     */
    protected $replacementsInUrlEncodeOnly = array(
    );

    /**
     * Returns array_keys or array_values of given source
     *
     * @param string $get Allowed values are 'keys' or 'values', see class constants
     * @param array $source Defines the array to return keys or values of
     * @return array
     */
    protected function prepareReplacements($get, array $source)
    {
        if ($get === self::REPLACEMENT_KEYS) {
            return array_keys($source);
        }
        return array_values($source);
    }

    /**
     * Do URL encoding (when creating links, etc.)
     *
     * @param array &$params
     * @return void
     */
    public function encode(&$params)
    {
        $params['URL'] = str_replace(
            $this->prepareReplacements(self::REPLACEMENT_KEYS, $this->replacementsInUrl),
            $this->prepareReplacements(self::REPLACEMENT_VALUES, $this->replacementsInUrl),
            $params['URL']
        );
        $params['URL'] = str_replace(
            $this->prepareReplacements(self::REPLACEMENT_KEYS, $this->replacementsInUrlEncodeOnly),
            $this->prepareReplacements(self::REPLACEMENT_VALUES, $this->replacementsInUrlEncodeOnly),
            $params['URL']
        );
    }

    /**
     * Do URL decoding (when calling a page)
     *
     * @param array &$params
     * @return void
     */
    public function decode(&$params)
    {
        $params['URL'] = str_replace(
            $this->prepareReplacements(self::REPLACEMENT_VALUES, $this->replacementsInUrl),
            $this->prepareReplacements(self::REPLACEMENT_KEYS, $this->replacementsInUrl),
            $params['URL']
        );
    }
}
