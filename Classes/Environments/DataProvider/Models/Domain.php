<?php
namespace InstituteWeb\Iwm\Environments\DataProvider\Models;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

/**
 * Class Domain
 * for table sys_domains
 *
 * @package InstituteWeb\Iwm
 */
class Domain extends AbstractSystemEntryModel
{

    /**
     * @var array key is the expected field name, value the actual one
     */
    protected $_inconsistentFieldNames = [
        'domain_name' => 'domainName',
        'redirect_to' => 'redirectTo',
        'redirect_http_status_code' => 'redirectHttpStatusCode',
    ];

    /**
     * @var string
     */
    protected $_tablename = 'sys_domain';

    /**
     * @var string
     */
    protected $domainName;

    /**
     * @var string Representing bool
     */
    protected $forced = '0';

    /**
     * @var string
     */
    protected $redirectTo = '';

    /**
     * @var int
     */
    protected $redirectHttpStatusCode = 301;

    /**
     * @var string Representing bool
     */
    protected $prependParams = '0';


    /**
     * Domain constructor
     *
     * @param string $domainname Name of the domain e.g. "www.domain.com"
     * @param string $identifier Unique identifier of this entity. Mandantory.
     * @param bool $isPrimary When true this domain will be used for links
     * @param string $redirectTo
     * @param int $redirectHttpStatusCode
     * @param bool $prependParams
     * @param array $additionalAttributes
     */
    public function __construct(
        $domainname,
        $identifier,
        $isPrimary = false,
        $redirectTo = '',
        $redirectHttpStatusCode = 301,
        $prependParams = true,
        $sorting = null,
        $additionalAttributes = []
    ) {
        $this->setDomainName($domainname);
        $this->setIdentifier($identifier);

        $this->setRedirectTo($redirectTo);
        $this->setRedirectHttpStatusCode($redirectHttpStatusCode);
        $this->setPrependParams($prependParams);
        $this->setForced($isPrimary);
        $this->setAdditionalAttributes($additionalAttributes);
    }

    /**
     * Returns label of this record
     *
     * @return string
     */
    public function getLabel() {
        return $this->getDomainName();
    }

    /**
     * Get Domain
     *
     * @return string
     */
    public function getDomainName()
    {
        return $this->domainName;
    }

    /**
     * Set Domain
     *
     * @param string $domainName
     * @return void
     */
    public function setDomainName($domainName)
    {
        $this->domainName = $domainName;
    }

    /**
     * Set Forced
     *
     * @param boolean $forced
     * @return void
     */
    public function setForced($forced)
    {
        $this->forced = $forced ? '1' : '0';
    }

    /**
     * True when forced flag is set
     *
     * @return bool
     */
    public function isForced()
    {
        return $this->forced;
    }

    /**
     * Add this domain to given page
     *
     * @param Pages\Page $page
     * @return Template
     */
    public function addTo(\InstituteWeb\Iwm\Environments\DataProvider\Models\Pages\Page $page)
    {
        $page->addDomain($this);
        return $this;
    }

    /**
     * Set RedirectTo
     *
     * @param string $redirectTo
     * @return Domain
     */
    public function setRedirectTo($redirectTo)
    {
        $this->redirectTo = $redirectTo;
        return $this;
    }

    /**
     * Set RedirectCode
     *
     * @param int $redirectHttpStatusCode
     * @return Domain
     */
    public function setRedirectHttpStatusCode($redirectHttpStatusCode)
    {
        $this->redirectHttpStatusCode = $redirectHttpStatusCode;
        return $this;
    }

    /**
     * Set TransferParameters
     *
     * @param boolean $prependParams
     * @return Domain
     */
    public function setPrependParams($prependParams)
    {
        $this->prependParams = $prependParams ? '1' : '0';
        return $this;
    }
}
