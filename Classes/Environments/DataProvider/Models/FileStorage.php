<?php
namespace InstituteWeb\Iwm\Environments\DataProvider\Models;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use \InstituteWeb\Iwm\Environments\DataProvider\Traits;

/**
 * Class FileStorage
 * for table sys_file_storage
 *
 * @package InstituteWeb\Iwm
 */
class FileStorage extends AbstractSystemEntryModel
{
    use Traits\SettersAcceptStringInputForArrays;

    const PATH_TYPE_RELATIVE = 'relative';
    const PATH_TYPE_ABSOLUTE = 'absolute';

    /**
     * @var string
     */
    protected $_tablename = 'sys_file_storage';

    /**
     * @var string Representing bool
     */
    protected $deleted = '0';

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $driver = 'Local';

    /**
     * @var string
     */
    protected $configuration;

    /**
     * Helper for $configuration
     *
     * @var string
     */
    protected $_basePath = 'fileadmin/';

    /**
     * Helper for $configuration
     *
     * @var string
     */
    protected $_pathType = self::PATH_TYPE_RELATIVE;

    /**
     * Helper for $configuration
     *
     * @var bool
     */
    protected $_caseSensitive = true;

    /**
     * @var string Representing bool
     */
    protected $isDefault = '0';

    /**
     * @var string Representing bool
     */
    protected $autoExtractMetadata = '1';

    /**
     * @var string
     */
    protected $processingfolder;

    /**
     * @var string Representing bool
     */
    protected $isBrowsable = '1';

    /**
     * @var string Representing bool
     */
    protected $isPublic = '1';

    /**
     * @var string Representing bool
     */
    protected $isWritable = '1';

    /**
     * @var string Representing bool
     */
    protected $isOnline = '1';

    /**
     * File storage constructor
     *
     * @param string $name A name for the file storage
     * @param string $identifier Unique identifier of this entity. Mandantory.
     * @param bool $isDefault
     * @param string $basePath
     * @param null|int $sorting
     * @param array $additionalAttributes
     */
    public function __construct($name, $identifier, $isDefault = false, $basePath = 'fileadmin/', $sorting = null, $additionalAttributes = [])
    {
        $this->setName($name);
        $this->setIdentifier($identifier);
        
        $this->setIsDefault($isDefault);
        $this->setBasePath($basePath);
        $this->setIsOnline(true);
        $this->setAdditionalAttributes($additionalAttributes);
    }

    /**
     * Get Label
     *
     * @return mixed
     */
    public function getLabel()
    {
        return $this->getName();
    }

    /**
     * Get Name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set Name
     *
     * @param string $name
     * @return FileStorage
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Set Driver
     *
     * @param string $driver
     * @return FileStorage
     */
    public function setDriver($driver)
    {
        $this->driver = $driver;
        return $this;
    }

    /**
     * Set BasePath (in flexform of $configuration)
     *
     * @param string $basePath
     * @return FileStorage
     */
    public function setBasePath($basePath)
    {
        $this->_basePath = $basePath;
        $this->configuration = $this->buildConfigurationTemplate($basePath, null, null);
        return $this;
    }

    /**
     * Set PathType (in flexform of $configuration)
     *
     * @param string $pathType
     * @return FileStorage
     */
    public function setPathType($pathType)
    {
        $this->_pathType = $pathType;
        $this->configuration = $this->buildConfigurationTemplate(null, $pathType, null);
        return $this;
    }

    /**
     * Set CaseSensitive (in flexform of $configuration)
     *
     * @param boolean $caseSensitive
     * @return FileStorage
     */
    public function setCaseSensitive($caseSensitive)
    {
        $this->_caseSensitive = $caseSensitive;
        $this->configuration = $this->buildConfigurationTemplate(null, null, $caseSensitive);
        return $this;
    }

    /**
     * @param string $basePath
     * @param string $pathType
     * @param bool $caseSensitive
     * @return string
     */
    protected function buildConfigurationTemplate($basePath = null, $pathType = null, $caseSensitive = null)
    {
        if (is_null($basePath)) {
            $basePath = $this->_basePath;
        }
        if (is_null($pathType)) {
            $pathType = $this->_pathType;
        }
        if (is_null($caseSensitive)) {
            $caseSensitive = $this->_caseSensitive ? '1' : '0';
        }

        return <<<XML
<?xml version="1.0" encoding="utf-8" standalone="yes" ?>
<T3FlexForms>
    <data>
        <sheet index="sDEF">
            <language index="lDEF">
                <field index="basePath">
                    <value index="vDEF">$basePath</value>
                </field>
                <field index="pathType">
                    <value index="vDEF">$pathType</value>
                </field>
                <field index="caseSensitive">
                    <value index="vDEF">$caseSensitive</value>
                </field>
            </language>
        </sheet>
    </data>
</T3FlexForms>
XML;
    }

    /**
     * Set IsDefault
     *
     * @param boolean $isDefault
     * @return FileStorage
     */
    public function setIsDefault($isDefault)
    {
        $this->isDefault = $isDefault ? '1' : '0';
        return $this;
    }

    /**
     * Set AutoExtractMetadata
     *
     * @param boolean $autoExtractMetadata
     * @return FileStorage
     */
    public function setAutoExtractMetadata($autoExtractMetadata)
    {
        $this->autoExtractMetadata = $autoExtractMetadata ? '1' : '0';
        return $this;
    }

    /**
     * Set Processingfolder
     *
     * @param string $processingfolder
     * @return FileStorage
     */
    public function setProcessingfolder($processingfolder)
    {
        $this->processingfolder = $processingfolder;
        return $this;
    }

    /**
     * Set IsBrowsable
     *
     * @param boolean $isBrowsable
     * @return FileStorage
     */
    public function setIsBrowsable($isBrowsable)
    {
        $this->isBrowsable = $isBrowsable ? '1' : '0';
        return $this;
    }

    /**
     * Set IsPublic
     *
     * @param boolean $isPublic
     * @return FileStorage
     */
    public function setIsPublic($isPublic)
    {
        $this->isPublic = $isPublic ? '1' : '0';
        return $this;
    }

    /**
     * Set IsWritable
     *
     * @param boolean $isWritable
     * @return FileStorage
     */
    public function setIsWritable($isWritable)
    {
        $this->isWritable = $isWritable ? '1' : '0';
        return $this;
    }

    /**
     * Set IsOnline
     *
     * @param boolean $isOnline
     * @return FileStorage
     */
    public function setIsOnline($isOnline)
    {
        $this->isOnline = $isOnline ? '1' : '0';
        return $this;
    }

    /**
     * Disable setter for sorting, cause sys_language has no sorting attribute
     *
     * @param int|null $sorting
     * @return void
     * @throws \InvalidArgumentException
     */
    public function setSorting($sorting = null)
    {
        throw new \InvalidArgumentException('sys_language has no sorting attribute which could be set.');
    }
}
