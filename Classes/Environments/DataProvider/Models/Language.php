<?php
namespace InstituteWeb\Iwm\Environments\DataProvider\Models;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

/**
 * Class Language
 * for table sys_langauge
 *
 * @package InstituteWeb\Iwm
 */
class Language extends AbstractSystemEntryModel
{
    /** Language ISO code constants used by sys_language */
    const LANG_ABKHAZ = 'ab';
    const LANG_AFAR = 'aa';
    const LANG_AFRIKAANS = 'af';
    const LANG_AKAN = 'ak';
    const LANG_ALBANIAN = 'sq';
    const LANG_AMHARIC = 'am';
    const LANG_ARABIC = 'ar';
    const LANG_ARAGONESE = 'an';
    const LANG_ARMENIAN = 'hy';
    const LANG_ASSAMESE = 'as';
    const LANG_AVARIC = 'av';
    const LANG_AVESTAN = 'ae';
    const LANG_AYMARA = 'ay';
    const LANG_AZERBAIJANI = 'az';
    const LANG_BAMBARA = 'bm';
    const LANG_BASHKIR = 'ba';
    const LANG_BASQUE = 'eu';
    const LANG_BELARUSIAN = 'be';
    const LANG_BENGALI_BANGLA = 'bn';
    const LANG_BIHARI = 'bh';
    const LANG_BISLAMA = 'bi';
    const LANG_BOSNIAN = 'bs';
    const LANG_BRETON = 'br';
    const LANG_BULGARIAN = 'bg';
    const LANG_BURMESE = 'my';
    const LANG_CATALAN_VALENCIAN = 'ca';
    const LANG_CHAMORRO = 'ch';
    const LANG_CHECHEN = 'ce';
    const LANG_CHICHEWA_CHEWA_NYANJA = 'ny';
    const LANG_CHINESE = 'zh';
    const LANG_CHUVASH = 'cv';
    const LANG_CORNISH = 'kw';
    const LANG_CORSICAN = 'co';
    const LANG_CREE = 'cr';
    const LANG_CROATIAN = 'hr';
    const LANG_CZECH = 'cs';
    const LANG_DANISH = 'da';
    const LANG_DIVEHI_DHIVEHI_MALDIVIAN = 'dv';
    const LANG_DUTCH = 'nl';
    const LANG_DZONGKHA = 'dz';
    const LANG_ENGLISH = 'en';
    const LANG_ESPERANTO = 'eo';
    const LANG_ESTONIAN = 'et';
    const LANG_EWE = 'ee';
    const LANG_FAROESE = 'fo';
    const LANG_FIJIAN = 'fj';
    const LANG_FINNISH = 'fi';
    const LANG_FRENCH = 'fr';
    const LANG_FULA_FULAH_PULAAR_PULAR = 'ff';
    const LANG_GALICIAN = 'gl';
    const LANG_GANDA = 'lg';
    const LANG_GEORGIAN = 'ka';
    const LANG_GERMAN = 'de';
    const LANG_GREEK = 'el';
    const LANG_GUARANI = 'gn';
    const LANG_GUJARATI = 'gu';
    const LANG_HAITIAN_HAITIAN_CREOLE = 'ht';
    const LANG_HAUSA = 'ha';
    const LANG_HEBREW = 'he';
    const LANG_HERERO = 'hz';
    const LANG_HINDI = 'hi';
    const LANG_HIRI_MOTU = 'ho';
    const LANG_HUNGARIAN = 'hu';
    const LANG_ICELANDIC = 'is';
    const LANG_IDO = 'io';
    const LANG_IGBO = 'ig';
    const LANG_INDONESIAN = 'id';
    const LANG_INTERLINGUA = 'ia';
    const LANG_INTERLINGUE = 'ie';
    const LANG_INUKTITUT = 'iu';
    const LANG_INUPIAQ = 'ik';
    const LANG_IRISH = 'ga';
    const LANG_ITALIAN = 'it';
    const LANG_JAPANESE = 'ja';
    const LANG_JAVANESE = 'jv';
    const LANG_KALAALLISUT_GREENLANDIC = 'kl';
    const LANG_KANNADA = 'kn';
    const LANG_KANURI = 'kr';
    const LANG_KASHMIRI = 'ks';
    const LANG_KAZAKH = 'kk';
    const LANG_KHMER = 'km';
    const LANG_KIKUYU_GIKUYU = 'ki';
    const LANG_KINYARWANDA = 'rw';
    const LANG_KIRUNDI = 'rn';
    const LANG_KOMI = 'kv';
    const LANG_KONGO = 'kg';
    const LANG_KOREAN = 'ko';
    const LANG_KURDISH = 'ku';
    const LANG_KWANYAMA_KUANYAMA = 'kj';
    const LANG_KYRGYZ = 'ky';
    const LANG_LAO = 'lo';
    const LANG_LATIN = 'la';
    const LANG_LATVIAN = 'lv';
    const LANG_LIMBURGISH_LIMBURGAN_LIMBURGER = 'li';
    const LANG_LINGALA = 'ln';
    const LANG_LITHUANIAN = 'lt';
    const LANG_LUBA_KATANGA = 'lu';
    const LANG_LUXEMBOURGISH_LETZEBURGESCH = 'lb';
    const LANG_MACEDONIAN = 'mk';
    const LANG_MALAGASY = 'mg';
    const LANG_MALAY = 'ms';
    const LANG_MALAYALAM = 'ml';
    const LANG_MALTESE = 'mt';
    const LANG_MANX = 'gv';
    const LANG_MARATHI = 'mr';
    const LANG_MARSHALLESE = 'mh';
    const LANG_MONGOLIAN = 'mn';
    const LANG_MĀORI = 'mi';
    const LANG_NAURU = 'na';
    const LANG_NAVAJO_NAVAHO = 'nv';
    const LANG_NDONGA = 'ng';
    const LANG_NEPALI = 'ne';
    const LANG_NORTHERN_NDEBELE = 'nd';
    const LANG_NORTHERN_SAMI = 'se';
    const LANG_NORWEGIAN = 'no';
    const LANG_NORWEGIAN_BOKMÅL = 'nb';
    const LANG_NORWEGIAN_NYNORSK = 'nn';
    const LANG_NUOSU = 'ii';
    const LANG_OCCITAN = 'oc';
    const LANG_OJIBWE_OJIBWA = 'oj';
    const LANG_OLD_CHURCH_SLAVONIC_OLD_BULGARIAN = 'cu';
    const LANG_ORIYA = 'or';
    const LANG_OROMO = 'om';
    const LANG_OSSETIAN_OSSETIC = 'os';
    const LANG_PANJABI_PUNJABI = 'pa';
    const LANG_PASHTO_PUSHTO = 'ps';
    const LANG_PERSIAN_FARSI = 'fa';
    const LANG_POLISH = 'pl';
    const LANG_PORTUGUESE = 'pt';
    const LANG_PĀLI = 'pi';
    const LANG_QUECHUA = 'qu';
    const LANG_ROMANIAN = 'ro';
    const LANG_ROMANSH = 'rm';
    const LANG_RUSSIAN = 'ru';
    const LANG_SAMOAN = 'sm';
    const LANG_SANGO = 'sg';
    const LANG_SANSKRIT = 'sa';
    const LANG_SARDINIAN = 'sc';
    const LANG_SCOTTISH_GAELIC_GAELIC = 'gd';
    const LANG_SERBIAN = 'sr';
    const LANG_SHONA = 'sn';
    const LANG_SINDHI = 'sd';
    const LANG_SINHALA_SINHALESE = 'si';
    const LANG_SLOVAK = 'sk';
    const LANG_SLOVENE = 'sl';
    const LANG_SOMALI = 'so';
    const LANG_SOUTHERN_NDEBELE = 'nr';
    const LANG_SOUTHERN_SOTHO = 'st';
    const LANG_SPANISH_CASTILIAN = 'es';
    const LANG_SUNDANESE = 'su';
    const LANG_SWAHILI = 'sw';
    const LANG_SWATI = 'ss';
    const LANG_SWEDISH = 'sv';
    const LANG_TAGALOG = 'tl';
    const LANG_TAHITIAN = 'ty';
    const LANG_TAJIK = 'tg';
    const LANG_TAMIL = 'ta';
    const LANG_TATAR = 'tt';
    const LANG_TELUGU = 'te';
    const LANG_THAI = 'th';
    const LANG_TIBETAN_STANDARD_TIBETAN_CENTRAL = 'bo';
    const LANG_TIGRINYA = 'ti';
    const LANG_TONGA = 'to';
    const LANG_TSONGA = 'ts';
    const LANG_TSWANA = 'tn';
    const LANG_TURKISH = 'tr';
    const LANG_TURKMEN = 'tk';
    const LANG_TWI = 'tw';
    const LANG_UKRAINIAN = 'uk';
    const LANG_URDU = 'ur';
    const LANG_UYGHUR_UIGHUR = 'ug';
    const LANG_UZBEK = 'uz';
    const LANG_VENDA = 've';
    const LANG_VIETNAMESE = 'vi';
    const LANG_VOLAPUK = 'vo';
    const LANG_WALLOON = 'wa';
    const LANG_WELSH = 'cy';
    const LANG_WESTERN_FRISIAN = 'fy';
    const LANG_WOLOF = 'wo';
    const LANG_XHOSA = 'xh';
    const LANG_YIDDISH = 'yi';
    const LANG_YORUBA = 'yo';
    const LANG_ZHUANG_CHUANG = 'za';
    const LANG_ZULU = 'zu';

    /** Codes for flags */
    const FLAG_NO_FLAG = '0';
    const FLAG_MULTIPLE = 'multiple';
    const FLAG_AD = 'ad';
    const FLAG_AE = 'ae';
    const FLAG_AF = 'af';
    const FLAG_AG = 'ag';
    const FLAG_AI = 'ai';
    const FLAG_AL = 'al';
    const FLAG_AM = 'am';
    const FLAG_AN = 'an';
    const FLAG_AO = 'ao';
    const FLAG_AR = 'ar';
    const FLAG_AS = 'as';
    const FLAG_AT = 'at';
    const FLAG_AU = 'au';
    const FLAG_AW = 'aw';
    const FLAG_AX = 'ax';
    const FLAG_AZ = 'az';
    const FLAG_BA = 'ba';
    const FLAG_BB = 'bb';
    const FLAG_BD = 'bd';
    const FLAG_BE = 'be';
    const FLAG_BF = 'bf';
    const FLAG_BG = 'bg';
    const FLAG_BH = 'bh';
    const FLAG_BI = 'bi';
    const FLAG_BJ = 'bj';
    const FLAG_BM = 'bm';
    const FLAG_BN = 'bn';
    const FLAG_BO = 'bo';
    const FLAG_BR = 'br';
    const FLAG_BS = 'bs';
    const FLAG_BT = 'bt';
    const FLAG_BV = 'bv';
    const FLAG_BW = 'bw';
    const FLAG_BY = 'by';
    const FLAG_BZ = 'bz';
    const FLAG_CA = 'ca';
    const FLAG_CATALONIA = 'catalonia';
    const FLAG_CC = 'cc';
    const FLAG_CD = 'cd';
    const FLAG_CF = 'cf';
    const FLAG_CG = 'cg';
    const FLAG_CH = 'ch';
    const FLAG_CI = 'ci';
    const FLAG_CK = 'ck';
    const FLAG_CL = 'cl';
    const FLAG_CM = 'cm';
    const FLAG_CN = 'cn';
    const FLAG_CO = 'co';
    const FLAG_CR = 'cr';
    const FLAG_CS = 'cs';
    const FLAG_CU = 'cu';
    const FLAG_CV = 'cv';
    const FLAG_CX = 'cx';
    const FLAG_CY = 'cy';
    const FLAG_CZ = 'cz';
    const FLAG_DE = 'de';
    const FLAG_DJ = 'dj';
    const FLAG_DK = 'dk';
    const FLAG_DM = 'dm';
    const FLAG_DO = 'do';
    const FLAG_DZ = 'dz';
    const FLAG_EC = 'ec';
    const FLAG_EE = 'ee';
    const FLAG_EG = 'eg';
    const FLAG_EH = 'eh';
    const FLAG_EN_US_GB = 'en-us-gb';
    const FLAG_ENGLAND = 'england';
    const FLAG_ER = 'er';
    const FLAG_ES = 'es';
    const FLAG_ET = 'et';
    const FLAG_EUROPEANUNION = 'europeanunion';
    const FLAG_FAM = 'fam';
    const FLAG_FI = 'fi';
    const FLAG_FJ = 'fj';
    const FLAG_FK = 'fk';
    const FLAG_FM = 'fm';
    const FLAG_FO = 'fo';
    const FLAG_FR = 'fr';
    const FLAG_GA = 'ga';
    const FLAG_GB = 'gb';
    const FLAG_GD = 'gd';
    const FLAG_GE = 'ge';
    const FLAG_GF = 'gf';
    const FLAG_GH = 'gh';
    const FLAG_GI = 'gi';
    const FLAG_GL = 'gl';
    const FLAG_GM = 'gm';
    const FLAG_GN = 'gn';
    const FLAG_GP = 'gp';
    const FLAG_GQ = 'gq';
    const FLAG_GR = 'gr';
    const FLAG_GS = 'gs';
    const FLAG_GT = 'gt';
    const FLAG_GU = 'gu';
    const FLAG_GW = 'gw';
    const FLAG_GY = 'gy';
    const FLAG_HK = 'hk';
    const FLAG_HM = 'hm';
    const FLAG_HN = 'hn';
    const FLAG_HR = 'hr';
    const FLAG_HT = 'ht';
    const FLAG_HU = 'hu';
    const FLAG_ID = 'id';
    const FLAG_IE = 'ie';
    const FLAG_IL = 'il';
    const FLAG_IN = 'in';
    const FLAG_IO = 'io';
    const FLAG_IQ = 'iq';
    const FLAG_IR = 'ir';
    const FLAG_IS = 'is';
    const FLAG_IT = 'it';
    const FLAG_JM = 'jm';
    const FLAG_JO = 'jo';
    const FLAG_JP = 'jp';
    const FLAG_KE = 'ke';
    const FLAG_KG = 'kg';
    const FLAG_KH = 'kh';
    const FLAG_KI = 'ki';
    const FLAG_KM = 'km';
    const FLAG_KN = 'kn';
    const FLAG_KP = 'kp';
    const FLAG_KR = 'kr';
    const FLAG_KW = 'kw';
    const FLAG_KY = 'ky';
    const FLAG_KZ = 'kz';
    const FLAG_LA = 'la';
    const FLAG_LB = 'lb';
    const FLAG_LC = 'lc';
    const FLAG_LI = 'li';
    const FLAG_LK = 'lk';
    const FLAG_LR = 'lr';
    const FLAG_LS = 'ls';
    const FLAG_LT = 'lt';
    const FLAG_LU = 'lu';
    const FLAG_LV = 'lv';
    const FLAG_LY = 'ly';
    const FLAG_MA = 'ma';
    const FLAG_MC = 'mc';
    const FLAG_MD = 'md';
    const FLAG_ME = 'me';
    const FLAG_MG = 'mg';
    const FLAG_MH = 'mh';
    const FLAG_MK = 'mk';
    const FLAG_ML = 'ml';
    const FLAG_MM = 'mm';
    const FLAG_MN = 'mn';
    const FLAG_MO = 'mo';
    const FLAG_MP = 'mp';
    const FLAG_MQ = 'mq';
    const FLAG_MR = 'mr';
    const FLAG_MS = 'ms';
    const FLAG_MT = 'mt';
    const FLAG_MU = 'mu';
    const FLAG_MV = 'mv';
    const FLAG_MW = 'mw';
    const FLAG_MX = 'mx';
    const FLAG_MY = 'my';
    const FLAG_MZ = 'mz';
    const FLAG_NA = 'na';
    const FLAG_NC = 'nc';
    const FLAG_NE = 'ne';
    const FLAG_NF = 'nf';
    const FLAG_NG = 'ng';
    const FLAG_NI = 'ni';
    const FLAG_NL = 'nl';
    const FLAG_NO = 'no';
    const FLAG_NP = 'np';
    const FLAG_NR = 'nr';
    const FLAG_NU = 'nu';
    const FLAG_NZ = 'nz';
    const FLAG_OM = 'om';
    const FLAG_PA = 'pa';
    const FLAG_PE = 'pe';
    const FLAG_PF = 'pf';
    const FLAG_PG = 'pg';
    const FLAG_PH = 'ph';
    const FLAG_PK = 'pk';
    const FLAG_PL = 'pl';
    const FLAG_PM = 'pm';
    const FLAG_PN = 'pn';
    const FLAG_PR = 'pr';
    const FLAG_PS = 'ps';
    const FLAG_PT = 'pt';
    const FLAG_PW = 'pw';
    const FLAG_PY = 'py';
    const FLAG_QA = 'qa';
    const FLAG_QC = 'qc';
    const FLAG_RE = 're';
    const FLAG_RO = 'ro';
    const FLAG_RS = 'rs';
    const FLAG_RU = 'ru';
    const FLAG_RW = 'rw';
    const FLAG_SA = 'sa';
    const FLAG_SB = 'sb';
    const FLAG_SC = 'sc';
    const FLAG_SCOTLAND = 'scotland';
    const FLAG_SD = 'sd';
    const FLAG_SE = 'se';
    const FLAG_SG = 'sg';
    const FLAG_SH = 'sh';
    const FLAG_SI = 'si';
    const FLAG_SJ = 'sj';
    const FLAG_SK = 'sk';
    const FLAG_SL = 'sl';
    const FLAG_SM = 'sm';
    const FLAG_SN = 'sn';
    const FLAG_SO = 'so';
    const FLAG_SR = 'sr';
    const FLAG_ST = 'st';
    const FLAG_SV = 'sv';
    const FLAG_SY = 'sy';
    const FLAG_SZ = 'sz';
    const FLAG_TC = 'tc';
    const FLAG_TD = 'td';
    const FLAG_TF = 'tf';
    const FLAG_TG = 'tg';
    const FLAG_TH = 'th';
    const FLAG_TJ = 'tj';
    const FLAG_TK = 'tk';
    const FLAG_TL = 'tl';
    const FLAG_TM = 'tm';
    const FLAG_TN = 'tn';
    const FLAG_TO = 'to';
    const FLAG_TR = 'tr';
    const FLAG_TT = 'tt';
    const FLAG_TV = 'tv';
    const FLAG_TW = 'tw';
    const FLAG_TZ = 'tz';
    const FLAG_UA = 'ua';
    const FLAG_UG = 'ug';
    const FLAG_UM = 'um';
    const FLAG_US = 'us';
    const FLAG_UY = 'uy';
    const FLAG_UZ = 'uz';
    const FLAG_VA = 'va';
    const FLAG_VC = 'vc';
    const FLAG_VE = 've';
    const FLAG_VG = 'vg';
    const FLAG_VI = 'vi';
    const FLAG_VN = 'vn';
    const FLAG_VU = 'vu';
    const FLAG_WALES = 'wales';
    const FLAG_WF = 'wf';
    const FLAG_WS = 'ws';
    const FLAG_YE = 'ye';
    const FLAG_YT = 'yt';
    const FLAG_ZA = 'za';
    const FLAG_ZMZW = 'zmzw';

    /**
     * @var string
     */
    protected $_tablename = 'sys_language';

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string Two digit iso code of language
     * @see Langauge constants in this class
     */
    protected $languageIsocode;

    /**
     * @var string Most important attribute here: the icon.
     *             You can try to use a shortcut from language iso codes, but it does no always match!
     */
    protected $flag;

    /**
     * SysLanguage constructor
     *
     * @param string $title The title of the language
     * @param string $identifier Unique identifier of this entity. Mandantory.
     * @param string $isocode Iso code. See class constants.
     * @param string $flag Flag code. See class constants.
     * @param int|null $sorting sys_language has no sorting
     * @param array $additionalAttributes
     */
    public function __construct($title, $identifier, $isocode, $flag, $sorting = null, $additionalAttributes = [])
    {
        $this->setTitle($title);
        $this->setIdentifier($identifier);

        $this->setLanguageIsocode($isocode);
        $this->setFlag($flag);
        $this->setAdditionalAttributes($additionalAttributes);
    }

    /**
     * Get Title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set Title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Set Language Isocode
     *
     * @param string $languageIsocode
     * @return void
     *
     * @see Langauge constants in this class
     */
    public function setLanguageIsocode($languageIsocode)
    {
        $this->languageIsocode = $languageIsocode;
    }

    /**
     * Set Flag
     *
     * @param string $flag
     * @return void
     *
     * @see Flag constants in this class
     */
    public function setFlag($flag)
    {
        $this->flag = $flag;
    }

    /**
     * Returns label of this record
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->getTitle();
    }

    /**
     * Disable setter for sorting, cause sys_language has no sorting attribute
     *
     * @param int|null $sorting
     * @return void
     * @throws \InvalidArgumentException
     */
    public function setSorting($sorting = null)
    {
        throw new \InvalidArgumentException('sys_language has no sorting attribute which could be set.');
    }

    /**
     * Get LanguageIsocode
     *
     * @return string
     */
    public function getLanguageIsocode()
    {
        return $this->languageIsocode;
    }

    /**
     * Get Flag
     *
     * @return string
     */
    public function getFlag()
    {
        return $this->flag;
    }
}
