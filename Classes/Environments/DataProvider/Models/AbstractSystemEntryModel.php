<?php
namespace InstituteWeb\Iwm\Environments\DataProvider\Models;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Iwm\Environments\DataProvider\EntityManager;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Abstract class AbstractSystemEntryModel
 *
 * @package InstituteWeb\Iwm
 */
abstract class AbstractSystemEntryModel
{
    /**
     * Required to get overwritten by extending objects
     *
     * @var string
     */
    protected $_tablename = null;

    /**
     * @var array
     */
    protected $_propertiesWithRelations = ['pid'];

    /**
     * Properties which just get touched in DB and sync if not set
     *
     * @var array
     */
    protected $_secureProperties = [];

    /**
     * Ignores this property completely
     *
     * @var array
     */
    protected $_ignoreProperties = [];

    /**
     * @var array
     */
    protected $_inconsistentFieldNames = [];

    /**
     * @var string
     */
    protected $identifier;

    /**
     * @var AbstractSystemEntryModel
     */
    protected $pid;

    /**
     * @var array
     */
    protected $additionalAttributes = [];



    /**
     * @return string
     */
    public function getLabel()
    {
        return '';
    }

    /**
     * Get uid of model
     *
     * @return int|null
     * @internal
     */
    public function _getUid()
    {
        return \InstituteWeb\Iwm\Environments\Environment::getInstance()->getEntityMapping($this->getIdentifier());
    }

    /**
     * Get Identifier
     *
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Sets the uid of this model or registers the model in entity manager
     *
     * @param int|string $identifier
     * @return void
     */
    protected function setIdentifier($identifier)
    {
        EntityManager::getInstance()->add($identifier, $this);
        $this->identifier = $identifier;
    }

    /**
     * Get pid (which returns the identifier)
     *
     * @return AbstractSystemEntryModel
     */
    public function getPid()
    {
        return $this->pid;
    }

    /**
     * Set the pid of this entity.
     * This method expects an AbstractSystemEntryModel or a string.
     *
     * @param AbstractSystemEntryModel $pid
     * @return AbstractSystemEntryModel
     */
    public function setPid(AbstractSystemEntryModel $pid)
    {
        $this->pid = $pid;
        return $this;
    }
    
    /**
     * Set pid of given entries to given parent object.
     *
     * @param AbstractSystemEntryModel[] $entries
     * @param AbstractSystemEntryModel $parentObject
     * @return void
     */
    protected function setPidInGivenEntries(array $entries, AbstractSystemEntryModel $parentObject)
    {
        foreach ($entries as $entry) {
            if (!is_null($entry->getPid())) {
                throw new \RuntimeException(
                    'You have assigned the same entry to two different parents! (' . (string) $entry . ')'
                );
            }
            $entry->setPid($parentObject);
        }
    }

     /**
     * Get all additional attributes
     *
     * @return array
     */
    public function getAdditionalAttributes()
    {
        return $this->additionalAttributes;
    }

    /**
     * Set all additional attributes
     *
     * @param array $additionalAttributes
     * @return AbstractSystemEntryModel
     */
    public function setAdditionalAttributes(array $additionalAttributes)
    {
        $this->additionalAttributes = $additionalAttributes;
        return $this;
    }

    /**
     * Add a single additional attribute
     *
     * @param string $key
     * @param mixed $value
     * @return AbstractSystemEntryModel
     */
    public function addAdditionalAttribute($key, $value)
    {
        $this->additionalAttributes[$key] = $value;
        return $this;
    }

    /**
     * Returns the table name in database of this entity
     *
     * @return string
     * @internal
     */
    public function getTablename()
    {
        return $this->_tablename;
    }

    /**
     * Returns the properties of this object as array. Properties with starting underscore (_) are ignored,
     * so as properties which contain arrays and the identifier value.
     *
     * @return array
     * @internal
     */
    public function getFieldArray($mappedItems = [])
    {
        $array = [];
        foreach (get_object_vars($this) as $key => $value) {
            if (!is_array($value) && substr($key, 0, 1) !== '_' && $key !== 'identifier') {
                $processedKey = GeneralUtility::camelCaseToLowerCaseUnderscored($key);
                if (array_key_exists($processedKey, $this->_inconsistentFieldNames)) {
                    $processedKey = $this->_inconsistentFieldNames[$processedKey];
                }

                if (is_null($value)) {
                    // TODO: Identify DB default value and return this instead
                    $array[$processedKey] = in_array($key, ['pid', 'sorting']) ? 0 : null;
                } else {
                    $getter = 'get' . ucfirst($key);
                    if ($value instanceof AbstractSystemEntryModel && method_exists($this, $getter)) {
                        $value = $this->$getter();
                        if ($value instanceof AbstractSystemEntryModel) {
                            $value = $value->getIdentifier();
                        }
                    }

                    if (in_array($key, $this->getPropertiesWithRelations()) && isset($mappedItems[$value])) {
                        $value = $mappedItems[$value];
                    }
                    $array[$processedKey] = $value;

                    if (isset($mappedItems[$this->getIdentifier()])) {
                        $array['uid'] = $mappedItems[$this->getIdentifier()];
                    }
                }
            }
        }
        return array_merge($array, $this->getAdditionalAttributes());
    }

    /**
     * Get diff of this entity with given database row
     *
     * @param array $row
     * @param array $mappedItems
     * @return array Differences
     */
    public function getDiff(array $row, array $mappedItems = [])
    {
        $differences = [];
        foreach ($this->getFieldArray($mappedItems) as $key => $value) {
            if ($row[$key] != $value) {
                if (in_array($key, $this->_secureProperties)) {
                    continue;
                }
                $differences[$key] = $value;
            }
        }
        return $differences;
    }

    /**
     * @return array
     * @internal
     */
    public function getPropertiesWithRelations()
    {
        return $this->_propertiesWithRelations;
    }

    /**
     * Converts this object to string and returns its identifier
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getIdentifier();
    }
}
