<?php
namespace InstituteWeb\Iwm\Environments\DataProvider\Models;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use \InstituteWeb\Iwm\Environments\DataProvider\Traits;

/**
 * Class FileMount
 * for table sys_file_mounts
 *
 * @package InstituteWeb\Iwm
 */
class FileMount extends AbstractSystemEntryModel
{
    use Traits\SettersAcceptStringInputForArrays;

    /**
     * @var int|null;
     */
    protected $sorting;

    /**
     * Get Sorting
     *
     * @return int|null
     */
    public function getSorting()
    {
        if (is_null($this->sorting)) {
            return INF;
        }
        return $this->sorting;
    }

    /**
     * Set Sorting
     *
     * @param int|null $sorting
     * @return AbstractSystemEntryModel
     */
    public function setSorting($sorting)
    {
        $this->sorting = $sorting;
        return $this;
    }
}
