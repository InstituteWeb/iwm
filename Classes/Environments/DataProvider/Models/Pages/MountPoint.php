<?php
namespace InstituteWeb\Iwm\Environments\DataProvider\Models\Pages;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Iwm\Environments\DataProvider\Traits;

/**
 * Class MountPoint
 *
 * @package InstituteWeb\Iwm
 */
class MountPoint extends Page
{
    /**
     * @var int
     */
    protected $doktype = self::DOKTYPE_MOUNT_POINT;

    /**
     * @var Page|int
     */
    protected $mountedPage;

    /**
     * Shortcut constructor
     *
     * @param string $title
     * @param string $identifier Unique identifier of this entity. Mandantory.
     * @param null|int $sorting
     * @param Page[] $childPages
     * @param array $additionalAttributes
     * @param Page|int $mountedPage
     */
    public function __construct($title, $identifier, $sorting = null, array $childPages = [], $additionalAttributes = [], $mountedPage = null)
    {
        parent::__construct($title, $identifier, $sorting, $childPages, $additionalAttributes);
        $this->setMountedPage($mountedPage);
    }

    /**
     * Get MountedPage
     *
     * @return Page|int
     */
    public function getMountedPage()
    {
        return $this->mountedPage;
    }

    /**
     * Set MountedPage
     *
     * @param Page|int $mountedPage
     * @return void
     */
    public function setMountedPage($mountedPage)
    {
        $this->mountedPage = $mountedPage;
    }
}
