<?php
namespace InstituteWeb\Iwm\Environments\DataProvider\Models\Pages;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Iwm\Environments\DataProvider\Traits;

/**
 * Class ExternalLink
 *
 * @package InstituteWeb\Iwm
 */
class ExternalLink extends Page
{
    protected $doktype = self::DOKTYPE_EXTERNAL_LINK;

    /**
     * @var string
     */
    protected $protocol = 'http';

    /**
     * @var string
     */
    protected $url;

    /**
     * Set Protocol
     *
     * @param string $protocol
     * @return void
     */
    public function setProtocol($protocol)
    {
        $this->protocol = $protocol;
    }

    /**
     * Set Url
     *
     * @param string $url
     * @return void
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }
}
