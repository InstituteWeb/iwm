<?php
namespace InstituteWeb\Iwm\Environments\DataProvider\Models\Pages;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Iwm\Environments\DataProvider\Traits;

/**
 * Class SysFolder
 *
 * @package InstituteWeb\Iwm
 */
class SysFolder extends Page
{
    protected $doktype = self::DOKTYPE_SYS_FOLDER;
}
