<?php
namespace InstituteWeb\Iwm\Environments\DataProvider\Models\Pages;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Iwm\Environments\DataProvider\Models\AbstractSystemEntryModel;
use InstituteWeb\Iwm\Environments\DataProvider\Traits;

/**
 * Class Shortcut
 *
 * @package InstituteWeb\Iwm
 */
class Shortcut extends Page
{
    /** Shortcut modes: for selected or current page (shortcut) */
    const SHORTCUT_MODE_SELECTED_PAGE = 0;
    const SHORTCUT_MODE_FIRST_SUBPAGE = 1;
    const SHORTCUT_MODE_RANDOM_SUBPAGE = 2;
    const SHORTCUT_MODE_PARENT_PAGE = 3;

    /**
     * @var array
     */
    protected $_propertiesWithRelations = ['pid', 'shortcut'];

    /**
     * @var int
     */
    protected $doktype = self::DOKTYPE_SHORTCUT;

    /**
     * @var Page|int
     */
    protected $shortcut;

    /**
     * @var string
     */
    protected $shortcutMode;

    /**
     * Shortcut constructor
     *
     * @param string $title
     * @param string $identifier Unique identifier of this entity. Mandantory.
     * @param null|int $sorting
     * @param Page[] $childPages
     * @param array $additionalAttributes
     * @param Page|int $shortcutTarget
     * @param null|string $shortcutMode See class constants
     */
    public function __construct($title, $identifier, $sorting = null, array $childPages = [], $additionalAttributes = [], $shortcutTarget = null, $shortcutMode = null)
    {
        parent::__construct($title, $identifier, $sorting, $childPages, $additionalAttributes);
        $this->setShortcut($shortcutTarget);
        $this->setShortcutMode($shortcutMode);
    }

    /**
     * Get ShortcutTarget
     *
     * @return Page|int
     */
    public function getShortcut()
    {
        if ($this->shortcut instanceof AbstractSystemEntryModel) {
            return $this->shortcut->getIdentifier();
        }
        return $this->shortcut;
    }

    /**
     * Set ShortcutTarget
     *
     * @param Page|int $shortcut
     * @return void
     */
    public function setShortcut($shortcut)
    {
        $this->shortcut = $shortcut;
    }

    /**
     * Get ShortcutMode
     *
     * @return string
     */
    public function getShortcutMode()
    {
        return $this->shortcutMode;
    }

    /**
     * Set ShortcutMode
     *
     * @param string $shortcutMode
     * @return void
     */
    public function setShortcutMode($shortcutMode)
    {
        $this->shortcutMode = $shortcutMode;
    }
}
