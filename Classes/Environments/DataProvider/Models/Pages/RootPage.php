<?php
namespace InstituteWeb\Iwm\Environments\DataProvider\Models\Pages;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Iwm\Environments\DataProvider\Traits;
use InstituteWeb\Iwm\Environments\DataProvider\Models;
use InstituteWeb\Iwm\Environments\DataProvider\Modifiers;

/**
 * Class RootPage
 *
 * @package InstituteWeb\Iwm
 */
class RootPage extends Page
{
    use Traits\SettersAcceptStringInputForArrays;

    /**
     * Flags this page as root
     *
     * @var string Representing bool
     */
    protected $isSiteroot = '1';

    /**
     * RootPage constructor.
     *
     * @param string $title
     * @param string $identifier Unique identifier of this entity. Mandantory.
     * @param int|null $sorting
     * @param array $childPages
     * @param array $additionalAttributes
     * @return RootPage
     */
    public function __construct($title, $identifier, $sorting = null, array $childPages = [], $additionalAttributes = [])
    {
        parent::__construct($title, $identifier, $sorting, $childPages, $additionalAttributes);
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return RootPage
     */
    public function setTitle($title)
    {
        parent::setTitle($title);
        return $this;
    }

    /**
     * Set ChildPages
     *
     * @param Page|array $childPages
     * @return RootPage
     */
    public function setChildPages($childPages)
    {
        $this->setPidInGivenEntries($this->convertToArray($childPages), $this);
        $this->childPages = $this->convertToArray($childPages);
        return $this;
    }

    /**
     * Set Templates
     *
     * @param Models\Template|array $templates
     * @return RootPage
     */
    public function setTemplates($templates)
    {
        $this->setPidInGivenEntries($this->convertToArray($templates), $this);
        $this->templates = $this->convertToArray($templates);
        return $this;
    }

    /**
     * Add Template
     *
     * @param Models\Template $template
     * @return RootPage
     */
    public function addTemplate(Models\Template $template)
    {
        $this->setPidInGivenEntries([$template], $this);
        $this->templates[] = $template;
        return $this;
    }

    /**
     * Set Domains
     *
     * @param Models\Domain|array $domains
     * @return RootPage
     */
    public function setDomains($domains)
    {
        $this->setPidInGivenEntries($this->convertToArray($domains), $this);
        $this->domains = $this->convertToArray($domains);
        return $this;
    }

    /**
     * Add Domain
     *
     * @param Models\Domain $domain
     * @return RootPage
     */
    public function addDomain(Models\Domain $domain)
    {
        $this->setPidInGivenEntries([$domain], $this);
        $this->domains[] = $domain;
        return $this;
    }

    /**
     * Add modifier to this entity.
     * Modifier must be implemented with PageModifierInterface.
     *
     * @param Modifiers\Interfaces\PageModifierInterface $modifier
     * @return RootPage
     */
    public function addModifier(Modifiers\Interfaces\PageModifierInterface $modifier)
    {
        $this->registerModifier($modifier);
        return $this;
    }
}
