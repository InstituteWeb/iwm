<?php
namespace InstituteWeb\Iwm\Environments\DataProvider\Models\Pages;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Iwm\Environments\DataProvider\Traits;
use InstituteWeb\Iwm\Environments\DataProvider\Models;
use InstituteWeb\Iwm\Environments\DataProvider\Modifiers;

/**
 * Class page
 *
 * @package InstituteWeb\Iwm
 */
class Page extends Models\AbstractSystemEntryModel
{
    use Traits\SettersAcceptStringInputForArrays;
    use Traits\HasModifier;

    const DOKTYPE_STANDARD = 1;
    const DOKTYPE_EXTERNAL_LINK = 3;
    const DOKTYPE_SHORTCUT = 4;
    const DOKTYPE_BACKEND_USER_SECTION = 6;
    const DOKTYPE_MOUNT_POINT = 7;
    const DOKTYPE_MENU_SEPARATOR = 199;
    const DOKTYPE_SYS_FOLDER = 254;

    protected $_inconsistentFieldNames = ['ts_config' => 'TSconfig'];

    protected $_secureProperties = ['sorting'];

    /**
     * @var string
     */
    protected $_tablename = 'pages';

    /**
     * @var string Representing bool
     */
    protected $deleted = '0';

    /**
     * The doktype of the page
     * which is the type defined in "pages" in TCA
     *
     * @var int
     */
    protected $doktype = self::DOKTYPE_STANDARD;

    /**
     * @var int|null;
     */
    protected $sorting;

    /**
     * Flags this page as root page
     *
     * @var string Representing bool
     */
    protected $isSiteroot = '0';

    /**
     * The title of the page
     *
     * @var string
     */
    protected $title;

    /**
     * Collection of SysTemplates
     * which may be defined for any page type. Corresponds with sys_template in TCA
     *
     * @var Models\Template[]
     */
    protected $templates = [];

    /**
     * Collection of SysDomains
     * which may be defined for any page type. Corresponds with sys_domain in TCA
     *
     * @var Models\Domain[]
     */
    protected $domains = [];

    /**
     * Collection of pages
     * which
     * @var array|Page[]
     */
    protected $childPages = [];

    /**
     * @var string
     */
    protected $tsConfig;


    /**
     * Page constructor
     *
     * @param string $title The title of this page
     * @param string $identifier Unique identifier of this entity. Mandantory.
     * @param int|null $sorting
     * @param Page[] $childPages
     * @param array $additionalAttributes
     */
    public function __construct($title, $identifier, $sorting = null, array $childPages = [], $additionalAttributes = [])
    {
        $this->setTitle($title);
        $this->setIdentifier($identifier);

        $this->setChildPages($childPages);
        $this->setSorting($sorting);
        $this->setAdditionalAttributes($additionalAttributes);
    }

    /**
     * Sets the isRoot attribute to true
     *
     * @return Page
     */
    public function flagAsRoot()
    {
        $this->isSiteroot = '1';
        return $this;
    }

    /**
     * Returns label of this record
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->getTitle();
    }

    /**
     * Get Title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set Title
     *
     * @param string $title
     * @return Page
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Get Sorting
     *
     * @return int|null
     */
    public function getSorting()
    {
        if (is_null($this->sorting)) {
            return INF;
        }
        return $this->sorting;
    }

    /**
     * Set Sorting
     *
     * @param int|null $sorting
     * @return AbstractSystemEntryModel
     */
    public function setSorting($sorting)
    {
        $this->sorting = $sorting;
        return $this;
    }

    /**
     * Get ChildPages
     *
     * @return array|Page[]
     */
    public function getChildPages()
    {
        if (!is_array($this->childPages)) {
            return [];
        }
        return $this->childPages;
    }

    /**
     * Set ChildPages
     *
     * @param Page|array $childPages
     * @return Page
     */
    public function setChildPages($childPages)
    {
        $this->setPidInGivenEntries($this->convertToArray($childPages), $this);
        $this->childPages = $this->convertToArray($childPages);
        return $this;
    }

    /**
     * Get Templates
     *
     * @return Models\Template[]
     */
    public function getTemplates()
    {
        if (!is_array($this->templates)) {
            return [];
        }
        return $this->templates;
    }

    /**
     * Get first sys_template if set
     *
     * @return Models\Template|null
     */
    public function getFirstTemplate()
    {
        if (!empty($this->templates)) {
            return reset($this->templates);
        }
        return null;
    }

    /**
     * Set Templates
     *
     * @param Models\Template|array $templates
     * @return Page
     */
    public function setTemplates($templates)
    {
        $this->setPidInGivenEntries($this->convertToArray($templates), $this);
        $this->templates = $this->convertToArray($templates);
        return $this;
    }

    /**
     * Add Template
     *
     * @param Models\Template $template
     * @return Page
     */
    public function addTemplate(Models\Template $template)
    {
        $this->setPidInGivenEntries([$template], $this);
        $this->templates[] = $template;
        return $this;
    }

    /**
     * Get Domains
     *
     * @return Models\Domain[]
     */
    public function getDomains()
    {
        if (!is_array($this->domains)) {
            return [];
        }
        return $this->domains;
    }

    /**
     * It returns the domain with given "forced" flag first.
     * If no given, then the first domain in array
     *
     * @return Models\Domain|null
     */
    public function getFirstDomain()
    {
        foreach ($this->domains as $domain) {
            if ($domain->isForced()) {
                return $domain;
            }
        }
        if (!empty($this->domains)) {
            return $this->domains[0];
        }
        return null;
    }

    /**
     * Set Domains
     *
     * @param Models\Domain|array $domains
     * @return Page
     */
    public function setDomains($domains)
    {
        $this->setPidInGivenEntries($this->convertToArray($domains), $this);
        $this->domains = $this->convertToArray($domains);
        return $this;
    }

    /**
     * Add Domain
     *
     * @param Models\Domain $domain
     * @return Page
     */
    public function addDomain(Models\Domain $domain)
    {
        $this->setPidInGivenEntries([$domain], $this);
        $this->domains[] = $domain;
        return $this;
    }

    /**
     * Get TsConfig
     *
     * @return string
     */
    public function getTsConfig()
    {
        return $this->tsConfig;
    }

    /**
     * Set TsConfig
     *
     * @param string $tsConfig
     * @return void
     */
    public function setTsConfig($tsConfig)
    {
        $this->tsConfig = $tsConfig;
    }

    /**
     * Add modifier to this entity.
     * Modifier must be implemented with PageModifierInterface.
     *
     * @param Modifiers\Interfaces\PageModifierInterface $modifier
     * @return Page
     */
    public function addModifier(Modifiers\Interfaces\PageModifierInterface $modifier)
    {
        $this->registerModifier($modifier);
        return $this;
    }
}
