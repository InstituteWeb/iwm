<?php
namespace InstituteWeb\Iwm\Environments\DataProvider\Models\Proxy;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Iwm\Environments\DataProvider\Models\AbstractSystemEntryModel;

/**
 * Proxy Model
 * which is used in \InstituteWeb\Iwm\Environments\DataProvider\EntityManager::get() only
 *
 * @package InstituteWeb\Iwm
 * @internal
 */
class ProxyModel extends AbstractSystemEntryModel
{
    /**
     * @var array
     */
    protected $calls = [];

    /**
     * RootPageProxy constructor.
     *
     * @param string $title
     * @param string $identifier
     * @internal
     */
    public function __construct($title, $identifier = null)
    {
        $this->title = $title;
        $this->identifier = $identifier;
    }

    /**
     * Fakes setPid method
     *
     * @param AbstractSystemEntryModel $pid
     * @return AbstractSystemEntryModel The skeleton
     */
    public function setPid(AbstractSystemEntryModel $pid)
    {
        return $this;
    }

    /**
     * Magic call method to record all access to not existing methods
     *
     * @param string $name
     * @param array $arguments
     * @return mixed
     */
    public function __call($name, array $arguments)
    {
        $this->calls[] = [
            'name' => $name,
            'arguments' => $arguments
        ];
        return $this;
    }
}
