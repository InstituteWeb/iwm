<?php
namespace InstituteWeb\Iwm\Environments\DataProvider\Models;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Iwm\Environments\DataProvider\Models\Services;
use InstituteWeb\Iwm\Environments\DataProvider\Modifiers\Interfaces;
use InstituteWeb\Iwm\Environments\DataProvider\Traits;

/**
 * Class Template
 * for table sys_template
 *
 * @package InstituteWeb\Iwm
 */
class Template extends AbstractSystemEntryModel
{
    use Traits\SettersAcceptStringInputForArrays;
    use Traits\HasModifier;

    const CLEAR_NONE = 0;
    const CLEAR_CONSTANTS = 1;
    const CLEAR_SETUP = 2;
    const CLEAR_BOTH = 3;

    /**
     * @var string
     */
    protected $_tablename = 'sys_template';

    /**
     * @var string Representing bool
     */
    protected $deleted = '0';

    /**
     * @var int|null;
     */
    protected $sorting;

    /**
     * Title of sys_template
     *
     * @var string
     */
    protected $title;

    /**
     * Include static paths (from extensions)
     * Directory paths in EXT: syntax, with ending slash.
     *
     * @var string
     */
    protected $includeStaticFile;

    /**
     * @var bool
     */
    protected $root = true;

    /**
     * @var int
     */
    protected $clear = self::CLEAR_BOTH;

    /**
     * @var string
     */
    public $constants;

    /**
     * @var string
     */
    protected $config;

    /**
     * Template constructor
     *
     * @param string $title A name for this template
     * @param string $identifier Unique identifier of this entity. Mandantory.
     * @param null|int $sorting
     * @param array $additionalAttributes
     */
    public function __construct($title, $identifier, $sorting = null, $additionalAttributes = [])
    {
        $this->setTitle($title);
        $this->setIdentifier($identifier);

        $this->setSorting($sorting);
        $this->setAdditionalAttributes($additionalAttributes);
    }

    /**
     * Returns label of this record
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->getTitle();
    }

    /**
     * Get Title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set Title
     *
     * @param string $title
     * @return Template
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Get Sorting
     *
     * @return int|null
     */
    public function getSorting()
    {
        if (is_null($this->sorting)) {
            return INF;
        }
        return $this->sorting;
    }

    /**
     * Set Sorting
     *
     * @param int|null $sorting
     * @return AbstractSystemEntryModel
     */
    public function setSorting($sorting)
    {
        $this->sorting = $sorting;
        return $this;
    }

    /**
     * Include static (from extensions)
     * Adds constants and setup from given path to template.
     *
     * Example path: "EXT:fluid_styled_content/Configuration/TypoScript/Static/"
     *
     * @param string|array $pathsToInclude It is important to respect the format of the paths
     * @return Template
     */
    public function setIncludeStaticFile($pathsToInclude)
    {
        if (is_array($pathsToInclude)) {
            $pathsToInclude = implode(',', $pathsToInclude);
        }
        $this->includeStaticFile = $pathsToInclude;
        return $this;
    }

    /**
     * Add this template to given page
     *
     * @param Pages\Page $page
     * @return Template
     */
    public function addTo(\InstituteWeb\Iwm\Environments\DataProvider\Models\Pages\Page $page)
    {
        $page->addTemplate($this);
        return $this;
    }

    /**
     * Set Root
     *
     * @param boolean $root
     * @return void
     */
    public function setRoot($root)
    {
        $this->root = $root;
    }

    /**
     * Set Clear
     *
     * @param int $clear
     * @return void
     */
    public function setClear($clear)
    {
        $this->clear = $clear;
    }

    /**
     * Get Service
     * Also instantiate the service, when non given.
     *
     * @return Services\TemplateService
     */
    public function service()
    {
        return $this->traitService($this);
    }

    /**
     * Get Constants
     *
     * @return string
     */
    public function getConstants()
    {
        return $this->constants;
    }

    /**
     * Set Constants
     *
     * @param string $constants
     * @return void
     */
    public function setConstants($constants)
    {
        $this->constants = $constants;
    }

    /**
     * Get Config
     *
     * @return string
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Set Config
     *
     * @param string $config
     * @return void
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * Add modifier to this entity.
     * Modifier must be implemented with TemplateModifierInterface.
     *
     * @param Interfaces\TemplateModifierInterface $modifier
     * @return Template
     */
    public function addModifier(Interfaces\TemplateModifierInterface $modifier)
    {
        $this->registerModifier($modifier);
        return $this;
    }
}
