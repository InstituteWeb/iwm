<?php
namespace InstituteWeb\Iwm\Environments\DataProvider;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Iwm\Environments\DataProvider\Models;
use InstituteWeb\Iwm\Environments\DataProvider\Traits;
use Tree\Node\Node;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class ConstantsManager
 *
 * @package InstituteWeb\Iwm
 */
class EntityManager implements \TYPO3\CMS\Core\SingletonInterface
{
    /**
     * @var array
     */
    protected $entities = [];

    /**
     * @var string
     */
    protected $source;

    /**
     * @var array
     */
    protected $keySources;


    /**
     * Check if entity manager has configured entries
     *
     * @return bool
     */
    public function hasEntries()
    {
        return count($this->getAll()) > 0;
    }

    /**
     * Get all entities as flat array with identifier as key
     *
     * @return array
     */
    public function getAll()
    {
        if (count($this->entities) === 0) {
            return [];
        }
        $this->processModifiers();
        return $this->entities;
    }

    /**
     * Get entity by identifier
     *
     * @param string $identifier
     * @return Models\AbstractSystemEntryModel|null
     */
    public function get($identifier)
    {
        if (!isset($this->entities[$identifier])) {
            return new Models\Proxy\ProxyModel('', $identifier);
        }
        $this->processModifiers();
        return $this->entities[$identifier];
    }

    /**
     * Add entity to entity manager
     * Overwrites existing data, when called twice with same key
     *
     * @param string $identifier
     * @param Models\AbstractSystemEntryModel $value
     * @return EntityManager
     */
    public function add($identifier, Models\AbstractSystemEntryModel $value)
    {
        $this->keySources[$identifier] = $this->source;
        $this->entities[$identifier] = $value;
        return $this;
    }

    /**
     * Process modifiers on $this->entities (all)
     *
     * @param int $priority
     * @return void
     */
    public function processModifiers($priority = Modifiers\AbstractModifier::PRIORITY_NORMAL)
    {
        /** @var Models\Pages\RootPage $entity */
        foreach ($this->entities as $entity) {
            if (method_exists($entity, 'registerModifier')) {
                $entity->processModifiers($priority);
            }
        }
    }

    /**
     * Get nested nodes
     * with correct sortings (if $orderBySorting is true)
     *
     * @return Node Given node with correct sortings
     */
    public function getAllAsNestedNodes()
    {
        $allNodes = [];
        foreach ($this->getAll() as $key => $entity) {
            $allNodes[$key] = new Node($entity);
        }

        $rootNode = new Node(new \stdClass());
        foreach ($allNodes as $key => $node) {
            /** @var Models\AbstractSystemEntryModel $entity */
            $entity = $node->getValue();
            if (!is_null($entity->getPid())) {
                if (!array_key_exists((string) $entity->getPid(), $allNodes)) {
                    $temporaryProxyNode = new Node(new Models\Proxy\ProxyModel('', $entity->getPid()));
                    $allNodes[$entity->getPid()] = $temporaryProxyNode;
                    $rootNode->addChild($temporaryProxyNode);
                    continue;
                }
                /** @var Node $newNode */
                $newNode = $allNodes[(string) $entity->getPid()];
                $newNode->addChild($node);

            } else {
                $rootNode->addChild($node);
            }
        }
        return $this->orderNestedNodes($rootNode);
    }

    /**
     * Filters all entries by given class name
     *
     * @param string $className
     * @return Models\AbstractSystemEntryModel[] Based on given class name
     */
    protected function filterEntities($className)
    {
        $filteredEntries = [];
        foreach ($this->getAll() as $entity) {
            if ($entity instanceof $className) {
                $filteredEntries[] = $entity;
            }
        }
        return $filteredEntries;
    }

    /**
     * Filters root page entities from all entities.
     *
     * @return Models\Pages\RootPage[]
     */
    public function getRootPages()
    {
        return $this->filterEntities(Models\Pages\RootPage::class);
    }

    /**
     * Filters domain entities from all entities.
     *
     * @return Models\Domain[]
     */
    public function getDomains()
    {
        return $this->filterEntities(Models\Domain::class);
    }

    /**
     * Filters language entities from all entities.
     *
     * @return Models\Language[]
     */
    public function getLanguages()
    {
        return $this->filterEntities(Models\Language::class);
    }

    /**
     * Orders given node entities by sorting, recursively
     *
     * @param Node $node
     * @return Node
     * @see $this->getAllAsNestedNodes() method
     */
    protected function orderNestedNodes(Node $node)
    {
        /** @var Node[] $children */
        $children = $node->getChildren();

        $childrenSortings = [];
        foreach ($children as $child) {
            if (method_exists($child->getValue(), 'getSorting')) {
                $childrenSortings[] = $child->getValue()->getSorting();
            } else {
                $childrenSortings[] = null;
            }
        }
        $childrenHaveAllSameSortings = count(array_unique($childrenSortings)) === 1;

        if (!$childrenHaveAllSameSortings) {
            // Group children by entity classname
            /** @var Node[] $childrenGroupedByClassnames */
            $childrenGroupedByClassnames = [];
            foreach ($children as $child) {
                if (!is_array($childrenGroupedByClassnames[get_class($child->getValue())])) {
                    $childrenGroupedByClassnames[get_class($child->getValue())] = [];
                }
                $childrenGroupedByClassnames[get_class($child->getValue())][] = $child;
            }

            // Order types alphabetically
            ksort($childrenGroupedByClassnames);

            /** @var Node[] $children */
            $groupedChildren = [];
            foreach ($childrenGroupedByClassnames as $className => $groupedNodes) {
                foreach ($groupedNodes as $className => $singleNode) {
                    $groupedChildren[] = $child;
                }
            }

            usort($groupedChildren, function (Node $nodeA, Node $nodeB) {
                $nodeA = method_exists($nodeA, 'getSorting') ? $nodeA->getValue()->getSorting() : 0;
                $nodeB = method_exists($nodeB, 'getSorting') ? $nodeB->getValue()->getSorting() : 0;

                if ($nodeA === $nodeB) {
                    // This ensures that entities are processed in order as instantiated (in All.php, e.g.)
                    return 1;
                }
                return $nodeA < $nodeB ? -1 : 1;
            });
        }

        $node->setChildren($children);

        foreach ($children as $child) {
            if (!$child->isLeaf()) {
                $this->orderNestedNodes($child);
            }
        }
        return $node;
    }

    /**
     * Set the current source (configuration file)
     *
     * @param string|null $file
     * @return void
     */
    public function setCurrentSource($file = null)
    {
        $this->source = $file;
    }

    /**
     * Get path of configuration in which the given entitiy has been defined
     *
     * @param Models\AbstractSystemEntryModel $entity
     * @return string|null Path to php configuration file
     */
    public function getSourceByEntity(Models\AbstractSystemEntryModel $entity)
    {
        if (method_exists($entity, 'getIdentifier') && isset($this->keySources[$entity->getIdentifier()])) {
            return $this->keySources[$entity->getIdentifier()];
        }
        return null;
    }

    /**
     * Resets the constants
     *
     * @return void
     */
    public function reset()
    {
        $this->entities = [];
    }

    /**
     * Get singleton instance of Constants Manager
     *
     * @return EntityManager
     */
    public static function getInstance()
    {
        return GeneralUtility::makeInstance(static::class);
    }
}
