<?php
namespace InstituteWeb\Iwm\Environments\DataProvider\Traits;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Iwm\Environments\DataProvider\Modifiers;

/**
 * Trait HasModifier
 *
 * @package InstituteWeb\Iwm
 */
trait HasModifier
{
    /**
     * @var Modifiers\AbstractModifier[]
     */
    protected $modifiers = [];

    /**
     * Registers given service instance
     *
     * @param Modifiers\Interfaces\AbstractModifierInterface $modifier
     * @return void
     * @internal
     */
    protected function registerModifier(Modifiers\Interfaces\AbstractModifierInterface $modifier)
    {
        $modifier->setEntity($this);
        $this->modifiers[] = $modifier;
    }

    /**
     * Checks all registred modifiers for given class name. When found and when the class has
     * the method getInfo() implemented, its value get returned.
     *
     * Because they can be more than one modifier with same class name registred, it is
     * recommeded to return array values in modifier's getInfo() method.
     *
     * This method returns an array of return values or null when no class has been found or
     * an empty array when the found modifier class has no getInfo() method implemented.
     *
     * @param string $modifierClassName
     * @return array|null
     */
    public function getModifierInfo($modifierClassName)
    {
        if (!class_exists($modifierClassName)) {
            return null;
        }

        $info = [];
        foreach ($this->modifiers as $modifier) {
            if ($modifier instanceof $modifierClassName) {
                if (method_exists($modifier, 'getInfo')) {
                    $info = array_merge(
                        $info,
                        is_array($modifier->getInfo()) ? $modifier->getInfo() : [$modifier->getInfo()]
                    );
                }
            }
        }
        return $info;
    }

    /**
     * Executes the registred modifiers, by calling its modify() method.
     * Before doing this, the modifiers get ordered by its defined priority.
     *
     * @param int $priority Defines which priorities should be respected. Default is PRIORITY_NORMAL which does not
     *                      execute modifiers with higher priority but all with same or lower one.
     * @return bool Returns true when all modifiers ran. The first exception will result in a false.
     */
    public function processModifiers($priority = Modifiers\AbstractModifier::PRIORITY_NORMAL)
    {
        if (empty($this->modifiers)) {
            return false;
        }
        try {
            $modifiersByPriority = $this->modifiers;
            usort($modifiersByPriority, function (Modifiers\AbstractModifier $a, Modifiers\AbstractModifier $b) {
                $a = $a->getPriority();
                $b = $b->getPriority();

                if ($a === $b) {
                    return 0;
                }
                return $a < $b ? -1 : 1;
            });
            foreach ($modifiersByPriority as $modifier) {
                if ($modifier->getPriority() <= $priority) {
                    $modifier->modify();
                }
            }
        } catch (\Exception $exception) {
            return false;
        }
        return true;
    }
}
