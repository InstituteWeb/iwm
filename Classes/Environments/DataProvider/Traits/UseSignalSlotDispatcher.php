<?php
namespace InstituteWeb\Iwm\Environments\DataProvider\Traits;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Iwm\Scripts\Helper\File;
use TYPO3\CMS\Extbase\SignalSlot;

/**
 * Trait HasSignal
 *
 * For models which provide a ->service() method.
 *
 * @package InstituteWeb\Iwm
 */
trait UseSignalSlotDispatcher
{
    /**
     * Instance cache manager just once
     *
     * @var bool
     */
    private $_cacheManagerInitialized = false;

    /**
     * Instance a new signalSlotDispatcher and offer a signal
     *
     * @param string $signalName
     * @param array $arguments
     */
    protected function signalDispatch($signalName, array $arguments = [])
    {
        if (!$this->_cacheManagerInitialized) {
            if (!isset($GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations'])) {
                $GLOBALS['TYPO3_CONF_VARS'] = array_merge_recursive(
                    File::getDefaultConfigurationValues(),
                    $GLOBALS['TYPO3_CONF_VARS']
                );
            }

            $cacheManager = new \TYPO3\CMS\Core\Cache\CacheManager();
            $cacheManager->setCacheConfigurations($GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']);
            \TYPO3\CMS\Core\Utility\GeneralUtility::setSingletonInstance(\TYPO3\CMS\Core\Cache\CacheManager::class, $cacheManager);

            $cacheFactory = new \TYPO3\CMS\Core\Cache\CacheFactory('production', $cacheManager);
            \TYPO3\CMS\Core\Utility\GeneralUtility::setSingletonInstance(\TYPO3\CMS\Core\Cache\CacheFactory::class, $cacheFactory);
            $this->_cacheManagerInitialized = true;
        }
        /** @var SignalSlot\Dispatcher $signalSlotDispatcher */
        $signalSlotDispatcher = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(SignalSlot\Dispatcher::class);
        $signalSlotDispatcher->dispatch(get_class($this), $signalName, $arguments);
    }

    /**
     * Connects a signal with a slot.
     *
     * @param string $signalClassName Name of the class containing the signal
     * @param string $signalName Name of the signal
     * @param mixed $slotClassNameOrObject Name of the class containing the slot or the instantiated class or a Closure object
     * @param string $slotMethodName Name of the method to be used as a slot. If $slotClassNameOrObject is a Closure object, this parameter is ignored
     * @param bool $passSignalInformation If set to TRUE, the last argument passed to the slot will be information about the signal (EmitterClassName::signalName)
     * @throws \InvalidArgumentException
     * @return void
     * @see \TYPO3\CMS\Extbase\SignalSlot\Dispatcher::connect
     */
    protected function connectSignalWithSlot($signalClassName, $signalName, $slotClassNameOrObject, $slotMethodName = '', $passSignalInformation = true)
    {
        /** @var SignalSlot\Dispatcher $signalSlotDispatcher */
        $signalSlotDispatcher = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(SignalSlot\Dispatcher::class);
        $signalSlotDispatcher->connect($signalClassName, $signalName, $slotClassNameOrObject, $slotMethodName, $passSignalInformation);
    }
}
