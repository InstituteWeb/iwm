<?php
namespace InstituteWeb\Iwm\Environments\DataProvider\Traits;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

/**
 * Trait SettersAcceptStringInputForArrays
 *
 * When a class uses this trait, which you see at the very top of a class,
 * you can expect that all setters in this class which have array arguments also
 * allow string input, which gets converted automatically.
 *
 * Also numbers can be added, they don't get converted!
 *
 * For convenience reasons and to lower the threshold for beginners.
 *
 * @package InstituteWeb\Iwm
 */
trait SettersAcceptStringInputForArrays
{
    /**
     * Converts given value to array or returns array untouched.
     *
     * @param mixed $value to check for if it is an array and if not, to convert it
     * @return array with at least one entry
     */
    protected function convertToArray($value)
    {
        if (!is_array($value)) {
            $value = [$value];
        }
        return $value;
    }
}
