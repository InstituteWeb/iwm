<?php
namespace InstituteWeb\Iwm\Environments\DataProvider\Traits;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Iwm\Environments\DataProvider\Models\AbstractSystemEntryModel;
use InstituteWeb\Iwm\Environments\DataProvider\Models\Services;
use InstituteWeb\Iwm\Scripts\Helper\File;

/**
 * Trait UseTypo3TempCache
 *
 * Get and store data in typo3temp/ directory.
 *
 * @package InstituteWeb\Iwm
 */
trait UseTypo3TempCache
{
    /**
     * @var string
     */
    private $filePrefix = 'iwm_';


    /**
     * Get file path in typo3temp/
     *
     * @param string $key
     * @return string
     */
    protected function getCacheFilePath($key)
    {
        return File::concatenatePathParts([File::getTypo3RootPath(), 'typo3temp', $this->filePrefix . $key]);
    }

    /**
     * Checks if value exists in typo3temp/
     *
     * @param string $key
     * @return bool True if value exists, otherwise false
     */
    protected function valueInTypo3TempExists($key)
    {
        return file_exists($this->getCacheFilePath($key));
    }

    /**
     * Reads cache content. Returns FALSE if file does not exist
     *
     * @return bool|string
     */
    protected function getValueFromTypo3Temp($key)
    {
        if ($this->valueInTypo3TempExists($key)) {
            return file_get_contents($this->getCacheFilePath($key));
        }
        return false;
    }

    /**
     * Writes content to cache file
     *
     * @param string $key
     * @param string $value
     * @param bool $append When true the given value get appended to file
     * @return void
     */
    protected function setValueInTypo3Temp($key, $value, $append = false)
    {
        $flags = null;
        if ($append && file_exists($this->getCacheFilePath($key))) {
            $flags = FILE_APPEND;
            $value = PHP_EOL . $value;
        }
        $status = file_put_contents($this->getCacheFilePath($key), $value, $flags);
        if (!$status) {
            throw new \RuntimeException('Can\'t write to file: ' . $this->getCacheFilePath($key));
        }
    }
}
