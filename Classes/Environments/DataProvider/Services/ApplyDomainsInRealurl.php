<?php
namespace InstituteWeb\Iwm\Environments\DataProvider\Services;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Iwm\Environments\DataProvider\EntityManager;
use InstituteWeb\Iwm\Environments\DataProvider\Models;
use InstituteWeb\Iwm\Environments\DataProvider\Modifiers;
use InstituteWeb\Iwm\Environments\DataProvider\Traits;
use InstituteWeb\Iwm\Environments\Environment;
use InstituteWeb\Iwm\Scripts\Helper\File;

/**
 * ApplyDomainsInRealurl Service
 * =============================
 *
 * Register
 * --------
 * Just make ` new \InstituteWeb\Iwm\Environments\DataProvider\Services\ApplyDomainsInRealurl()` to register this
 * service with the early operations slot.
 *
 * What it does
 * ------------
 * It loads the file typo3conf/UrlConfiguration.php which contains the realurl configuration array. For each rootpage
 * which if defined this service is making a copy of realurl's _DEFAULT configuration and apply the
 * ['pagePath']['rootpage_id'] and also fill in ['preVars']['L']['valueMap'] automatically.
 *
 * Also it checks $_SERVER['REQUEST_URI'] and set $GLOBALS['TYPO3_CONF_VARS']['FE']['pageNotFound_handling'] to
 * translated 404 page, if existing. Otherwise to default.
 *
 *
 * @package InstituteWeb\Iwm
 */
class ApplyDomainsInRealurl
{
    use Traits\UseSignalSlotDispatcher;

    const SLOT = Environment::SLOT_EARLYOPERATIONS;

    /**
     * ApplyDomainsInRealurl constructor
     * Connect this service with earlyOperations slot
     *
     * @return ApplyDomainsInRealurl
     */
    public function __construct()
    {
        $this->connectSignalWithSlot(
            Environment::class,
            Environment::SLOT_EARLYOPERATIONS,
            ApplyDomainsInRealurl::class,
            ApplyDomainsInRealurl::SLOT
        );
    }

    /**
     * Signal called in Environment::apply() method
     *
     * @param Environment $environment
     * @param EntityManager $entityManager
     */
    public function earlyOperations(Environment $environment, EntityManager $entityManager)
    {
        // TODO: Make configurable
        $urlConfiguration = File::loadPhpConfigurationArray(File::getTypo3ConfPath() . 'UrlConfiguration.php');

        // Apply urlConfiguration to realurl configuration
        if (!empty($entityManager->getDomains())) {
            foreach ($entityManager->getDomains() as $domain) {
                /** @var Models\Pages\RootPage $rootPage */
                $rootPage = $domain->getPid();

                $urlConfiguration[$domain->getDomainName()] = $urlConfiguration['_DEFAULT'] ?: [];
                $urlConfiguration[$domain->getDomainName()]['preVars']['L']['valueMap'] = $this->getValueMappingForLanguages($rootPage);
                $urlConfiguration[$domain->getDomainName()]['pagePath']['rootpage_id'] = $rootPage->_getUid();
            }
        } else {
            $rootPages = $entityManager->getRootPages();
            /** @var Models\Pages\RootPage $firstRootPage */
            $firstRootPage = reset($rootPages);
            if ($firstRootPage instanceof Models\Pages\RootPage) {
                $urlConfiguration['_DEFAULT']['preVars']['L']['valueMap'] = $this->getValueMappingForLanguages($firstRootPage);
                $urlConfiguration['_DEFAULT']['pagePath']['rootpage_id'] = $firstRootPage->_getUid();
            }
        }
        $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'] = $urlConfiguration;


        $currentUrlConfiguration = isset($urlConfiguration[$_SERVER['HTTP_HOST']]) ? $urlConfiguration[$_SERVER['HTTP_HOST']] : $urlConfiguration['_DEFAULT'];

        // Enable multi language 404 pages
        if (isset($_SERVER['REQUEST_URI']) && isset($_SERVER['HTTP_HOST']) && $currentUrlConfiguration['preVars']['L']['valueMap']) {
            $requestedLanguageKey = substr($_SERVER['REQUEST_URI'], 1, 2);
            if ($requestedLanguageKey && array_key_exists($requestedLanguageKey, $currentUrlConfiguration['preVars']['L']['valueMap'])) {
                $GLOBALS['TYPO3_CONF_VARS']['FE']['pageNotFound_handling']
                    = $requestedLanguageKey . '/' . $GLOBALS['TYPO3_CONF_VARS']['FE']['pageNotFound_handling'];
            }
        }
    }

    /**
     * Get value mapping for enabled languages in given root page
     *
     * @param Models\Pages\RootPage $rootPage
     * @return array
     */
    protected function getValueMappingForLanguages(Models\Pages\RootPage $rootPage)
    {
        $enabledLanguages = $rootPage->getModifierInfo(Modifiers\Page\RestrictAvailableLanguagesTo::class);
        $valueMap = [];
        foreach (EntityManager::getInstance()->getLanguages() as $language) {
            if (empty($enabledLanguages) || in_array($language, $enabledLanguages)) {
                $valueMap[$language->getLanguageIsocode()] = $language->_getUid();
            }
        }
        return $valueMap;
    }
}
