<?php
namespace InstituteWeb\Iwm\Environments\DataProvider\Services;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Iwm\Environments\DataProvider\EntityManager;
use InstituteWeb\Iwm\Environments\DataProvider\Models;
use InstituteWeb\Iwm\Environments\DataProvider\Traits;
use InstituteWeb\Iwm\Environments\Environment;

/**
 * AutoDomainRecordByHostname Service
 * ==================================
 *
 * Don't use this serivce!
 *
 *
 * @package InstituteWeb\Iwm
 * @deprecated Don't use this class. It is not 100% working.
 * @todo Make a better version of this one.
 */
class AutoDomainRecordByHostname
{
    use Traits\UseTypo3TempCache;
    use Traits\UseSignalSlotDispatcher;

    const CACHE_IDENTIFIER = 'auto_domain_record';

    const SLOT = Environment::SLOT_EARLYOPERATIONS;

    /**
     * AutoDomainRecordByHostname constructor
     * Connect this service with earlyOperations slot
     *
     * @return AutoDomainRecordByHostname
     * @deprecated
     */
    public function __construct()
    {
        $this->connectSignalWithSlot(
            Environment::class,
            Environment::SLOT_EARLYOPERATIONS,
            AutoDomainRecordByHostname::class,
            AutoDomainRecordByHostname::SLOT
        );
    }

    /**
     * Signal called in Environment::apply() method
     *
     * @param Environment $environment
     * @param EntityManager $entityManager
     * @throws \Exception
     */
    public function earlyOperations(Environment $environment, EntityManager $entityManager)
    {
        // TODO: not properly working
        $rootPages = $entityManager->getRootPages();
        /** @var Models\Pages\RootPage $rootPage */
        $rootPageUid = $this->getRootPageUidToPointTo($rootPages);
        if (empty($rootPageUid)) {
            return;
        }

        $hostName = $_SERVER['HTTP_HOST'];
        if ($this->hostNameExistsInConfiguration($hostName)) {
            return;
        }

        if (strpos($this->getValueFromTypo3Temp(static::CACHE_IDENTIFIER), $hostName) !== false) {
            return;
        }

        $database = $environment->getDatabaseConnection();
        $existingDomainRecord = $database->exec_SELECTgetSingleRow(
            'pid,domainName',
            'sys_domain',
            'domainName="' . $hostName . '"'
        );

        if ($existingDomainRecord && (int) $existingDomainRecord['pid'] === $rootPageUid) {
            $this->setValueInTypo3Temp(static::CACHE_IDENTIFIER, $hostName, true);
            return;
        }

        if ($existingDomainRecord && (int) $existingDomainRecord['pid'] !== $rootPageUid) {
            $existingDomainRecord['pid'] = $rootPageUid;
            $status = $database->exec_UPDATEquery('sys_domain', 'uid = ' . $existingDomainRecord['uid'], $existingDomainRecord);
            if ($status) {
                $this->setValueInTypo3Temp(static::CACHE_IDENTIFIER, $hostName, true);
            }
            return;
        }

        $status = $database->exec_INSERTquery('sys_domain', [
            'pid' => $rootPageUid,
            'domainName' => $hostName,
            'forced' => '1',
            'sorting' => '0',
            'tstamp' => time(),
            'crdate' => time()
        ]);
        if ($status) {
            $this->setValueInTypo3Temp(static::CACHE_IDENTIFIER, $hostName, true);
        }
    }

    /**
     * Get uid of root page, to create new domain record in it
     *
     * @param Models\Pages\RootPage[] $rootPages
     * @return int uid of root page
     */
    protected function getRootPageUidToPointTo(array $rootPages)
    {
        foreach ($rootPages as $rootPage) {
            if ($rootPage->service()->isCreateDomainRecordForNewHostnames()) {
                return (int) Environment::getInstance()->getEntityMapping($rootPage->getIdentifier());
            }
        }
        return 0;
    }

    /**
     * Checks if given host name exists as domain entity in configuration
     *
     * @param string $hostName
     * @return bool
     */
    protected function hostNameExistsInConfiguration($hostName)
    {
        foreach (EntityManager::getInstance()->getDomains() as $domain) {
            if ($hostName === $domain->getDomainName()) {
                return true;
            }
        }
        return false;
    }
}
