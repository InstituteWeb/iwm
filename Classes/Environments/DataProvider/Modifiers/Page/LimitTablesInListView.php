<?php
namespace InstituteWeb\Iwm\Environments\DataProvider\Modifiers\Page;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Iwm\Environments\DataProvider\Models;
use InstituteWeb\Iwm\Environments\DataProvider\Modifiers;
use InstituteWeb\Iwm\Environments\DataProvider\Modifiers\Interfaces;
use InstituteWeb\Iwm\Environments\DataProvider\Utility\StringUtility;

/**
 * Restricts entity types in list view to be able to get create
 *
 * @package InstituteWeb\Iwm
 */
class LimitTablesInListView extends Modifiers\AbstractModifier implements Interfaces\PageModifierInterface
{
    use \InstituteWeb\Iwm\Environments\DataProvider\Traits\SettersAcceptStringInputForArrays;

    /**
     * @var Models\Pages\Page
     */
    protected $entity;

    /**
     * @var array
     */
    protected $limitTablesInListView = [];

    /**
     * LimitTablesInListView constructor
     *
     * @param array $tables
     * @return LimitTablesInListView
     */
    public function __construct($tables)
    {
        $this->limitTablesInListView = $this->convertToArray($tables);
    }

    /**
     * Modifies given entity
     *
     * @return void
     */
    public function modify()
    {
        if ($this->hasNotBeenProcessed()) {
            $text = 'mod.web_list.allowedNewTables = ' . implode(',', $this->limitTablesInListView);
            $tsConfig = StringUtility::append($this->entity->getTsConfig(), $text);
            $this->entity->setTsConfig($tsConfig);
        }
    }

    /**
     * Returns priority of modifier. One digit int from 0 (low) to 9 (high)
     *
     * @return int
     */
    public function getPriority()
    {
        return parent::getPriority();
    }
}
