<?php
namespace InstituteWeb\Iwm\Environments\DataProvider\Modifiers\Page;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Iwm\Environments\DataProvider\EntityManager;
use InstituteWeb\Iwm\Environments\DataProvider\Models;
use InstituteWeb\Iwm\Environments\DataProvider\Modifiers;
use InstituteWeb\Iwm\Environments\DataProvider\Modifiers\Interfaces;
use InstituteWeb\Iwm\Environments\DataProvider\Utility\StringUtility;

/**
 * Includes pageTS files to page
 *
 * @package InstituteWeb\Iwm
 */
class IncludePageTsFiles extends Modifiers\AbstractModifier implements Interfaces\PageModifierInterface
{
    use \InstituteWeb\Iwm\Environments\DataProvider\Traits\SettersAcceptStringInputForArrays;

    /**
     * @var Models\Pages\Page
     */
    protected $entity;

    /**
     * @var array
     */
    protected $pageTsFilesToInclude = [];

    /**
     * SetDefaultLanguage constructor
     *
     * @param array $paths
     * @return IncludePageTsFiles
     */
    public function __construct($paths)
    {
        $this->pageTsFilesToInclude = array_merge($this->pageTsFilesToInclude, $this->convertToArray($paths));
    }

    /**
     * Modifies given entity
     *
     * @return void
     */
    public function modify()
    {
        if ($this->hasNotBeenProcessed()) {
            $text = '';
            foreach ($this->pageTsFilesToInclude as $filePath) {
                $text .= '<INCLUDE_TYPOSCRIPT: source="FILE:' . $filePath . '">' . PHP_EOL;
            }

            $tsConfig = StringUtility::append($this->entity->getTsConfig(), $text);
            $this->entity->setTsConfig($tsConfig);
        }
    }

    /**
     * Returns priority of modifier. One digit int from 0 (low) to 9 (high)
     *
     * @return int
     */
    public function getPriority()
    {
        return parent::getPriority();
    }
}
