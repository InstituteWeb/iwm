<?php
namespace InstituteWeb\Iwm\Environments\DataProvider\Modifiers\Page;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Iwm\Environments\DataProvider\EntityManager;
use InstituteWeb\Iwm\Environments\DataProvider\Models;
use InstituteWeb\Iwm\Environments\DataProvider\Modifiers;
use InstituteWeb\Iwm\Environments\DataProvider\Modifiers\Interfaces;
use InstituteWeb\Iwm\Environments\DataProvider\Utility\StringUtility;

/**
 * Set default language in backend
 *
 * @package InstituteWeb\Iwm
 */
class SetDefaultLanguage extends Modifiers\AbstractModifier implements Interfaces\PageModifierInterface
{
    /**
     * @var Models\Pages\Page
     */
    protected $entity;

    /**
     * @var string
     */
    protected $defaultLanguageName;

    /**
     * @var string
     */
    protected $defaultLanguageFlag;

    /**
     * SetDefaultLanguage constructor
     *
     * @param string $defaultLanguageName
     * @param string $defaultLanguageFlag
     * @return SetDefaultLanguage
     */
    public function __construct($defaultLanguageName, $defaultLanguageFlag)
    {
        $this->defaultLanguageName = $defaultLanguageName;
        $this->defaultLanguageFlag = $defaultLanguageFlag;
    }

    /**
     * Modifies given entity
     *
     * @return void
     */
    public function modify()
    {
        if ($this->hasNotBeenProcessed()) {
            $text = 'mod.SHARED.defaultLanguageLabel = ' . $this->defaultLanguageName . PHP_EOL;
            $text .= 'mod.SHARED.defaultLanguageFlag = ' . $this->defaultLanguageFlag . PHP_EOL;

            $tsConfig = StringUtility::append($this->entity->getTsConfig(), $text);
            $this->entity->setTsConfig($tsConfig);
        }
    }

    /**
     * Returns priority of modifier. One digit int from 0 (low) to 9 (high)
     *
     * @return int
     */
    public function getPriority()
    {
        return parent::getPriority();
    }
}
