<?php
namespace InstituteWeb\Iwm\Environments\DataProvider\Modifiers\Page;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Iwm\Environments\DataProvider\EntityManager;
use InstituteWeb\Iwm\Environments\DataProvider\Models;
use InstituteWeb\Iwm\Environments\DataProvider\Modifiers;
use InstituteWeb\Iwm\Environments\DataProvider\Modifiers\Interfaces;
use InstituteWeb\Iwm\Environments\DataProvider\Utility\StringUtility;

/**
 * Restricts available languages in TYPO3 backend for this page (and all subpages) to given languages
 *
 * @package InstituteWeb\Iwm
 */
class RestrictAvailableLanguagesTo extends Modifiers\AbstractModifier implements Interfaces\PageModifierInterface
{
    use \InstituteWeb\Iwm\Environments\DataProvider\Traits\SettersAcceptStringInputForArrays;

    /**
     * @var Models\Pages\Page
     */
    protected $entity;

    /**
     * @var Models\Language[]
     */
    protected $languages = [];

    /**
     * RestrictAvailableLanguagesTo constructor
     *
     * @param array $languages
     * @return RestrictAvailableLanguagesTo
     */
    public function __construct($languages)
    {
        $this->languages = $this->convertToArray($languages);
    }

    /**
     * Modifies given entity
     *
     * @return void
     */
    public function modify()
    {
        if ($this->hasNotBeenProcessed()) {
            $disabledLanguageUids = [];
            foreach (EntityManager::getInstance()->getLanguages() as $language) {
                if (!in_array($language->getLanguageIsocode(), $this->getLanguagesIsoCodes())) {
                    $disabledLanguageUids[] = $language->_getUid();
                }
            }
            if (!empty($disabledLanguageUids)) {
                $text = 'mod.SHARED.disableLanguages = ' . implode(',', $disabledLanguageUids);
                $tsConfig = StringUtility::append($this->entity->getTsConfig(), $text);
                $this->entity->setTsConfig($tsConfig);
            }
        }
    }

    /**
     * Returns the iso codes of assigned languages
     *
     * @return array<string>
     */
    protected function getLanguagesIsoCodes()
    {
        $isoCodes = [];
        foreach ($this->languages as $language) {
            $isoCodes[] = $language->getLanguageIsocode();
        }
        return $isoCodes;
    }

    /**
     * Returns the priority
     *
     * @return int
     */
    public function getPriority()
    {
        return parent::getPriority();
    }

    /**
     * Returns enabled languages
     *
     * @return Models\Language[]
     * @api Called in \InstituteWeb\Iwm\Environments\DataProvider\Traits\HasModifier::getModifierInfo when existing
     */
    public function getInfo()
    {
        return $this->languages;
    }
}
