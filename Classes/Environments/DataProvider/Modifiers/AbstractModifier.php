<?php
namespace InstituteWeb\Iwm\Environments\DataProvider\Modifiers;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Iwm\Environments\DataProvider\Models;

/**
 * Class AbstractService
 *
 * @package InstituteWeb\Iwm
 */
abstract class AbstractModifier implements Interfaces\AbstractModifierInterface
{
    const PRIORITY_LOW = 0;
    const PRIORITY_NORMAL = 5;
    const PRIORITY_HIGH = 9;

    /**
     * @var bool
     */
    protected $_hasBeenProcessed = false;

    /**
     * @var int
     */
    protected $priority = self::PRIORITY_NORMAL;

    /**
     * @var Models\AbstractSystemEntryModel The instance of the entity
     */
    protected $entity;

    /**
     * Set entity to apply service on
     *
     * @param Models\AbstractSystemEntryModel $entity
     */
    public function setEntity(Models\AbstractSystemEntryModel $entity)
    {
        $this->entity = $entity;
    }

    /**
     * Returns priority of modifier. One digit int from 0 (low) to 9 (high)
     *
     * @return int
     * @internal
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Checks if this modifier has already been processed
     *
     * @return bool
     */
    protected function hasBeenProcessed()
    {
        if ($this->_hasBeenProcessed) {
            return true;
        }
        $this->_hasBeenProcessed = true;
        return false;
    }

    /**
     * Inverted alias for $this->hasBeenProcessed()
     *
     * @return bool
     */
    protected function hasNotBeenProcessed()
    {
        return !$this->hasBeenProcessed();
    }

    /**
     * Returns the last part of this class name. e.g. "AbstractModifier"
     *
     * @return string
     */
    protected function getShortClassName()
    {
        $classNameParts = explode('\\', get_class($this));
        return end($classNameParts);
    }
}
