<?php
namespace InstituteWeb\Iwm\Environments\DataProvider\Modifiers\Interfaces;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

/**
 * Interface AbstractModifierInterface
 *
 * @package InstituteWeb\Iwm
 */
interface AbstractModifierInterface
{

    // /**
    //  * @var Models\AbstractSystemEntryModel The instance of the entity this modifier is about to modify
    //  */
    // protected $entity;

    /// **
    //  * @var int
    //  */
    // protected $priority;

    /**
     * @param \InstituteWeb\Iwm\Environments\DataProvider\Models\AbstractSystemEntryModel $entity
     * @return void
     */
    public function setEntity(\InstituteWeb\Iwm\Environments\DataProvider\Models\AbstractSystemEntryModel $entity);

    /**
     * Returns priority of modifier. One digit int from 0 (low) to 9 (high)
     *
     * @return int
     */
    public function getPriority();

    /**
     * @return mixed
     */
    public function modify();
}
