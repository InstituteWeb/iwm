<?php
namespace InstituteWeb\Iwm\Environments\DataProvider\Modifiers\Interfaces;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

interface PageModifierInterface extends AbstractModifierInterface
{

}
