<?php
namespace InstituteWeb\Iwm\Environments\DataProvider\Modifiers\Template;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Iwm\Environments\DataProvider\Models;
use InstituteWeb\Iwm\Environments\DataProvider\Modifiers;
use InstituteWeb\Iwm\Environments\DataProvider\Modifiers\Interfaces;
use InstituteWeb\Iwm\Environments\DataProvider\Utility\StringUtility;

/**
 * Includes typoscript setup files
 *
 * @package InstituteWeb\Iwm
 */
class IncludeConstantsTsFiles extends Modifiers\AbstractModifier implements Interfaces\TemplateModifierInterface
{
    use \InstituteWeb\Iwm\Environments\DataProvider\Traits\SettersAcceptStringInputForArrays;

    /**
     * @var Models\Template
     */
    protected $entity;

    /**
     * @var array
     */
    protected $constantsFilePaths = [];

    /**
     * AddTypoScriptConstantsFiles constructor
     *
     * @param array $paths
     * @return IncludeConstantsTsFiles
     */
    public function __construct($paths)
    {
        $this->constantsFilePaths = array_merge($this->constantsFilePaths, $this->convertToArray($paths));
    }

    /**
     * Modifies given entity
     *
     * @return void
     */
    public function modify()
    {
        if ($this->hasNotBeenProcessed()) {
            $text = '';
            foreach ($this->constantsFilePaths as $filePath) {
                $text .= '<INCLUDE_TYPOSCRIPT: source="FILE:' . $filePath . '">' . PHP_EOL;
            }

            $config = StringUtility::append($this->entity->getConstants(), $text);
            $this->entity->setConstants($config);
        }
    }

    /**
     * Returns priority of modifier. One digit int from 0 (low) to 9 (high)
     *
     * @return int
     */
    public function getPriority()
    {
        return parent::getPriority();
    }
}
