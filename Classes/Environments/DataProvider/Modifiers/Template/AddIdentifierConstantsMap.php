<?php
namespace InstituteWeb\Iwm\Environments\DataProvider\Modifiers\Template;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Iwm\Environments\DataProvider\Models;
use InstituteWeb\Iwm\Environments\DataProvider\Modifiers;
use InstituteWeb\Iwm\Environments\DataProvider\Modifiers\Interfaces;
use InstituteWeb\Iwm\Environments\DataProvider\Utility\StringUtility;
use InstituteWeb\Iwm\Environments\Environment;

/**
 * Add all entitity identifiers from sys_registry as constants to template
 *
 * @package InstituteWeb\Iwm
 * @TODO: Write a new modifier which just add identifier from child entities (like page tree)?
 */
class AddIdentifierConstantsMap extends Modifiers\AbstractModifier implements Interfaces\TemplateModifierInterface
{
    use \InstituteWeb\Iwm\Environments\DataProvider\Traits\SettersAcceptStringInputForArrays;

    /**
     * @var Models\Template
     */
    protected $entity;

    /**
     * @var int Defines high priority modifier
     */
    protected $priority = self::PRIORITY_HIGH;

    /**
     * @var string
     */
    protected $prefix = 'global.';

    /**
     * AddIdentifierConstantsMap constructor
     *
     * @param string $prefix
     * @return AddIdentifierConstantsMap
     */
    public function __construct($prefix = 'global.')
    {
        $this->prefix = $prefix;
    }

    /**
     * Modifies given entity
     *
     * @return void
     */
    public function modify()
    {
        if ($this->hasNotBeenProcessed()) {
            $text = '# Entity mapping added by modifier "' . $this->getShortClassName() . '"' . PHP_EOL;
            foreach (Environment::getInstance()->getEntityMappings() as $identifier => $uid) {
                if (strpos($identifier, 'new_') !== 0) {
                    $text .= $this->prefix . $identifier . ' = ' . $uid . PHP_EOL;
                }
            }
            $config = StringUtility::append($this->entity->getConstants(), $text);
            $this->entity->setConstants($config);
        }
    }

    /**
     * Returns priority of modifier. One digit int from 0 (low) to 9 (high)
     *
     * @return int
     */
    public function getPriority()
    {
        return parent::getPriority();
    }
}
