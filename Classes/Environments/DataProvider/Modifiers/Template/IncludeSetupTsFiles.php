<?php
namespace InstituteWeb\Iwm\Environments\DataProvider\Modifiers\Template;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Iwm\Environments\DataProvider\Models;
use InstituteWeb\Iwm\Environments\DataProvider\Modifiers;
use InstituteWeb\Iwm\Environments\DataProvider\Modifiers\Interfaces;
use InstituteWeb\Iwm\Environments\DataProvider\Utility\StringUtility;

/**
 * Includes typoscript setup files
 *
 * @package InstituteWeb\Iwm
 */
class IncludeSetupTsFiles extends Modifiers\AbstractModifier implements Interfaces\TemplateModifierInterface
{
    use \InstituteWeb\Iwm\Environments\DataProvider\Traits\SettersAcceptStringInputForArrays;

    /**
     * @var Models\Template
     */
    protected $entity;

    /**
     * @var array
     */
    protected $setupFilePaths = [];

    /**
     * AddTypoScriptSetupFiles constructor
     *
     * @param array $paths
     * @return IncludeSetupTsFiles
     */
    public function __construct($paths)
    {
        $this->setupFilePaths = array_merge($this->setupFilePaths, $this->convertToArray($paths));
    }

    /**
     * Modifies given entity
     *
     * @return void
     */
    public function modify()
    {
        if ($this->hasNotBeenProcessed()) {
            $text = '';
            foreach ($this->setupFilePaths as $filePath) {
                $text .= '<INCLUDE_TYPOSCRIPT: source="FILE:' . $filePath . '">' . PHP_EOL;
            }

            $config = StringUtility::append($this->entity->getConfig(), $text);
            $this->entity->setConfig($config);
        }
    }

    /**
     * Returns priority of modifier. One digit int from 0 (low) to 9 (high)
     *
     * @return int
     */
    public function getPriority()
    {
        return parent::getPriority();
    }
}
