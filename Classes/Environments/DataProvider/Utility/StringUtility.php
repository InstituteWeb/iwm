<?php
namespace InstituteWeb\Iwm\Environments\DataProvider\Utility;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

/**
 * Class with helper functions for string handling
 */
class StringUtility
{
    /**
     * Appends string to another string and separates them with two times PHP_EOL.
     * Trims each value before using line separators.
     *
     * @param string $original text
     * @param string $appendText text to append
     * @return string
     */
    public static function append($original, $appendText)
    {
        $original = trim($original);
        $appendText = trim($appendText);

        $text = empty($original) ? '' : $original . PHP_EOL . PHP_EOL;
        $text .= trim($appendText) . PHP_EOL . PHP_EOL;

        return $text;
    }
}
