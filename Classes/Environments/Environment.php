<?php
namespace InstituteWeb\Iwm\Environments;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2014-2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Iwm\Environments\DataProvider\EntityManager;
use InstituteWeb\Iwm\Scripts\Helper;
use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use InstituteWeb\Iwm\Environments\DataProvider\Traits;

/**
 * Helper class for different environments based on ApplicationContexts
 *
 * @package Iwm\Iwm
 */
class Environment implements \TYPO3\CMS\Core\SingletonInterface
{
    use Traits\UseSignalSlotDispatcher;

    const SLOT_EARLYOPERATIONS = 'EarlyOperations';

    /**
     * @var array|null
     */
    public $typo3ConfVars = [];

    /**
     * @var array key => value storage
     */
    protected $values = [];

    /**
     * If Environment is marked as mock, the method apply() will not
     * merge $this->typo3ConfVars with $GLOBALS['TYPO3_CONF_VARS'].
     *
     * @var bool Default false
     */
    protected $isMock = false;

    /**
     * @var array<closure>
     */
    protected $calledPhpConfigFiles = [];

    /**
     * @var DatabaseConnection|null
     */
    protected $databaseConnection;

    /**
     * @var array
     * @TODO: Move to EntityManager?
     */
    protected $mappedEntities = [];

    /**
     * Environment constructor.
     *
     * @param array $typo3ConfVars
     * @param bool $mock
     */
    public function __construct($typo3ConfVars = null, $mock = false)
    {
        if (!is_null($typo3ConfVars) && empty($this->typo3ConfVars)) {
            $this->typo3ConfVars = $typo3ConfVars;
        }
        if (!$mock && file_exists(Helper\File::getTypo3RootPath(). 'FIRST_INSTALL')) {
            $mock = true;
        }
        $this->isMock = $mock;
    }

    /**
     * Initializes this utilities
     *
     * @param array $typo3ConfVars not referenced!
     * @return void
     */
    public function init($typo3ConfVars)
    {
        $this->typo3ConfVars = $typo3ConfVars;
    }

    /**
     * Applies the set Typo3ConfVars to $GLOBALS['TYPO3_CONF_VARS']
     *
     * @return Environment
     */
    public function apply()
    {
        if (!$this->isMock) {
            $GLOBALS['TYPO3_CONF_VARS'] = $this->typo3ConfVars;
            $GLOBALS['TYPO3_DB'] = Environment::getInstance()->getDatabaseConnection();
            // Processing modifiers with high priority
            EntityManager::getInstance()->processModifiers(DataProvider\Modifiers\AbstractModifier::PRIORITY_HIGH);
            $this->signalDispatch(self::SLOT_EARLYOPERATIONS, [Environment::getInstance(), EntityManager::getInstance()]);
        }
        return $this;
    }

    /**
     * Set $this->isMock to false, executes given callback and restores $this->isMock.
     * If $this->isMock is already false just the callback is getting executed and returned.
     *
     * @param callable $callback
     * @return mixed Return value of given callback
     */
    public function unmock(callable $callback)
    {
        $previousMockState = $this->isMock;
        $this->isMock = false;
        $returnValue = $callback();
        $this->isMock = $previousMockState;
        return $returnValue;
    }

    /**
     * Includes environment settings based on root application context
     * located in EXT:iwm/Resources/Private/ApplicationContexts/
     *
     * Also it appends the root application context name to ['SYS']['sitename']
     * wrapped with squared brackets.
     *
     * @param string $path Optional. When not empty the given folder will be taken to fetch root context.
     * @param string $overwriteRootContext When not empty, it overwrites the given root context.
     * @return Environment
     */
    public function includeRootEnvironmentSettings($path = '', $overwriteRootContext = '')
    {
        $extPath = Helper\File::concatenatePathParts([Helper\File::getTypo3ConfPath(), 'ext', 'iwm'], true);
        $rootContext = $this->getRootApplicationContext();
        if (!empty($overwriteRootContext)) {
            $rootContext = $overwriteRootContext;
        }

        // TODO: Make path configurable
        $path = empty($path) ? Helper\File::concatenatePathParts([$extPath, 'Resources', 'Private', 'ApplicationContexts'], true) : $path;
        $includePath = $path . (string) $rootContext . '.php';
        $this->callPhpConfigFile($includePath, false);

        if (isset($this->typo3ConfVars['SYS']['sitename'])) {
            $this->typo3ConfVars['SYS']['sitename'] .= ' [' . $this->getRootApplicationContext() . ']';
        }
        return $this;
    }

    /**
     * Includes given php file. Expecting and executing a returning closure function.
     * Passing three arguments: $constantsManager, $system and $environment ($this)
     *
     * @param string $pathToPhpFile Which contains a returning closure function
     * @param bool $trackCall Internal. When true this call is not registred in $this->calledPhpConfigFiles.
     * @return Environment
     */
    public function callPhpConfigFile($pathToPhpFile, $trackCall = true)
    {

        $path = realpath($pathToPhpFile);
        if (!$path) {
            throw new \RuntimeException('File not found! File: ' . $pathToPhpFile);
        }
        $phpResult = require ($path);
        if (is_callable($phpResult)) {
            $constantsManager = EntityManager::getInstance();
            $phpResult($constantsManager, $this);
            if ($trackCall) {
                if (!in_array($pathToPhpFile, $this->calledPhpConfigFiles)) {
                    $this->calledPhpConfigFiles[] = $path;
                }
            }
        } else {
            throw new \RuntimeException('This file does not return a callable function. File: ' . $path);
        }
        return $this;
    }

    /**
     * @return array
     */
    public function getCalledPhpConfigFiles()
    {
        return $this->calledPhpConfigFiles;
    }


        /**
     * Includes sub environment settings based on sub application contexts
     * which are located in typo3conf/Environments/
     *
     * Examples:
     * The context "Development/DevServer" will include "typo3conf/Environments/DevServer.php"
     * The context "Development/Live/Server1" will include "typo3conf/Environments/Live/Server1.php"
     *
     * @param string $path Optional. When not empty, the location of environment files gets overwritten.
     * @param string $overwriteApplicationContext When not empty, overwrites the given application context.
     * @return Environment
     */
    public function includeSubEnvironmentSettings($path = '', $overwriteApplicationContext = '')
    {
        if ($this->isMock) {
            $applicationContext = getenv('TYPO3_CONTEXT');
            if (!$applicationContext) {
                $applicationContext = Helper\ApplicationContext::getCurrentApplicationContextStatus();
                $applicationContext = $applicationContext['context'];
            }
        } else {
            $applicationContext = GeneralUtility::getApplicationContext();
            if (!empty($overwriteApplicationContext)) {
                $applicationContext = $overwriteApplicationContext;
            }
        }
        $contextParts = GeneralUtility::trimExplode('/', (string) $applicationContext, true);
        array_shift($contextParts);
        $subContext = implode('/', $contextParts);

        $path = empty($path) ? Helper\File::concatenatePathParts([Helper\File::getTypo3ConfPath(), 'Environments'], true) : $path;
        $subEnvironmentPath = $path . $subContext . '.php';

        $this->callPhpConfigFile($subEnvironmentPath);
        return $this;
    }

    /**
     * Returns root ApplicationContext (Development, Production or Testing)
     *
     * @return \TYPO3\CMS\Core\Core\ApplicationContext The root context
     */
    public function getRootApplicationContext()
    {
        if ($this->isMock) {
            return 'Development';
        }
        $context = GeneralUtility::getApplicationContext();

        if (is_null($context)) {
            $contextString = Helper\ApplicationContext::getCurrentApplicationContext();
            GeneralUtility::presetApplicationContext(new \TYPO3\CMS\Core\Core\ApplicationContext($contextString));
            $context = GeneralUtility::getApplicationContext();
        }

        do {
            $rootContext = $context;
            $context = $context->getParent();
        } while (!is_null($context));
        return $rootContext;
    }

    /**
     * Compares given contextPattern with current application context
     *
     * @param string $contextPattern Simple string (with wildcard * support) or regular expression.
     *                               You can also pass multiple patterns, separated by comma.
     * @return bool Returns true if given pattern matches with current application context
     */
    public function matchApplicationContext($contextPattern)
    {
        $values = GeneralUtility::trimExplode(',', $contextPattern, true);
        foreach ($values as $applicationContext) {
            if (static::searchStringWildcard(GeneralUtility::getApplicationContext(), $applicationContext)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Matching two strings against each other, supporting a "*" wildcard
     * or (if wrapped in "/") PCRE regular expressions
     *
     * @param string $haystack
     * @param string $needle
     * @return bool
     *
     * @see \TYPO3\CMS\Core\Configuration\TypoScript\ConditionMatching\AbstractConditionMatcher::searchStringWildcard
     */
    protected function searchStringWildcard($haystack, $needle)
    {
        $result = false;
        if ($haystack === $needle) {
            $result = true;
        } elseif ($needle) {
            if (preg_match('/^\\/.+\\/$/', $needle)) {
                // Regular expression, only "//" is allowed as delimiter
                $regex = $needle;
            } else {
                $needle = str_replace(array('*', '?'), array('###MANY###', '###ONE###'), $needle);
                $regex = '/^' . preg_quote($needle, '/') . '$/';
                // Replace the marker with .* to match anything (wildcard)
                $regex = str_replace(array('###MANY###', '###ONE###'), array('.*', '.'), $regex);
            }
            $result = (bool)preg_match($regex, $haystack);
        }
        return $result;
    }

    /**
     * Set credentials for TYPO3 DatabaseConnection
     *
     * @param string $username
     * @param string $password
     * @param string $database
     * @param string $host
     * @param string $port
     * @param string $socket
     * @return void
     */
    public function setDatabaseCredentials($username, $password, $database, $host = '127.0.0.1', $port = '3306', $socket = '')
    {
        $this->typo3ConfVars['DB']['host'] = $host;
        $this->typo3ConfVars['DB']['username'] = $username;
        $this->typo3ConfVars['DB']['password'] = $password;
        $this->typo3ConfVars['DB']['database'] = $database;
        $this->typo3ConfVars['DB']['port'] = $port;
        $this->typo3ConfVars['DB']['socket'] = $socket;
    }

    /**
     * Get current DatabaseConnection
     *
     * @return \TYPO3\CMS\Core\Database\DatabaseConnection
     * @throws \Exception
     */
    public function getDatabaseConnection()
    {
        if (!is_null($this->databaseConnection)) {
            return $this->databaseConnection;
        }

        if (!$this->typo3ConfVars['DB']['host'] ||
            !$this->typo3ConfVars['DB']['username'] ||
            !$this->typo3ConfVars['DB']['database']
        ) {
            throw new \Exception(
                'Please call "setDatabaseCredentials()" first, before requesting the actual connection.'
            );
        }

        /** @var \TYPO3\CMS\Core\Database\DatabaseConnection $db */
        $db = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Database\DatabaseConnection::class);
        $db->setDatabaseHost($this->typo3ConfVars['DB']['host']);
        $db->setDatabaseUsername($this->typo3ConfVars['DB']['username']);
        $db->setDatabasePassword($this->typo3ConfVars['DB']['password']);
        $db->setDatabaseName($this->typo3ConfVars['DB']['database']);

        if (isset($this->typo3ConfVars['DB']['database']['socket'])) {
            $db->setDatabaseSocket($this->typo3ConfVars['DB']['database']['socket']);
        }
        if (isset($this->typo3ConfVars['DB']['database']['port'])) {
            $db->setDatabasePort($this->typo3ConfVars['DB']['database']['port']);
        }

        try {
            $db->initialize();
            $db->connectDB();
        } catch (\Exception $exception) {
            throw new \Exception('An error occured: ' . $exception->getMessage());
        }

        if (!$db->isConnected()) {
            throw new \Exception('Connection to database failed!');
        }
        return $this->databaseConnection = $db;
    }

    /**
     * Get entity mappings from sys_registry
     *
     * @return array key is the identifier, value the uid
     * @TODO: Move to EntityManager?
     */
    public function getEntityMappings()
    {
        if (!empty($this->mappedEntities)) {
            return $this->mappedEntities;
        }
        $database = $GLOBALS['TYPO3_DB'] = $this->getDatabaseConnection();
        if ($this->isMock) {
            $tables = $database->admin_get_tables();
            if (!array_key_exists('sys_registry', $tables)) {
                return [];
            }
        }
        /** @var \TYPO3\CMS\Core\Registry $registry */
        $registry = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Registry::class);
        $mappedEntities = $registry->get('iwm', 'mappedEntities', []);
        $items = [];
        foreach (array_keys(EntityManager::getInstance()->getAll()) as $identifier) {
            $items[$identifier] = $mappedEntities[$identifier];
        }
        return $items;
    }

    /**
     * Set entity mappings in sys_registry
     *
     * @param array $mappedItems
     * @return void
     * @throws \Exception
     * @TODO: Move to EntityManager?
     */
    public function setEntityMappings(array $mappedItems)
    {
        $this->mappedEntities = $mappedItems;
        $GLOBALS['TYPO3_DB'] = $this->getDatabaseConnection();

        /** @var \TYPO3\CMS\Core\Registry $registry */
        $registry = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Registry::class);
        $registry->set('iwm', 'mappedEntities', $mappedItems);
    }

    /**
     * Get uid of given identifier. Returns null if not found.
     *
     * @param string $identifier
     * @return int|null
     */
    public function getEntityMapping($identifier)
    {
        $mappings = $this->getEntityMappings();
        if (isset($mappings[$identifier])) {
            return (int) $mappings[$identifier];
        }
        return null;
    }

    /**
     * Get value
     *
     * @param string $key
     * @param mixed $default
     * @return mixed
     * @throws \Exception
     * @internal
     */
    public function getValue($key, $default = null)
    {
        return $this->values[$key] ?: $default;
    }

    /**
     * Set value
     *
     * @param string $key
     * @param mixed $value
     * @return void
     * @throws \Exception
     * @internal
     */
    public function setValue($key, $value)
    {
        $this->values[$key] = $value;
    }

    /**
     * Unset value
     *
     * @param string $key
     * @return void
     * @throws \Exception
     * @internal
     */
    public function removeValue($key)
    {
        unset($this->values[$key]);
    }

    /**
     * Sets given setting of extension configuration
     *
     * @param string $extensionKey
     * @param string $settingName
     * @param mixed $value
     * @return void
     */
    public function setExtensionSetting($extensionKey, $settingName, $value)
    {
        $extensionConfiguration = array();
        if (isset($this->typo3ConfVars['EXT']['extConf'][$extensionKey])) {
            $extensionConfiguration = unserialize($this->typo3ConfVars['EXT']['extConf'][$extensionKey]);
            if (is_array($extensionConfiguration)) {
                $extensionConfiguration[$settingName] = $value;
            }
        } else {
            $extensionConfiguration[$settingName] = $value;
        }
        if (is_array($extensionConfiguration)) {
            $this->typo3ConfVars['EXT']['extConf'][$extensionKey] = serialize($extensionConfiguration);
        }
    }

    /**
     * Adds given extensionKey to runtimeActivatedPackages
     *
     * @param string $extensionKey
     * @return void
     */
    public function activateExtension($extensionKey)
    {
        if ($this->isMock || !$this->isExtensionValid($extensionKey)) {
            return;
        }
        if (!isset($this->typo3ConfVars['EXT']['runtimeActivatedPackages'])) {
            $this->typo3ConfVars['EXT']['runtimeActivatedPackages'] = [];
        }
        $this->typo3ConfVars['EXT']['runtimeActivatedPackages'][] = $extensionKey;
    }

    /**
     * Checks if given extension is existing and set in PackageStates.php
     *
     * @param string $extensionKey
     * @return bool
     */
    protected function isExtensionValid($extensionKey)
    {
        $emconf = Helper\File::concatenatePathParts([Helper\File::getTypo3ConfPath(), 'ext', $extensionKey, 'ext_emconf.php']);
        if (!file_exists($emconf)) {
            return false;
        }
        $packageStates = @require(Helper\File::getTypo3ConfPath() . 'PackageStates.php');
        return is_array($packageStates) && isset($packageStates['packages'][$extensionKey]['state']);
    }

    /**
     * Resets the typo3ConfVars
     *
     * @return void
     */
    public function reset()
    {
        $this->typo3ConfVars = [];
    }

    /**
     * Get singleton instance of Environment
     *
     * @param array $typo3ConfVars
     * @param bool $mock
     * @return Environment
     */
    public static function getInstance(array $typo3ConfVars = null, $mock = false)
    {
        return GeneralUtility::makeInstance(static::class, $typo3ConfVars, $mock);
    }
}
