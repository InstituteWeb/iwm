<?php
namespace InstituteWeb\Iwm\UserConditions;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2014-2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

/**
 * Checks if the current page in frontend has a translation.
 *
 * Usage:
 * [userFunc = \InstituteWeb\Iwm\UserConditions\user_translationExistsNot()]
 *  && [globalVar = GP:L > 0]
 *
 * @return bool Returns TRUE if the current page has been translated,
 *                 otherwise FALSE
 */
function user_translationExistsNot()
{
    return !user_translationExists();
}
