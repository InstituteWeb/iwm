<?php
namespace InstituteWeb;

/*  | This script is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Iwm\Environments\Environment;
use InstituteWeb\Iwm\Environments\DataProvider\EntityManager;
use InstituteWeb\Iwm\Environments\DataProvider\Models;

/**
 * @param EntityManager $constantsManager
 * @param Environment $environment
 * @return callable function
 */
return function (EntityManager $constantsManager, Environment $environment) {

    $environment->typo3ConfVars['BE']['debug'] = false;
    $environment->typo3ConfVars['FE']['debug'] = false;
    
    $environment->typo3ConfVars['SYS']['displayErrors'] = 0;
    $environment->typo3ConfVars['SYS']['enableDeprecationLog'] = '';
    $environment->typo3ConfVars['SYS']['sqlDebug'] = 0;
    $environment->typo3ConfVars['SYS']['systemLogLevel'] = 1;
};
