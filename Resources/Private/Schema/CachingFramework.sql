CREATE TABLE cf_cache_hash (
  id int(11) unsigned NOT NULL AUTO_INCREMENT,
  identifier varchar(250) NOT NULL DEFAULT '',
  expires int(11) unsigned NOT NULL DEFAULT '0',
  content mediumblob,
  PRIMARY KEY (id),
  KEY cache_id (identifier,expires)
);

CREATE TABLE cf_cache_hash_tags (
  id int(11) unsigned NOT NULL AUTO_INCREMENT,
  identifier varchar(250) NOT NULL DEFAULT '',
  tag varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (id),
  KEY cache_id (identifier),
  KEY cache_tag (tag)
);

CREATE TABLE cf_extbase_object (
  id int(11) unsigned NOT NULL AUTO_INCREMENT,
  identifier varchar(250) NOT NULL DEFAULT '',
  expires int(11) unsigned NOT NULL DEFAULT '0',
  content mediumblob,
  PRIMARY KEY (id),
  KEY cache_id (identifier,expires)
);

CREATE TABLE cf_extbase_object_tags (
  id int(11) unsigned NOT NULL AUTO_INCREMENT,
  identifier varchar(250) NOT NULL DEFAULT '',
  tag varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (id),
  KEY cache_id (identifier),
  KEY cache_tag (tag)
);
