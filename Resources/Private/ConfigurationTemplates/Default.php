<?php
namespace InstituteWeb;

/*  | This script is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Iwm\Environments\Environment;
use InstituteWeb\Iwm\Environments\DataProvider\EntityManager;
use InstituteWeb\Iwm\Environments\DataProvider\Models;

/**
 * @param EntityManager $entityManager
 * @param Environment $environment
 * @return callable function
 */
return function (EntityManager $entityManager, Environment $environment) {
    // Environment settings
    $environment->setDatabaseCredentials(
        'username',
        'password',
        'db',
        '127.0.0.1'
    );

    //$environment->typo3ConfVars['MAIL']['transport'] = 'mail';

    //$environment->activateExtension('t3adminer');


    ///** @var Models\Pages\RootPage $rootPage */
    //$rootPage = $entityManager->get('root.default');
    //$rootPage->setDomains([
    //    new Models\Domain('dev.www.domain.com', true),
    //    new Models\Domain('dev.domain.com', false, 'http://dev.www.domain.com')
    //]);
};
