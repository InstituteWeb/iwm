<?php

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2014-2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

$boot = function ($extensionKey) {
    $extensionPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($extensionKey);

    // User Conditions
    require_once($extensionPath . 'Classes/UserConditions/user_translationExists.php');
    require_once($extensionPath . 'Classes/UserConditions/user_translationExistsNot.php');

    // Overwrite f:uri.resource ViewHelper
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects']['TYPO3\CMS\Fluid\ViewHelpers\Uri\ResourceViewHelper'] = array(
        'className' => 'InstituteWeb\Iwm\ViewHelpers\Uri\ResourceViewHelper',
    );
};
$boot($_EXTKEY);
unset($boot);
