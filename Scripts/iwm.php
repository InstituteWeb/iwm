<?php

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

use InstituteWeb\Iwm\Scripts\Helper;

/**
 * Concatenates given path parts with DIRECTORY_SEPARATOR
 *
 * @param array $pathParts
 * @return string
 * @see \InstituteWeb\Iwm\Utility\File::concatenatePathParts
 */
$concatPathParts = function (array $pathParts) {
    return implode(DIRECTORY_SEPARATOR, $pathParts);
};

// Include Composer Autoloader
$sitePath = realpath($concatPathParts([__DIR__, '..', '..', '..', '..'])) . DIRECTORY_SEPARATOR;
require_once($sitePath . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php');

/**
 * Fetches version from composer.json for Application title
 *
 * @return string
 */
$getVersion = function () use ($sitePath, $concatPathParts) {
    $version = '';
    $iwmPath = $sitePath . $concatPathParts(['typo3conf', 'ext', 'iwm']) . DIRECTORY_SEPARATOR;
    $composerJson = @json_decode(file_get_contents($iwmPath . 'composer.json'), true);
    if ($composerJson) {
        $version = $composerJson['version'];
    }
    return str_pad($version, 21, ' ', STR_PAD_LEFT);
};

/**
 * Get TYPO3_CONTEXT from .env file
 *
 * @return string The current context or notice that no context is set
 */
$getContext = function () {
    $context = Helper\ApplicationContext::getCurrentApplicationContext();
    if (empty($context)) {
        return str_pad('Context: None' . $context, 64, ' ', STR_PAD_RIGHT);
    }
    return str_pad('Context: ' . $context, 64, ' ', STR_PAD_RIGHT);
};

/**
 * Get path to config file for set up environment. Returns dash sign when no context set.
 *
 * @return string path to config file, or dash sign
 */
$getEnvironmentConfigFile = function () {
    $pathToConfigFile = Helper\ApplicationContext::getConfigPathFromSetApplicationContext();
    return str_pad('Config file: ' . $pathToConfigFile, 64, ' ', STR_PAD_RIGHT);
};

$application = new \Symfony\Component\Console\Application(implode(PHP_EOL, [
    '+-------------------------------------------------------------------+',
    '|                                                                   |',
    '|  Institute Web                                                    |',
    '|  TYPO3 Master Console Tool                                        |',
    '|                                            ' . $getVersion() . '  |',
    '+-------------------------------------------------------------------+',
    '|  ' . $getContext() . ' |',
    '|  ' . $getEnvironmentConfigFile() . ' |',
    '+-------------------------------------------------------------------+',
]));

$application->add(new \InstituteWeb\Iwm\Scripts\Commands\InstallCommand());
$application->add(new \InstituteWeb\Iwm\Scripts\Commands\CompatibilitySymlinkCommand());
$application->add(new \InstituteWeb\Iwm\Scripts\Commands\EntitiesShowCommand());
$application->add(new \InstituteWeb\Iwm\Scripts\Commands\EntitiesUpdateCommand());
$application->add(new \InstituteWeb\Iwm\Scripts\Commands\EntitiesRemapCommand());
$application->add(new \InstituteWeb\Iwm\Scripts\Commands\EntitiesRenameIdentifierCommand());
$application->add(new \InstituteWeb\Iwm\Scripts\Commands\EntitiesDebugCommand());
$application->add(new \InstituteWeb\Iwm\Scripts\Commands\StatusConfigCommand());
$application->add(new \InstituteWeb\Iwm\Scripts\Commands\StatusContextCommand());
$application->add(new \InstituteWeb\Iwm\Scripts\Commands\ContextSetCommand());
$application->add(new \InstituteWeb\Iwm\Scripts\Commands\ContextRemoveCommand());

$application->run();
